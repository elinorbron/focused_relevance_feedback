__author__='Elinor'
"""


"""

import os
import os.path
import sys
import subprocess
import itertools
scriptpath="/lv_local/home/elinor/pycharm/nf/evaluation/"
sys.path.append(os.path.dirname(scriptpath))
from qpos_eval import eval_settings as es



feedback_setup_name=['RelSeg','NRSeg','NRD','RelD','PRF']
RM_setup_name=['NRseg','uni','percent']
#define paths
#baseline_home_dir="/lv_local/home/elinor/negative_feedback/baselines/Fixed_RelD"+'/'
baseline_home_dir=sys.argv[1]+'/'
initial_feedback_setup_name=baseline_home_dir.rstrip('/').rstrip('/').split('_')[-1]
setup=baseline_home_dir.rstrip('/').rstrip('/').split('_')[-2].split('/')[-1]
print setup
Pflag=0
print initial_feedback_setup_name
if initial_feedback_setup_name=='RelSeg':
	initial_feedback_setup=1
elif initial_feedback_setup_name == 'RelD' or initial_feedback_setup_name == 'RelDP':
	initial_feedback_setup=4
	if initial_feedback_setup_name == 'RelDP':
		Pflag=1;
else:
	print 'error'

if setup=="Fixed":
	print "Fixed"
	flag_fixed=True
	max_K=10
else:
	print "NonFixed"
	flag_fixed=False
	max_K=5

ROBUSTflag=0
if "robust" in baseline_home_dir:
	ROBUSTflag=1;
if ROBUSTflag:
	start_param_file_path="/lv_local/home/elinor/negative_feedback_robust/robust_qrelsAndParams//param_part1_keeping_origQW_RM.xml"
	orig_queries="/lv_local/home/elinor/negative_feedback_robust/robust_qrelsAndParams/fake_segments_queries.xml"
	init_ret_path='/lv_local/home/elinor/negative_feedback_robust/QL/QL_robust_1500.txt'
	qrels_path='/lv_local/home/elinor/negative_feedback_robust/robust_qrelsAndParams/qrelsROBUST'
else:
	start_param_file_path="/lv_local/home/elinor/negative_feedback/Inex_qrelsAndParams/param_part1_keeping_origQW_RM.xml"
	orig_queries="/lv_local/home/elinor/negative_feedback/Inex_qrelsAndParams/INEX_params_b_segQ.xml"
	init_ret_path='/lv_local/home/elinor/negative_feedback/QL/ql_ret_INEX.txt'#'/lv_local/home/elinor/negative_feedback/QL/ql_ret_INEX.txt'
	qrels_path='/lv_local/home/elinor/negative_feedback/Inex_qrelsAndParams/inex2009-2010-article.qrels_binary'
output_param_files_dir=baseline_home_dir+"parameters/"
##any number bigger than the amount of docs we are using for feedback

RM_setup=2
if Pflag:
	RM_setup=3
fbDocs=range(1,max_K+1)
fbOrigWeight=[0.2,0.5,0.8] #for file name only. within we pass
fbTerms=[10,25,50]


def create_dir():
	if not os.path.exists(baseline_home_dir):
		os.makedirs(baseline_home_dir)
	l=["parameters/","res/","chunks/"]
	for dir in l:
		if not os.path.exists(baseline_home_dir+dir):
			os.makedirs(baseline_home_dir+dir)


def read_query_info(orig_queries):
	#step one- for each query create a dictionary of the query words.
	global q_info
	q_info={}
	q_file=open(orig_queries,'r')
	query_str_a='\t<query>\n'
	query_str_b=str()
	for line in q_file:
		if 'number' in line:
			q_id=line.strip('\t\t<number>')
			q_id=q_id.rstrip('</number>\n\t\t')
			query_str_a+=line;
		if '<text>' in line:
			query_str_a+=line;
		if '<relseg>' in line:
			query_str_b+=line;
		if '</query>' in line:
			query_str_b+=line;
			q_info[q_id]=[query_str_a,query_str_b]
			query_str_a='\t<query>\n'
			query_str_b=str()
	q_file.close()
	return


def create_params_baseline_fixed():
	read_query_info(orig_queries)
	#es.getting_rel_docs_from_res_file( max_K, flag_fixed, init_ret_path, qrels_path )
	create_dir()
	#start code:
	#i=0;
	start_param_file=open(start_param_file_path,'r')
	start_param=start_param_file.read()
	start_param_file.close()
	for fbDocs_parm in fbDocs:
		for fbOrigWeight_parm in fbOrigWeight:
			for term in fbTerms:
				#writing parameter file part 1
				this_output=str()
				this_output_name=str(fbDocs_parm)+'_'+str(fbOrigWeight_parm)+'_'+str(term)+'_'+initial_feedback_setup_name+'_'+RM_setup_name[RM_setup-1]
				this_output_path=output_param_files_dir+this_output_name
				this_output+=start_param
				this_output+='\t<RM_Setup>'+str( RM_setup )+'</RM_Setup>\n'
				this_output+='\t<fbTerms>'+str(term)+'</fbTerms>\n'
				this_output+='\t<fbOrigWeight>'+str(fbOrigWeight_parm)+'</fbOrigWeight>\n'
				this_output+='\t<feedback_setup>'+str(initial_feedback_setup)+'</feedback_setup>\n'
				this_output+='\t<fbDocs>'+str(fbDocs_parm)+'</fbDocs>\n'
				for query in sorted(q_info.keys()):
					this_output+=q_info[query][0]
					this_output+=q_info[query][1]
				this_output+="</parameters>\n"
				out_f=open(this_output_path,'w+')
				out_f.write(this_output)
				out_f.close()
		print "done"
	return 1


def create_params_baseline_accumulative():
	CONST_fbdoc_num=2*max_K+1
	read_query_info(orig_queries)
	es.getting_rel_docs_from_res_file( max_K, flag_fixed, init_ret_path, qrels_path )
	create_dir()
	#start code:
	#i=0;
	start_param_file=open(start_param_file_path,'r')
	start_param=start_param_file.read()
	start_param_file.close()
	for fbDocs_parm in fbDocs:
		for fbOrigWeight_parm in fbOrigWeight:
			for term in fbTerms:
				#writing parameter file part 1
				this_output=str()
				this_output_name=str(fbDocs_parm)+'_'+str(fbOrigWeight_parm)+'_'+str(term)+'_'+initial_feedback_setup_name+'_'+RM_setup_name[RM_setup-1]
				this_output_path=output_param_files_dir+this_output_name
				this_output+=start_param
				this_output+='\t<RM_Setup>'+str( RM_setup )+'</RM_Setup>\n'
				this_output+='\t<fbTerms>'+str(term)+'</fbTerms>\n'
				this_output+='\t<fbOrigWeight>'+str(fbOrigWeight_parm)+'</fbOrigWeight>\n'
				this_output+='\t<feedback_setup>'+str(initial_feedback_setup)+'</feedback_setup>\n'
				this_output+='\t<fbDocs>'+str(CONST_fbdoc_num)+'</fbDocs>\n'
				for query in sorted(q_info.keys()):
					this_output+=q_info[query][0]
					for docNo in es.Rel_used[fbDocs_parm][query]:
						this_output+='\t\t<feedbackDocno>'+str(docNo)+'</feedbackDocno>\n'
					this_output+=q_info[query][1]
				this_output+="</parameters>\n"
				out_f=open(this_output_path,'w+')
				out_f.write(this_output)
				out_f.close()
		print "done"
	return 1


if flag_fixed:
	create_params_baseline_fixed();
else:
	create_params_baseline_accumulative( )

