__author__='Elinor'
"""this is much more efficient than the other Fusion file, ####
but should do the same.
This implementation is correct for the normal evaluation
This implementation is correct for the residual evaluation under the asspmtion that all files used (relevant and non-relevant) in feedback should be removed because they are part of the model, whethere we choose to use them or not (by the leave-one-out-cross-validation)"""
import os
import os.path
import sys
import subprocess
import itertools
import pandas as pd
import numpy as np
import scipy.stats
#print sys.path
scriptpath="/lv_local/home/elinor/pycharm/nf/evaluation/"
sys.path.append(os.path.dirname(scriptpath))
from qpos_eval import eval_settings as es


#feedback_setup_name_list=['RelSeg','NRSeg','NRD','RelD','PRF']
rerank_setup_name_list=['RelSeg','NRSeg','NRD','RelD','PRF']
alpha_setup=np.arange(0.0,1.2,0.2)
beta_setup=np.arange(0.0,1.2,0.2)
#alpha_setup=(0.0,1.0)
#beta_setup=(0.0,1.0)

residual_docs_per_query={}
baseline_used_doc_for_fixed_rel={}
#setup="nonFixed"
# #baseline_home_dir='/lv_local/home/elinor/negative_feedback/baselines/Accumulative_RelSeg/'
baseline_home_dir=sys.argv[1]
if len(sys.argv)>2:
	flagNormal= int(sys.argv[2])
	flagAll=0
else:
	flagAll=1

home_dir_input=sys.argv[1]+'/'
full_setup_name=home_dir_input.rstrip( '/' ).rstrip( '/' ).split( '/' )[-1]
setup=full_setup_name.split( '_' )[0]
model=home_dir_input.rstrip( '/' ).rstrip( '/' ).split( '/' )[-2]
print "sampling setup", setup
print "model", model
print "full_setup_name", full_setup_name

#print "setup input is"+setup
flag_residual=True

if setup=="Fixed":
	print "Fixed"
	flag_fixed=True
	max_K=10
else:
	print "NonFixed"
	flag_fixed=False
	max_K=5
################

str_queries_path='/lv_local/home/elinor/negative_feedback/Inex_qrelsAndParams/INEX_params_b_segQ.xml'
init_ret_path='/lv_local/home/elinor/negative_feedback/QL/ql_ret_INEX.txt'
binary_qrels_path='/lv_local/home/elinor/negative_feedback/Inex_qrelsAndParams/inex2009-2010-article.qrels_binary'


def create_folders(home_rerank_dir):
	home_rerank_pos=home_rerank_dir
	if not os.path.exists(home_rerank_pos):
		os.makedirs(home_rerank_pos)
	if not os.path.exists( home_rerank_pos+'res_1000/' ):
		os.makedirs(home_rerank_pos+'res_1000/')
	if not os.path.exists( home_rerank_pos+'res_1000_residual/' ):
		os.makedirs(home_rerank_pos+'res_1000_residual/')


def read_queries_to_dict( str_path, origin_q_dic ):
	f=open(str_path,'r')
	for line in f:
		if 'number' in line:
			q_id=line.strip('\t\t<number>')
			q_id=q_id.rstrip('</number>\n\t\t')
			origin_q_dic[q_id]=str()
	return origin_q_dic



#this function load the original results from str_res_path to res_df_dict, and returns the file name
##res_df_dict[queryid][doc]['original']=grade
def load_result_file_into_df_orig_normal(origin_q_dic, res_df_dict, str_res_path):
	#print str_res_path
	res_file=open(str_res_path,'r')
	file_name=str_res_path.split('/')[-1]
	res_per_q={}
	res_per_q=res_per_q.fromkeys(origin_q_dic.keys())
	str_res_per_q=dict( (key, str()) for key in origin_q_dic.keys() )
	for line in res_file:
		q_id=line.split()[0]
		str_res_per_q[q_id]+=line
	for q_id in sorted(origin_q_dic.keys()):
		temp_res_df=pd.Series((str_res_per_q[q_id].split('\n')[:-1]))
		splits=temp_res_df.astype('str').str.split()
		q_query_id=pd.DataFrame()
		q_query_id['doc']=splits.str[2]
		q_query_id['original']=splits.str[4]
		q_query_id=q_query_id.set_index('doc')
		res_per_q[q_id]=q_query_id
	res_df_dict[file_name]=res_per_q
	res_file.close()
	return file_name

def read_res_from_file_to_dict_rerank(filepath,origin_q_dic):
	res_file=open( filepath, 'r' )
	dict_res_str=dict( (key, str()) for key in origin_q_dic.keys() )
	queries_no_fb=[]
	for line in res_file:
		q_id=line.split( )[0]
		if line.split( )[1] == "Q0":
			dict_res_str[q_id]+=line
		else:
			queries_no_fb+=[q_id]
	return dict_res_str,queries_no_fb

def read_res_from_dictionary_to_df_rerank(rerank_res_dict,rerank_name,queries_no_fb,file_name,original_res_dict_file):
	for q_id in sorted( rerank_res_dict.keys( ) ):
		if q_id not in queries_no_fb:
			temp_res_df=pd.Series( (rerank_res_dict[q_id].split( '\n' )[:-1]) )
			splits=temp_res_df.astype( 'str' ).str.split( )
			rerank_res_per_query=pd.DataFrame( )
			rerank_res_per_query['doc']=splits.str[2]
			rerank_res_per_query[rerank_name]=splits.str[4]
		else:
			# q_query_id=original_res_dict[file_name][q_id].copy(True)
			rerank_res_per_query=pd.DataFrame( original_res_dict_file[q_id].index )
			rerank_res_per_query[rerank_name]=0.0
		rerank_res_per_query=rerank_res_per_query.set_index( 'doc' )
		df=pd.concat([original_res_dict_file[q_id],rerank_res_per_query],axis=1)
		df=df.astype( 'float64' ).apply( np.exp ).astype( 'float64' )
		rerank_res_dict[q_id]=df


def load_result_file_into_df_rerank_residual(origin_q_dic, unified_res_df_dict,original_res_dict_forNRD, NRDstr_rerank_res_path,NRSegstr_rerank_res_path, file_name): ##gets empty unified_res_df_dict and returns unified_res_df_dict  with the results of original and reranking.
	str_res_per_q_NRSeg,queries_no_fb_NRSeg=read_res_from_file_to_dict_rerank(NRSegstr_rerank_res_path,origin_q_dic )
	# #because have both NRD and NRSeg in onew table dropping na here, will chop everything to NRD level. we don't won't that!
	read_res_from_dictionary_to_df_rerank( str_res_per_q_NRSeg, "NRSeg", queries_no_fb_NRSeg, file_name, original_res_dict_forNRD[file_name] )
	unified_res_df_dict[file_name]={}
	unified_res_df_dict[file_name]['NRSeg']=str_res_per_q_NRSeg
	return queries_no_fb_NRSeg



##assign into res_df_dict the full original retrieval results, under 'original'
#res_per_q[q_id]['original_NRD'] will hold the original result after dropping both pos and neg docs (used)
#res_per_q[q_id]['original_NRSeg'] will hold the original result after dropping pos docs (used)
def load_result_file_into_df_orig_residual(origin_q_dic,res_df_dict_NRSeg,  str_res_path):
	res_file=open(str_res_path,'r')
	file_name=str_res_path.split('/')[-1]
	res_per_q_NRSeg={}
	res_per_q={}
	res_per_q=res_per_q.fromkeys(origin_q_dic.keys())
	str_res_per_q={}
	str_res_per_q=str_res_per_q.fromkeys(origin_q_dic.keys(),str()) #this works fine (does not create the same points str. with list[] it does!! do not use for initializing with empty lists
	fb_docs=int( file_name.split( '_' )[0] )
	for line in res_file:
		q_id=line.split()[0]
		str_res_per_q[q_id]+=line
	for q_id in sorted(origin_q_dic.keys()):
		temp_res_df=pd.Series((str_res_per_q[q_id].split('\n')[:-1]))
		splits=temp_res_df.astype('str').str.split()
		q_query_id=pd.DataFrame()
		q_query_id['doc']=splits.str[2]
		q_query_id['original']=splits.str[4]
		q_query_id=q_query_id.set_index('doc')
		res_per_q[q_id]=pd.DataFrame(q_query_id.astype('float64').sort( columns='original', ascending=False ).head(1199 )) # this is the number of working set docs passed( the fbdocs taken from Ql, while the reranking results is based on the expansion which is already in different order (non residual)
		drop_list_NRSeg=list(set(res_per_q[q_id].index).intersection(set(es.Rel_used[fb_docs][q_id])))
		#print drop_list_NRSeg,drop_list_NRD
		res_per_q_NRSeg[q_id]=pd.DataFrame(res_per_q[q_id].index).set_index('doc')
		res_per_q_NRSeg[q_id]['original_NRSeg']=pd.DataFrame( res_per_q[q_id].drop( drop_list_NRSeg, axis=0 ) )
		res_per_q_NRSeg[q_id]=res_per_q_NRSeg[q_id].dropna().sort( columns='original_NRSeg', ascending=False ).head( 1000 )
		# dropping the docs we used and taking 1000 we want to rerank
		#print res_per_q[q_id].sort( columns='original', ascending=False )
	res_df_dict_NRSeg[file_name]= res_per_q_NRSeg
	res_file.close()
	return file_name

def load_result_file_into_df_rerank_normal(origin_q_dic, unified_res_df_dict,original_res_dict, NRSegstr_rerank_res_path, file_name): ##gets empty unified_res_df_dict and returns unified_res_df_dict  with the results of original and reranking.
	res_file_NRSeg=open(NRSegstr_rerank_res_path,'r')
	#rerank_column_name="rerank_setup_"+rerank_setup_name_list[rerank_setup-1]
	res_per_q={}
	res_per_q=res_per_q.fromkeys(origin_q_dic.keys())
	str_res_per_q_NRSeg=dict( (key, str()) for key in origin_q_dic.keys() )
	queries_no_fb_NRSeg=[]
	for line in res_file_NRSeg:
		q_id=line.split( )[0]
		if line.split( )[1] == "Q0":
			str_res_per_q_NRSeg[q_id]+=line
		else:
			queries_no_fb_NRSeg+=[q_id]
	for q_id in sorted(origin_q_dic.keys()):
		if q_id not in queries_no_fb_NRSeg:
			temp_res_df=pd.Series( (str_res_per_q_NRSeg[q_id].split( '\n' )[:-1]) )
			splits=temp_res_df.astype( 'str' ).str.split( )
			q_query_id_NRSeg=pd.DataFrame( )
			q_query_id_NRSeg['doc']=splits.str[2]
			q_query_id_NRSeg['NRSeg']=splits.str[4]
			q_query_id_NRSeg=q_query_id_NRSeg.set_index( 'doc' )
		else:
			# q_query_id=original_res_dict[file_name][q_id].copy(True)
			q_query_id_NRSeg=pd.DataFrame( original_res_dict[file_name][q_id].index )
			q_query_id_NRSeg["NRSeg"]=0.0
			q_query_id_NRSeg=q_query_id_NRSeg.set_index( 'doc' )
		res_per_q[q_id]=pd.concat([original_res_dict[file_name][q_id],q_query_id_NRSeg],axis=1)
		res_per_q[q_id]=res_per_q[q_id]##because have both NRD and NRSeg in one table, dropping na here will chop everything to ##original level. basically there is no reason not to do it here already, but I will review the code to end before changing anything
		#print res_per_q[q_id]
		#print res_per_q[q_id]['original'].dropna().shape,res_per_q[q_id]['NRSeg'].dropna().shape,res_per_q[q_id]['NRD'].dropna().shape
	unified_res_df_dict[file_name]=res_per_q
	df=unified_res_df_dict[file_name]
	res_file_NRSeg.close()
	for q_id in sorted(origin_q_dic.keys()):
		df[q_id]=pd.DataFrame((df[q_id].astype('float64')).apply(np.exp).astype('float64'))
	return queries_no_fb_NRSeg


def print_res_fuse_to_file(res_fuse,output_dir,file_name,alpha,beta):
	indri_file=str()
	for q_id in sorted(origin_q_dic.keys()):
		res_fuse[q_id]['doc_name']=list(res_fuse[q_id].index)
		temp_res=res_fuse[q_id].dropna().astype('float64').sort_index(axis=0, by=['results','doc_name'],ascending=[False, True]).head(1000)
		for rank,(grade,docNo) in enumerate(zip(temp_res['results'],temp_res['results'].index)):
			indri_file+=str(q_id)+' Q0 '+str(docNo)+' '+str(rank+1)+' '+str(grade)+' indri \n'
	f=open(output_dir+file_name+'_'+str(alpha)+'_NRD_'+str(beta)+'_NRSeg','w')##fuse_dir+'res_1000/'
	f.write(indri_file)
	f.close()


def fusion_for_setup_file_normal(df,narrow_single_neg,file_name,alpha_setup,beta_setup,q_no_fb_NRSeg):
	for beta in beta_setup:
		res_fuse={}
		for q_id in sorted(origin_q_dic.keys()):
			if ( beta == 1 and (q_id in q_no_fb_NRSeg)): # #onlynrd and no nrd res for query or only nrseg and no rels for query- backoff to pos.
				res_fuse[q_id]=pd.DataFrame( (df[q_id]["original"].astype( 'float64' )))
			else:
				res_fuse[q_id]=pd.DataFrame((df[q_id]["original"].astype('float64')*(1-beta)-df[q_id]["NRSeg"].astype('float64')*beta))
			res_fuse[q_id].columns=['results']
		print_res_fuse_to_file(res_fuse,narrow_single_neg+'res_1000/',file_name,0,beta)
	return


def fusion_for_setup_file_residual(unified_res_df_dict_perfile,narrow_single_neg,beta_setup,output_file_name,q_no_fb_NRSeg):
	#print file_name
	NRSeg_res_df=unified_res_df_dict_perfile['NRSeg']
	for beta in beta_setup:
		res_fuse={}
		for q_id in sorted( origin_q_dic.keys( ) ):
			if ( beta == 1 and (q_id in q_no_fb_NRSeg)): # #onlynrd and no nrd res for query or only nrseg and no rels for query- backoff to pos.
				res_fuse[q_id]=pd.DataFrame( NRSeg_res_df[q_id]["original_NRSeg"].astype( 'float64' ).astype( 'float64' ) )
			else:
				res_fuse[q_id]=pd.DataFrame( NRSeg_res_df[q_id]["original_NRSeg"].astype( 'float64' ).astype( 'float64' )*(1-beta)-NRSeg_res_df[q_id]["NRSeg"].astype( 'float64' )*beta)
			res_fuse[q_id].columns=['results']
		print_res_fuse_to_file( res_fuse, narrow_single_neg+'res_1000_residual/', output_file_name, 0.0, beta )




def process_fusion_normal(origin_q_dic,original_res_dict,unified_res_df_dict,baseline_home_dir,single_neg_dir,alpha_setup,beta_setup):
	print "normal"
	narrow_single_neg=baseline_home_dir+'/narrow_single_neg/'
	str_NRSeg_res_dir=baseline_home_dir+'/rerank_with_NRSeg/res/'
	res_dir_1000=baseline_home_dir+'eval/res_1000/'
	icount=0
	for res_file in os.listdir(res_dir_1000):
		icount+=1
		print res_file , icount
		if os.path.exists(narrow_single_neg+'/res_1000/'+res_file+'_0.0_NRD_0.0_NRSeg'):
			continue;
		#for NRD_rerank_res_file in os.listdir( str_NRD_res_dir ):
			#if res_file in NRD_rerank_res_file:
		q_no_fb_NRSeg=[]
		NRD_rerank_res_file=res_file+"_NRD"
		NRSeg_rerank_res_file=NRD_rerank_res_file.rstrip( NRD_rerank_res_file.split( '_' )[-1] )+"NRSeg"
		#for NRSeg_rerank_res_file in os.listdir( str_NRSeg_res_dir ):
		#	if res_file in NRSeg_rerank_res_file:
		output_file_name=res_file#+NRD_rerank_res_file[len( res_file ):]+NRSeg_rerank_res_file[len( res_file ):]
		original_res_dict.clear()
		unified_res_df_dict.clear()
		str_res_path=res_dir_1000+res_file
		str_NRSeg_res_path=str_NRSeg_res_dir+NRSeg_rerank_res_file
		file_name=load_result_file_into_df_orig_normal(origin_q_dic,original_res_dict,str_res_path) ## load original
		q_no_fb_NRSeg=load_result_file_into_df_rerank_normal(origin_q_dic, unified_res_df_dict,original_res_dict,str_NRSeg_res_path, file_name)#add rerank
		#print q_no_fb_NRSeg, q_no_fb_NRD
		#print unified_res_df_dict ##checked that the exponent passed.
		fusion_for_setup_file_normal(unified_res_df_dict[file_name],narrow_single_neg,output_file_name,alpha_setup,beta_setup,q_no_fb_NRSeg)#(df,singleneg,file_name,alpha_setup,beta_setup)

def process_fusion_residual(origin_q_dic,original_res_dict,original_res_dict_residuall_All,original_res_dict_residuall_Rel,unified_res_df_dict,baseline_home_dir,home_rerank_dir,alpha_setup,beta_setup):
	print "residual"
	narrow_single_neg=baseline_home_dir+'/narrow_single_neg/'
	str_NRD_res_dir=baseline_home_dir+'/rerank_with_NRD/res/'
	str_NRSeg_res_dir=baseline_home_dir+'/rerank_with_NRSeg/res/'
	res_dir=baseline_home_dir+'res/'
	icount=0
	for res_file in os.listdir(res_dir):
		icount+=1
		print res_file , icount
		if os.path.exists(narrow_single_neg+'/res_1000_residual/'+res_file+'_0.0_NRD_0.0_NRSeg'):
			continue;
		NRD_rerank_res_file=res_file+"_NRD"
		NRSeg_rerank_res_file=NRD_rerank_res_file.rstrip( NRD_rerank_res_file.split( '_' )[-1] )+"NRSeg"
		#	#if res_file in NRSeg_rerank_res_file:
		#original_res_dict.clear( )
		original_res_dict_residuall_All.clear( )
		q_no_fb_NRSeg=[]
		q_no_fb_NRD=[]
		str_res_path=res_dir+res_file
		#load_result_file_into_df_orig_normal( origin_q_dic, original_res_dict, str_res_path ) # # load original
		file_name=load_result_file_into_df_orig_residual( origin_q_dic, original_res_dict_residuall_All,  str_res_path )
		output_file_name=res_file#+NRD_rerank_res_file[len( res_file ):]+NRSeg_rerank_res_file[len( res_file ):]
		unified_res_df_dict.clear()
		str_NRD_res_path=str_NRD_res_dir+NRD_rerank_res_file
		str_NRSeg_res_path=str_NRSeg_res_dir+NRSeg_rerank_res_file
		q_no_fb_NRSeg=load_result_file_into_df_rerank_residual(origin_q_dic, unified_res_df_dict,original_res_dict_residuall_All,str_NRD_res_path,str_NRSeg_res_path,file_name)##inclusing exp
		fusion_for_setup_file_residual(unified_res_df_dict[file_name],narrow_single_neg,beta_setup,output_file_name,q_no_fb_NRSeg)
	return


#baseline_used_doc_for_fixed()
origin_q_dic={}
read_queries_to_dict(str_queries_path,origin_q_dic)



#for setup
##rerank with nrseg:

single_neg=baseline_home_dir+'single_neg/'
narrow_single_neg=baseline_home_dir+'/narrow_single_neg/'
create_folders(single_neg)
create_folders(narrow_single_neg)

original_res_dict={}
original_res_dict_residuall_All={}
original_res_dict_residuall_Rel={}
unified_res_df_dict={}
if flagAll:
	es.getting_rel_docs_from_res_file( max_K, flag_fixed, init_ret_path, binary_qrels_path )
	process_fusion_normal(origin_q_dic,original_res_dict,unified_res_df_dict,baseline_home_dir,single_neg,alpha_setup,beta_setup)
	process_fusion_residual( origin_q_dic, original_res_dict, unified_res_df_dict, original_res_dict_residuall_All, original_res_dict_residuall_Rel, baseline_home_dir, single_neg, alpha_setup, beta_setup )
else:
	if flagNormal:
		process_fusion_normal( origin_q_dic, original_res_dict, unified_res_df_dict, baseline_home_dir, single_neg ,alpha_setup,beta_setup)
	else:
		es.getting_rel_docs_from_res_file( max_K, flag_fixed, init_ret_path, binary_qrels_path )
		process_fusion_residual( origin_q_dic, original_res_dict, unified_res_df_dict,original_res_dict_residuall_All,original_res_dict_residuall_Rel, baseline_home_dir, single_neg ,alpha_setup,beta_setup)

