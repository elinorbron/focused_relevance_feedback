__author__='Elinor'
__author__='Elinor'
"""this is much more efficient than the other Fusion file, ####
but should do the same.
This implementation is correct for the normal evaluation
This implementation is correct for the residual evaluation under the asspmtion that all files used (relevant and non-relevant) in feedback should be removed because they are part of the model, whethere we choose to use them or not (by the leave-one-out-cross-validation)"""
import os
import os.path
import sys
import subprocess
import itertools
import pandas as pd
import numpy as np
import scipy.stats
#print sys.path
scriptpath="/lv_local/home/elinor/pycharm/nf/evaluation/"
sys.path.append(os.path.dirname(scriptpath))
from foo import read_res_from_file_to_dict_rerank as read_res
from foo import read_queries_to_dict as read_queries


#feedback_setup_name_list=['RelSeg','NRSeg','NRD','RelD','PRF']
rerank_setup_name_list=['RelSeg','NRSeg','NRD','RelD','PRF']
alpha_setup=np.arange(0.0,1.2,0.2)
beta_setup=np.arange(0.0,1.2,0.2)

residual_docs_per_query={}
baseline_used_doc_for_fixed_rel={}
#setup="nonFixed"
# #baseline_home_dir='/lv_local/home/elinor/negative_feedback/baselines/Accumulative_RelSeg/'
baseline_home_dir=sys.argv[1]
if len(sys.argv)>2:
	flagNormal= int(sys.argv[2])
	flagAll=0
else:
	flagAll=1

home_dir_input=sys.argv[1]+'/'
full_setup_name=home_dir_input.rstrip( '/' ).rstrip( '/' ).split( '/' )[-1]
setup=full_setup_name.split( '_' )[0]
model=home_dir_input.rstrip( '/' ).rstrip( '/' ).split( '/' )[-2]
print "sampling setup", setup
print "model", model
print "full_setup_name", full_setup_name

#print "setup input is"+setup
flag_residual=True

if setup=="Fixed":
	print "Fixed"
	flag_fixed=True
	max_K=10
else:
	print "NonFixed"
	flag_fixed=False
	max_K=5
################

str_queries_path='/lv_local/home/elinor/negative_feedback/Inex_qrelsAndParams/INEX_params_b_segQ.xml'
init_ret_path='/lv_local/home/elinor/negative_feedback/QL/ql_ret_INEX.txt'
binary_qrels_path='/lv_local/home/elinor/negative_feedback/Inex_qrelsAndParams/inex2009-2010-article.qrels_binary'


def create_folders(home_rerank_dir):
	home_rerank_pos=home_rerank_dir
	if not os.path.exists(home_rerank_pos):
		os.makedirs(home_rerank_pos)
	if not os.path.exists( home_rerank_pos+'res_1000_correction/' ):
		os.makedirs(home_rerank_pos+'res_1000_correction/')
	if not os.path.exists( home_rerank_pos+'res_1000_residual_correction/' ):
		os.makedirs(home_rerank_pos+'res_1000_residual_correction/')






def correct_extream_cases(sourceresdir,backoffdir,baseline_home_dir):
	global queries_dict
	correction_dir=sourceresdir.rstrip('/')+'_correction/'
	rerank_nrd=baseline_home_dir+'rerank_with_NRD/res/'
	rerank_nrseg=baseline_home_dir+'rerank_with_NRSeg/res/'
	for f in os.listdir(sourceresdir):
		if '_1.0_NRD_0.0_NRSeg' in f: #all NRD
			original_fname=f[:-len('_1.0_NRD_0.0_NRSeg')]
			_,queries_no_NRD=read_res(rerank_nrd+original_fname+'_NRD',queries_dict)
			str_corrected=""
			res_backoff,_=read_res(backoffdir+'/'+original_fname,queries_dict)
			res_signeg,_=read_res(sourceresdir+f,queries_dict)
			for query in sorted(queries_dict.keys()):
				if query in queries_no_NRD:
					str_corrected+=res_backoff[query]
				else:
					str_corrected+=res_signeg[query]
			corectedfile=open(correction_dir+f,'w')
			corectedfile.write(str_corrected)
			corectedfile.close()
		if '_0.0_NRD_1.0_NRSeg' in f: #all NRSeg
			print rerank_nrseg+f[:-len('_0.0_NRD_1.0_NRSeg')]+'_NRSeg'
			#return
			original_fname=f[:-len('_0.0_NRD_1.0_NRSeg')]
			_,queries_no_NRSeg=read_res(rerank_nrseg+original_fname+'_NRSeg',queries_dict)
			str_corrected=""
			res_backoff,_=read_res(backoffdir+'/'+original_fname,queries_dict)
			res_signeg,_=read_res(sourceresdir+f,queries_dict)
			for query in sorted(queries_dict.keys()):
				if query in queries_no_NRSeg:
					str_corrected+=res_backoff[query]
				else:
					str_corrected+=res_signeg[query]
			corectedfile=open(correction_dir+f,'w+')
			corectedfile.write(str_corrected)
			corectedfile.close()





single_neg=baseline_home_dir+'single_neg/'
create_folders(single_neg)
backoff_res_dict={}
global queries_dict
queries_dict={}
read_queries(str_queries_path,queries_dict)
if flagAll:
	correct_extream_cases(single_neg+'res_1000/',baseline_home_dir+'/eval/res_1000/',baseline_home_dir)
	correct_extream_cases(single_neg+'res_1000_residual/',baseline_home_dir+'/eval/residual_collection_res_1000/' ,baseline_home_dir)
else:
	if flagNormal:
		correct_extream_cases( single_neg+'res_1000/',baseline_home_dir+'/eval/res_1000/',baseline_home_dir)
	else:
		correct_extream_cases( single_neg+'res_1000_residual/', baseline_home_dir+'/eval/residual_collection_res_1000/' ,baseline_home_dir)

