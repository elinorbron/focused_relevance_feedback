__author__='Yojik'
import os
import os.path
import sys
import subprocess
import re

def read_split(path_doc):
	f=open(path_doc,'r')
	text=f.read()
	f.close()
	#print text.split('<relrelbegin>')
	#print text.split('<relrelend>')
	relbegin= text.split('<relrelbegin>')
	rellen=[]
	nonrel=[]
	all=[len(relbegin[0])]
	nonrel+=[len(relbegin.pop(0))]
	#print nonrel
	for portion in relbegin:
		rellen+=[len(portion.split('<relrelend>')[0])]
		all+=[len(portion.split('<relrelend>')[0])]
		nonrel+=[len(portion.split('<relrelend>')[1])]
		all+=[len(portion.split('<relrelend>')[1])]
		#print "*\n"
		#print portion.split('<relrelend>')[0]
		#print "*\n"
		#print portion.split('<relrelend>')[1]
	#print rellen
	#print nonrel
	#print all
	pointer=0
	relseg=[]
	for i in xrange(0,len(all),1):
		#print i
		if (i)%2:
			#print "true",all[i],pointer
			relseg+=[(pointer,pointer+all[i])]
		pointer+=all[i]
	return relseg,sum(all),sum(rellen)






def overlap(intervalA,intervalB):
	overlap_len= max( 0, min( intervalA[1], intervalB[1] )-max( intervalA[0], intervalB[0] ) )
	if overlap_len:
		return  overlap_len,(max( intervalA[0], intervalB[0]) , min( intervalA[1], intervalB[1] ))
	else:
		return 0

a=[(245, 368), (1792, 2006)]
b=[(270, 300), (500, 506),(1992, 2806)]
def segments_overlap(useraSEG,userbSEG):
	overlapLEN=0
	final_segments=[]
	for i in useraSEG:
		for j in userbSEG:
			res=overlap(i,j)
			if res:
				overlapLEN+=res[0]
				final_segments+=[res[1]]
	return overlapLEN,final_segments


#print segments_overlap(a,b)


def user_overlap(doc1,doc2):
	docname1=doc1.split('/')[-1].rstrip(doc1.split('_')[-1])
	docname2=doc2.split('/')[-1].rstrip(doc2.split('_')[-1])
	user1name=doc1.split('_')[-1]
	user2name=doc2.split('_')[-1]
	if (docname1==docname2) and (user1name!=user2name):
		user1_segments,doclen1,rellen1=read_split(doc1)
		user2_segments,doclen2,rellen2=read_split(doc2)
		if doclen1!=doclen2:
			print 'error'
		conjuctionLen,final_segments=segments_overlap(user1_segments,user2_segments)
		#print rellen2,rellen1,conjuctionLen
		if rellen1==0 and rellen2==0:
			return 1.0, final_segments
		else:
			return float(conjuctionLen)/float(max(rellen1+rellen2-conjuctionLen,1)),final_segments
	else:
		return "nomatch"


def user_agreement(diruser1,diruser2):
	documents_compares=0
	score=[]
	dict={}
	user1name=diruser1.rstrip('/').split('/')[-1]
	user2name=diruser2.rstrip('/').split('/')[-1]
	for doc1 in os.listdir(diruser1):
		for doc2 in os.listdir(diruser2):
			res=user_overlap(diruser1+'/'+doc1,diruser2+'/'+doc2)
			if not res=="nomatch":
				documents_compares+=1
				score+=[res[0]]
				dict[doc1.rstrip('_'+doc1.split('_')[-1])]=[res[0],res[1]]
	average_score=float(sum(score))/float(max(1,len(score)))
	return user1name,user2name, documents_compares,average_score,dict

print user_agreement('/lv_local/home/elinor/users_docs/user1/','/lv_local/home/elinor/users_docs/user2/')
print user_agreement('/lv_local/home/elinor/users_docs/user1/','/lv_local/home/elinor/users_docs/user3/')
print user_agreement('/lv_local/home/elinor/users_docs/user1/','/lv_local/home/elinor/users_docs/user4/')
print user_agreement('/lv_local/home/elinor/users_docs/user2/','/lv_local/home/elinor/users_docs/user3/')
print user_agreement('/lv_local/home/elinor/users_docs/user2/','/lv_local/home/elinor/users_docs/user4/')
print user_agreement('/lv_local/home/elinor/users_docs/user3/','/lv_local/home/elinor/users_docs/user4/')
print user_agreement('/lv_local/home/elinor/users_docs/user4/','/lv_local/home/elinor/users_docs/user4/')