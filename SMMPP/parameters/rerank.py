__author__='Elinor'
import os
import os.path
import sys
import subprocess
import itertools
scriptpath="/lv_local/home/elinor/pycharm/nf/evaluation/"
sys.path.append(os.path.dirname(scriptpath))
from qpos_eval import eval_settings as es


#define paths
feedback_setup_name=['RelSeg','NRSeg','NRD','RelD','PRF']

#initial_feedback_setup=1
#baseline_home_dir="/lv_local/home/elinor/negative_feedback/baselines/Accumulative_RelSeg"+'/'
home_dir_input=sys.argv[1]+'/'
full_setup_name=home_dir_input.rstrip('/').rstrip('/').split('/')[-1]
sampling_setup=full_setup_name.split('_')[0]
model=home_dir_input.rstrip('/').rstrip('/').split('/')[-2]
print "sampling setup", sampling_setup
print "model", model
print "full_setup_name", full_setup_name
flag_residual=True

if model != 'SMMPP': ##does the same as SMMP..except that I moved the indexes in the names, could be easily united.
	print "Error- wrong model!!!"
else:
	initial_feedback_setup_name=full_setup_name.lstrip(sampling_setup)


rerank_setups=[2,3]
if sampling_setup=="Fixed":
	flag_fixed=True
	max_K=10
elif sampling_setup=="Accumulative":
	flag_fixed=False
	max_K=5

start_param_file_path="/lv_local/home/elinor/negative_feedback/Inex_qrelsAndParams/param_part1_dropping_origQW.xml" ##using SMM
orig_queries="/lv_local/home/elinor/negative_feedback/Inex_qrelsAndParams/INEX_params_b_segQ.xml"

init_ret_path='/lv_local/home/elinor/negative_feedback/QL/ql_ret_INEX.txt'
qrels_path='/lv_local/home/elinor/negative_feedback/Inex_qrelsAndParams/inex2009-2010-article.qrels_binary'
baseline_res=home_dir_input+'res'+'/'
##any number bigger than the amount of docs we are using for feedback

fbDocs=range(1,max_K+1)
fbOrigWeight=[0.2,0.5,0.8] #for file name only. within we pass
weights=[0,0.1,0.5,0.9]
lambda1=lambda2=0
fbTerms=[10,25,50]


def define_paths(rerank_setup):
	global home_dir
	global output_param_files_dir
	home_dir=home_dir_input+'rerank_with_'+feedback_setup_name[rerank_setup-1]+'/'
	output_param_files_dir=home_dir+"parameters/"

def create_dir():
	if not os.path.exists(home_dir):
		os.makedirs(home_dir)
	l=["parameters/","res/","chunks/"]
	for dir in l:
		if not os.path.exists(home_dir+dir):
			os.makedirs(home_dir+dir)

#define arg



def read_query_info(orig_queries):
	#step one- for each query create a dictionary of the query words.
	global q_info
	q_info={}
	q_file=open(orig_queries,'r')
	query_str_a='\t<query>\n'
	query_str_b=str()
	for line in q_file:
		if 'number' in line:
			q_id=line.strip('\t\t<number>')
			q_id=q_id.rstrip('</number>\n\t\t')
			query_str_a+=line;
		if '<text>' in line:
			query_str_a+=line;
		if '<relseg>' in line:
			query_str_b+=line;
		if '</query>' in line:
			query_str_b+=line;
			q_info[q_id]=[query_str_a,query_str_b]
			query_str_a='\t<query>\n'
			query_str_b=str()
	q_file.close()
	return


def create_parameters_accumulative():
	read_query_info(orig_queries)
	es.getting_rel_docs_from_res_file( max_K, flag_fixed, init_ret_path, qrels_path )
	for rerank_setup in rerank_setups:
		define_paths( rerank_setup )
		create_dir( )
		#start code:
		#i=0;
		start_param_file=open(start_param_file_path,'r')
		start_param=start_param_file.read()
		start_param_file.close()
		for file in os.listdir(baseline_res):
			this_input_name=file
			fbDocs_parm=int(file.split('_')[0])
			fbOrigWeight_parm=float(file.split('_')[1])
			lambda3=float(file.split('_')[-5])
			term=int(file.split('_')[-4])
			mixtureCorpusWeight_param_t=lambda1+lambda2+lambda3 # lambda bg
			#writing parameter file part 1
			this_output=str()
			print this_input_name
			this_output_name=this_input_name+'_'+feedback_setup_name[rerank_setup-1]
			this_output_path=output_param_files_dir+this_output_name
			this_output+=start_param
			this_output+='\t<mixtureCorpusWeight>'+str(mixtureCorpusWeight_param_t)+'</mixtureCorpusWeight>\n'
			this_output+='\t<fbTerms>'+str(term)+'</fbTerms>\n'
			this_output+='\t<fbOrigWeight>'+str(0)+'</fbOrigWeight>\n'
			this_output+='\t<feedback_setup>'+str(rerank_setup)+'</feedback_setup>\n'
			this_output+='\t<fbDocs>'+str(10)+'</fbDocs>\n'
			for query in sorted(q_info.keys()):
				this_output+=q_info[query][0]
				if rerank_setup == 2:
					for docNo in es.Rel_used[fbDocs_parm][query]:
						this_output+='\t\t<feedbackDocno>'+str(docNo)+'</feedbackDocno>\n'
				elif rerank_setup == 3:
					for docNo in es.All_used[fbDocs_parm][query]:
						this_output+='\t\t<feedbackDocno>'+str( docNo )+'</feedbackDocno>\n'
				batch_cut_top='grep "'+query+' Q0 " '+baseline_res+this_input_name+" | awk '{ if ($4<1200) print $3;}'"
				output2 = subprocess.check_output(batch_cut_top, shell=True)
				for Docno in output2.rstrip('\n').split('\n'):
					this_output+='\t\t<workingSetDocno>'+Docno+'</workingSetDocno>\n'
				this_output+=q_info[query][1]
			this_output+="</parameters>\n"
			out_f=open(this_output_path,'w+')
			out_f.write(this_output)
			out_f.close()
		print "done"
	return 1

def create_parameters_fixed():
	read_query_info(orig_queries)
	es.getting_rel_docs_from_res_file( max_K, flag_fixed, init_ret_path, qrels_path )
	for rerank_setup in rerank_setups:
		define_paths( rerank_setup )
		create_dir( )
		#start code:
		#i=0;
		start_param_file=open(start_param_file_path,'r')
		start_param=start_param_file.read()
		start_param_file.close()
		for file in os.listdir(baseline_res):
			this_input_name=file
			fbDocs_parm=int(file.split('_')[0])
			fbOrigWeight_parm=float(file.split('_')[1])
			lambda3=float(file.split('_')[-5])
			term=int(file.split('_')[-4])
			mixtureCorpusWeight_param_t=lambda1+lambda2+lambda3 # lambda bg
			#writing parameter file part 1
			this_output=str()
			print this_input_name
			this_output_name=this_input_name+'_'+feedback_setup_name[rerank_setup-1]
			this_output_path=output_param_files_dir+this_output_name
			this_output+=start_param
			this_output+='\t<mixtureCorpusWeight>'+str(mixtureCorpusWeight_param_t)+'</mixtureCorpusWeight>\n'
			this_output+='\t<fbTerms>'+str(term)+'</fbTerms>\n'
			this_output+='\t<fbOrigWeight>'+str(0)+'</fbOrigWeight>\n'
			this_output+='\t<feedback_setup>'+str(rerank_setup)+'</feedback_setup>\n'
			this_output+='\t<fbDocs>'+str(fbDocs_parm)+'</fbDocs>\n'
			for query in sorted( q_info.keys( ) ):
				if rerank_setup == 2:
					D_f=es.Rel_used[fbDocs_parm][query]
				elif rerank_setup == 3:
					D_f=es.Neg_used[fbDocs_parm][query]
				if len( D_f ) >= 1:
					this_output+=q_info[query][0]
					for docNo in D_f:
						this_output+='\t\t<feedbackDocno>'+str( docNo )+'</feedbackDocno>\n'
					batch_cut_top='grep "'+query+' Q0 " '+baseline_res+this_input_name+" | awk '{ if ($4<1200) print $3;}'"
					output2=subprocess.check_output( batch_cut_top, shell=True )
					for Docno in output2.rstrip( '\n' ).split( '\n' ):
						this_output+='\t\t<workingSetDocno>'+Docno+'</workingSetDocno>\n'
					this_output+=q_info[query][1]
			this_output+="</parameters>\n"
			out_f=open(this_output_path,'w+')
			out_f.write(this_output)
			out_f.close()
		print "done"
	return 1

if flag_fixed:
	create_parameters_fixed( );
else:
	create_parameters_accumulative();
