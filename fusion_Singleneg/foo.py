__author__='Elinor'


def read_queries_to_dict( str_path, origin_q_dic ):
	f=open(str_path,'r')
	for line in f:
		if 'number' in line:
			q_id=line.strip('\t\t<number>')
			q_id=q_id.rstrip('</number>\n\t\t')
			origin_q_dic[q_id]=str()
	return origin_q_dic




def read_res_from_file_to_dict_rerank(filepath,origin_q_dic):
	res_file=open( filepath, 'r' )
	dict_res_str=dict( (key, str()) for key in origin_q_dic.keys() )
	queries_no_fb=[]
	for line in res_file:
		q_id=line.split( )[0]
		if line.split( )[1] == "Q0":
			dict_res_str[q_id]+=line
		else:
			queries_no_fb+=[q_id]
	return dict_res_str,queries_no_fb
