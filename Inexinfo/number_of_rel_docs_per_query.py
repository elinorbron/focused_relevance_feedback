__author__='Elinor'
import numpy as np
file=open("/lv_local/home/elinor/negative_feedback/Inex_qrelsAndParams/inex2009-2010-article.qrels_binary","r")
rels={}
doclist={}

for line in file:
	q_id=line.split()[0]
	if q_id[0:4]=="2010":
		if q_id not in rels.keys():
			rels[q_id]=0
			doclist[q_id]=[]
		if line.split()[3]=="1":
			doc=line.split()[2]
			if doc not in doclist[q_id]:
				rels[q_id]+=1
				doclist[q_id]+=[doc]

print len(rels.keys())
print np.mean(rels.values())
print rels.values()
print np.std(rels.values())
