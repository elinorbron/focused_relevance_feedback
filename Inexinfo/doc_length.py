__author__='Elinor'

import numpy as np
file=open("/lv_local/home/elinor/docvectors","r")
allterms={}
no_sw={}

for line in file:
	docNO=line.split()[0]
	allterms[docNO]=int(line.split()[2])
	no_sw[docNO]=int(line.split()[3])

print "************all terms"+"*"*10
print len(allterms.keys())
print np.mean(allterms.values())
#print allterms.values()
print np.std(allterms.values())


print "************without stopwords"+"*"*10
print len(no_sw.keys())
print np.mean(no_sw.values())
#print no_sw.values()
print np.std(no_sw.values())


