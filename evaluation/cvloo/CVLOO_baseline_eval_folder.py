"""
in eval folders
"""

import os
import os.path
from os import listdir
from os.path import isfile, join
import os
import sys
import itertools
import subprocess

backoff={"noPos+Neg_NRD":"NRD","Pos+Neg_NRD":"Pos","Pos+noNeg_NRD":"Pos","Pos+Neg_NRSeg":"Pos","Pos+noNeg_NRSeg":"Pos","only_Pos":"Pos"}

def separate_to_dirs_for_model_type(modeltype):
	queries_per_cat={}
	try:
		if modeltype == "Accu":
			Pcategories=["pos"]
			parameters_set_if_cat=""
			queries_per_cat["pos"]=Qstatk["all"]
		elif modeltype == "Fixed":
			Pcategories=["pos"]
			parameters_set_if_cat=""
			queries_per_cat["pos"]=Qstatk["Pos"]
		elif modeltype=="posonly":
			Pcategories=["pos","noPos"]
			parameters_set_if_cat=""
			queries_per_cat["pos"]=Qstatk["Pos"]
			queries_per_cat["noPos"]=Qstatk["noPos"] ##nopos should take QL and should all be the same!!!!
		elif modeltype=="pos+NRD":
			parameters_set_if_cat=["Pos+Neg_NRD"]##this is a special case. in this case we will might use the condition in "parameter set"
			parameters_set={"Pos+Neg_NRD":"0.2_NRD_0.0_NRSeg"}
			Pcategories=["noPos+Neg_NRD", "Pos+Neg_NRD", "Pos+noNeg_NRD"]
			queries_per_cat["Pos+Neg_NRD"]=sorted(list(set(Qstatk["Pos"]).intersection(set(Qstatk["NRD"]))))
			queries_per_cat["noPos+Neg_NRD"]=sorted(list(set(Qstatk["noPos"]).intersection(set(Qstatk["NRD"]))))
			queries_per_cat["Pos+noNeg_NRD"]=sorted(list(set(Qstatk["Pos"]).difference(set(Qstatk["NRD"]))))
		elif modeltype=="pos+NRSeg":
			parameters_set_if_cat=["Pos+Neg_NRSeg"]# #this is a special case. in this case we will might use the condition in "parameter set"
			parameters_set={"Pos+Neg_NRSeg":"0.0_NRD_0.2_NRSeg"}
			Pcategories=["Pos+Neg_NRSeg", "Pos+noNeg_NRSeg","noPos"]
			queries_per_cat["Pos+Neg_NRSeg"]=sorted( list( set( Qstatk["Pos"] ).intersection( set( Qstatk["NRSeg"] ) ) ) )
			queries_per_cat["noPos"]=Qstatk["noPos"] ##nopos should take QL and should all be the same!!!!
			queries_per_cat["Pos+noNeg_NRSeg"]=sorted( list( set( Qstatk["Pos"] ).difference( set( Qstatk["NRSeg"] ) ) ) )
		elif modeltype=="full":
			Pcategories=["Pos+Neg_NRSeg","noPos+Neg_NRD","only_Pos","Pos+Neg_NRD", "Pos+Neg_all"]
			parameters_set_if_cat=["Pos+Neg_NRSeg","Pos+Neg_NRD"]# #this is a special case. in this case we will might use the condition in "parameter set"
			parameters_set={"Pos+Neg_NRSeg":"0.0_NRD_0.2_NRSeg","Pos+Neg_NRD":"0.2_NRD_0.0_NRSeg"}
			for category in Pcategories:
				queries_per_cat[category]=Qstatk[category]
		else:
			raise TypeError("ModeltypeMismatch")
		overlap= set()
		for cat1 in Pcategories:
			for cat2 in Pcategories:
				if (cat1!=cat2) and (len(set(queries_per_cat[cat1]).intersection(set(queries_per_cat[cat2])))>0):
					print cat1, cat2
					print len( set( queries_per_cat[cat1] ).intersection( set( queries_per_cat[cat2] ) ) )
					print set( queries_per_cat[cat1] ).intersection( set( queries_per_cat[cat2] ) )
					raise TypeError("Category_overlap")
	except TypeError("modeltype_Mismatch"):
		print modeltype+" modeltype not found"
	except TypeError("Category_overlap"):
		print cat1,cat2
		print len(set(queries_per_cat[cat1]).intersection(set(queries_per_cat[cat2])))
		print set(queries_per_cat[cat1]).intersection(set(queries_per_cat[cat2]))

	for category in queries_per_cat.keys():
		try:
			if (len(queries_per_cat[category])>5) or ((category=="noPos") and (len(queries_per_cat[category])>0) ): ##noPos alone should be only QL therefore there is no real learning of parameters. therefore the group size is not relevant.
				"this case is all pure! create queries files for pure-category directory"
				pureCatDir= output_dir+'/pure/'+category+'/'
				if not os.path.exists( pureCatDir ):
					os.makedirs(pureCatDir )
				for evalfile in os.listdir(source_dir):
					batch_eval="grep -w '"+'\|'.join(queries_per_cat[category])+"' "+source_dir+evalfile+" | grep -w 'map\|P5 ' > "+pureCatDir+evalfile
					subprocess.check_output( batch_eval, shell=True )
			elif len(queries_per_cat[category])>0:
				"this case is not pure! create queries files for the dammi-category directory by adding the backoff queries, and keep a list of pure queries"
				dammiCatDir=output_dir+'/dammi/'+category+'/'
				dammiResCatDir=output_dir+'/cvloo_dammi/'+category+'/'
				if not os.path.exists( dammiCatDir ):
					os.makedirs( dammiCatDir )
				for evalfile in os.listdir( source_dir ):
					if category in parameters_set_if_cat :
						# meaning we will only work with files that fit the parameters discussed
						if parameters_set[category] not in evalfile:
							continue
					batch_eval="grep -w '"+'\|'.join( Qstatk[backoff[category]] )+"' "+source_dir+evalfile+" | grep -w 'map\|P5 ' > "+dammiCatDir+evalfile
					subprocess.check_output( batch_eval, shell=True )
				cv_loo_dammi_dir(dammiCatDir,reported_param_list,dammiResCatDir,queries_per_cat[category] )
		except:
			print "erorr in  separate_to_dirs_for_model_type"
			print category, len( queries_per_cat[category] )
		#print "finished category " +category



def cv_loo_onedir(source_dir,reported_param_list,dest_dir):
	l=[]
	list_of_eval_files=str( )
	if not os.path.exists( dest_dir ):
		os.makedirs( dest_dir )
	for file in os.listdir( source_dir ):
		list_of_eval_files+=source_dir+file+'\n'
	for reported_param in reported_param_list:
		leval_file_path=dest_dir+'list_of_eval_files'+reported_param+'.'+optimization_param+'.txt'
		l_file=open( leval_file_path, 'w+' )
		l_file.write( list_of_eval_files )
		l_file.close( )
		dest_file=dest_dir+"cvloo_res_"+reported_param+'_'+optimization_param+".txt"
		temporal_folder=dest_dir+'tempfolder.'+reported_param+'.'+optimization_param
		batch_eval=anna_Script_path+" "+leval_file_path+" "+reported_param+' '+optimization_param+" "+binary_qrels_path+" "+temporal_folder+" > "+dest_file
		output1=subprocess.check_output( batch_eval, shell=True )
		l+=[dest_file]
	return l

def cv_loo_pure_dirs(pure_mother_dir,reported_param_list,pure_res_mother_dir):
	for source_dir in os.listdir(pure_mother_dir):
		dest_dir=pure_res_mother_dir+source_dir+'/'
		cv_loo_onedir(pure_mother_dir+source_dir+'/',reported_param_list,dest_dir)

def cv_loo_dammi_dir(dammiCatDir,reported_param_list,dammiResCatDir,purequeries):
	res_files_l=cv_loo_onedir(dammiCatDir,reported_param_list,dammiResCatDir)
	for dest_file in res_files_l:
		batch_cptemp="cp "+dest_file+" "+dest_file+'.temp'
		subprocess.check_output( batch_cptemp, shell=True )
		batch_eval="grep -w "+"'"+'\|'.join( purequeries )+"' " +dest_file+'.temp'+" > "+dest_file
		subprocess.check_output( batch_eval, shell=True )
	return

def combining_results_to_dest_dir(dir1 ,dir2,dest_file,reported_param):
	list_to_concat=[]
	for dir in os.listdir(dir1):
		input_file=dir1+dir+"/cvloo_res_"+reported_param+'_'+optimization_param+".txt"
		list_to_concat+=[input_file]
	for dir in os.listdir(dir2):
		input_file=dir2+dir+"/cvloo_res_"+reported_param+'_'+optimization_param+".txt"
		list_to_concat+=[input_file]
	if len(list_to_concat)>0:
		batch_cat="cat "+' '.join( list_to_concat )+"  > "+dest_file
		subprocess.check_output( batch_cat, shell=True )

def define_paths_and_arguments(binary_qrels_pathout):
	global binary_qrels_path
	global anna_Script_path
	anna_Script_path='/lv_local/home/elinor/negative_feedback/scripts_in_use/LOO_CV_list_effective.sh'
	global optimization_param
	optimization_param="map"
	global reported_param_list
	reported_param_list=["map", "P5"]
	global qrels_path_input
	binary_qrels_path=binary_qrels_pathout
	#init_ret_path='/lv_local/home/elinor/negative_feedback/baselines/QL/ql_ret_INEX.txt'
	#binary_qrels_path='/lv_local/home/elinor/negative_feedback/Inex_qrelsAndParams/inex2009-2010-article.qrels_binary'
	trec_file="/lv_local/home/elinor/trec_eval.8.1/trec_eval"
	##
"""
This is the main function of the file.
defines paths and setups and all.
call the other functions that:
1. separate to different files and folder by queries
2. cvloo per query set
3. keep only relevant results
4. unite them
than like it use to work- have the files in a folder of cvloo and read from them everything needed for the final report (returns the arguments to fb_separate)
"""
#modeltype="posonly"## "posonly" or "pos+NRD" or "pos+NRSeg" or "full"##################################################################################################
def cv_loo(source_dirp,output_dirp,Qstat,modeltype,k,binary_qrels_pathout):
	###defining paths
	define_paths_and_arguments(binary_qrels_pathout)
	global Qstatk
	Qstatk=Qstat
	global output_dir
	output_dir=output_dirp
	global source_dir
	source_dir=source_dirp
	##
	if not os.path.exists( output_dir+'/pure/' ):
		os.makedirs( output_dir+'/pure/' )
	if not os.path.exists( output_dir+'/dammi/' ):
		os.makedirs( output_dir+'/dammi/' )
	if not os.path.exists( output_dir+'/cvloo_pure/' ):
		os.makedirs( output_dir+'/cvloo_pure/' )
	if not os.path.exists( output_dir+'/cvloo_dammi/' ):
		os.makedirs( output_dir+'/cvloo_dammi/' )
	separate_to_dirs_for_model_type(modeltype)
	cv_loo_pure_dirs(output_dir+'/pure/',reported_param_list,output_dir+'/cvloo_pure/')
	#combining_results_to_dest_dir()
	##normal
	dict_res={}
	for reported_param in reported_param_list:
		try:
			dest_file=output_dir+"cvloo_res_"+reported_param+'_'+optimization_param+".txt"
			combining_results_to_dest_dir(output_dir+'/cvloo_pure/' ,output_dir+'/cvloo_dammi/',dest_file,reported_param)
			batch_test="more "+dest_file+""" | wc """
			outputtest=subprocess.check_output( batch_test, shell=True )
			if (outputtest.split()[0]!="120") and (not( modeltype == "Fixed")):
				raise TypeError("number of queries doesnt match Inex after combining")
		except:
			print "number of queries doesnt match Inex after combining"
			print outputtest.split()[0]
			print k
			print dest_file
		batch_sum="more "+dest_file+""" | awk '{sum+=$2;c++;}END{print sum/c;}'"""
		output1=subprocess.check_output( batch_sum, shell=True )
		final_res_str="reported_param "+reported_param+",optimization_param "+optimization_param+':'+output1+'\n'
		dict_res[reported_param]=output1.rstrip("\n")
		#make sure that we have the cvloo info
		cvloo_res_file=open(dest_file,'r')
		query_measure_val=[]
		query_grade_file=str()
		for line in cvloo_res_file:
			grade=float(line.split()[1])
			query_grade_file+=" ".join(line.split()[:1])+'\n'
			query_measure_val+=[grade]
		dict_res[reported_param+'_file']=query_grade_file
		dict_res[reported_param+'_Glist']=query_measure_val
		if not os.path.exists( output_dir+"CVLOO_avg_on_query.csv" ):
			final_res=open( output_dir+"CVLOO_avg_on_query.csv", "w" )
		if os.path.exists( output_dir+"CVLOO_avg_on_query.csv" ):
			final_res=open( output_dir+"CVLOO_avg_on_query.csv", "a" )
		final_res.write( final_res_str )
		final_res.close( )
	return dict_res




def cv_loo_normal(home_dir_path):
	home_dir=home_dir_path
	source_dir=home_dir+'/normal_eval/'+'/'
	dest_dir=home_dir+'/normal_eval_cvloo/'
	cv_loo( source_dir, dest_dir )
	print "done for: "+source_dir

def cv_loo_residual(home_dir_path):
	home_dir=home_dir_path
	source_dir=home_dir+'/residual_collection_eval'+'/'
	dest_dir=home_dir+'/residual_collection_cvloo/'
	cv_loo( source_dir, dest_dir )
	print "done for: "+source_dir

















