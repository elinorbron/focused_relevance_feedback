import eval_settings as es



__author__='Elinor'
import os
import subprocess



def chop_to_top_1000(source_dir,dest_dir):
	for res_file in os.listdir( source_dir ):
		batch_eval1="more "+source_dir+res_file+""" | awk '{if ($4<=1000) print;}' > """+dest_dir+res_file
		output1=subprocess.check_output( batch_eval1, shell=True )
	return

def trec_eval_normal_on_1000(source_dir,dest_dir):
	for res_file in os.listdir( source_dir ):
		batch_eval2=es.trec_file+" -q "+es.qrels_path+" "+source_dir+res_file+ " > "+es.path_cut_res_normal+res_file
		output1 = subprocess.check_output(batch_eval2, shell=True)
	return

def  QL_residual_eval():
	##create QL residual at 1000
	for res_file in os.listdir( es.ql_residual_res ):
		k=res_file.split( '_' )[0]
		out_file=es.ql_residual_res_1000+res_file
		batch_eval1="more "+es.ql_residual_res+res_file+""" | awk '{if ($4<=1000) print;}' > """+out_file
		output1=subprocess.check_output( batch_eval1, shell=True )
		new_qrels_file_path=es.qrels_dir+str( k )+'_'+es.baseline_home_dir.rstrip( '/' ).split( '_' )[-1]+'_residual_collection.qrels_binary'
		out_file_eval=es.ql_residual_eval+res_file
		batch_eval2=es.trec_file+" -q "+new_qrels_file_path+" "+out_file+" > "+out_file_eval+'_eval'
		output1=subprocess.check_output( batch_eval2, shell=True )
	print "QL _residual for comparison -files are formed\n"

def eval_residual():
	##create res_files residual at 1000
	for res_file in os.listdir( es.ret_res_dir ):
		k=res_file.split( '_' )[0]
		batch_eval1="more "+es.ret_res_dir+res_file+""" | awk '{if ($4<=1000) print;}' > """+es.path_cut_res+res_file
		output1=subprocess.check_output( batch_eval1, shell=True )
		new_qrels_file_path=es.qrels_dir+str( k )+'_'+es.baseline_home_dir.rstrip( '/' ).split( '_' )[-1]+'_residual_collection.qrels_binary'
		batch_eval2=es.trec_file+" -q "+new_qrels_file_path+" "+es.path_cut_res+res_file+" > "+es.eval_dir_residual+res_file
		output1=subprocess.check_output( batch_eval2, shell=True )
	print "eval _residual for top 1000 in res files"

def eval_residual_chopped(source_dir_1000,dest_dir,Qrels_dir):
	for res_file in os.listdir( source_dir_1000 ):
		k=res_file.split( '_' )[0]
		new_qrels_file_path=Qrels_dir+str( k )+'_'+es.baseline_home_dir.rstrip( '/' ).split( '_' )[-1]+'_residual_collection.qrels_binary'
		batch_eval2=es.trec_file+" -q "+new_qrels_file_path+" "+source_dir_1000+res_file+" > "+dest_dir+res_file
		output1=subprocess.check_output( batch_eval2, shell=True )
	print "eval _residual for top 1000 in res files"


def sum_data_dir(source_dir,measure,flag_top,flag_summary,dest_dir,name_init,max_K):##'"P5 "'
	if flag_top:
		templist=[]
		for j in xrange(max_K):
			tempfile=dest_dir+name_init+'_'+measure+'_'+str(j+1)+'_temp_list.txt'
			batch_grep="grep "+measure+" "+source_dir+'/'+str(j+1)+"""_* > """+tempfile
			output1=subprocess.check_output( batch_grep, shell=True )
			templist+=[tempfile]
		batch_grep="cat "+" ".join(templist)+""" | grep all | sort -g -k3 | tail -n 1 |  awk '{print $1"  "$3;}'"""
		output1=subprocess.check_output( batch_grep, shell=True )
		out=measure +" max in "+  source_dir+": "+output1
		file=open(dest_dir+name_init+'_'+"top",'a')
		file.write(out)
		file.close()
		if flag_summary:
			batch_grep="cat "+" ".join( templist )+""" | grep all | sort -g -k3  > """+dest_dir+name_init+'_'+measure+"_summary"
			output1=subprocess.check_output( batch_grep, shell=True)
		batch_rm="rm "+" ".join( templist )
		output1=subprocess.check_output( batch_rm, shell=True )
		print "done summery dir"
	return 1;
