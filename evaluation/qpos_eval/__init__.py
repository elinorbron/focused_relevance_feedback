__author__='Elinor'
"""main file for evaluation on the baseline setup- before reranking
Namley the expended query with some positive model created by SMM,SMMP,SMMPP or RM-

run file: qpos_eval/main.py
input parameters: main directory after running the baseline. for example: "/lv_local/home/elinor/SMMP/Fixed_RelD_NRSeg/"
with res folder that have the results of the run

output:
eval folder with:
-QL-residual results directorie
-qrel residual files directorie
-normal, residual and QL-residual results directories, cut to 1000
-normal, residual and QL-residual eval directories, for 1000 res
-cvloo results for normal and residual results
-summerry files for top results

"""