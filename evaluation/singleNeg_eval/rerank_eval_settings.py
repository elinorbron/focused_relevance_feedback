__author__='Elinor'
__author__ = 'Elinor'
import os
import subprocess


def init_path(init_ret_path_input,qrels_path_input):
	global init_ret_path
	global qrels_path
	init_ret_path=init_ret_path_input
	qrels_path=qrels_path_input
	return

def create_sub_dir(sub_dir_input,Flag_positive):
	folders_paths=[]
	if Flag_positive:
		#already exist
		global p_rerank_sub_home
		global p_rerank_res_normal
		global p_rerank_res_residual
		p_rerank_sub_home=sub_dir_input+'/'
		p_rerank_res_normal=sub_dir_input+'res_1000/'
		p_rerank_res_residual=sub_dir_input+'res_1000_residual/'
		#should_be_formed in sub folder:
		global p_rerank_sub_eval
		global p_sub_eval_normal
		global p_sub_eval_residual
		p_rerank_sub_eval=p_rerank_sub_home+'eval/'
		p_sub_eval_normal=p_rerank_sub_eval+'res_1000_eval/'
		p_sub_eval_residual=p_rerank_sub_eval+'res_1000_residual_eval/'
		folders_paths+=[p_rerank_sub_eval,p_sub_eval_normal,p_sub_eval_residual]
	else:
		# already exist
		global n_rerank_sub_home
		global n_rerank_res_normal
		global n_rerank_res_residual
		n_rerank_sub_home=sub_dir_input+'/'
		n_rerank_res_normal=sub_dir_input+'res_1000/'
		n_rerank_res_residual=sub_dir_input+'res_1000_residual/'
		#should_be_formed in sub folder:
		global n_rerank_sub_eval
		global n_sub_eval_normal
		global n_sub_eval_residual
		n_rerank_sub_eval=n_rerank_sub_home+'eval/'
		n_sub_eval_normal=n_rerank_sub_eval+'res_1000_eval/'
		n_sub_eval_residual=n_rerank_sub_eval+'res_1000_residual_eval/'
		folders_paths+=[n_rerank_sub_eval, n_sub_eval_normal, n_sub_eval_residual]
	for folder in folders_paths:
		if not os.path.exists( folder ):
			os.makedirs( folder )
	return True;

def create_eval_Folders(home_dir_input, fuse_dir):
	folders_paths=[]
	#exists:
	global trec_file
	global QL_full_res_path
	QL_full_res_path='/lv_local/home/elinor/negative_feedback/QL/ql_ret_INEX.txt'
	trec_file="/lv_local/home/elinor/trec_eval.8.1/trec_eval"
	global baseline_home_dir
	global fuse_home_dir
	#global positive_home_dir
	global fuse_home_dir
	baseline_home_dir=home_dir_input
	fuse_home_dir=fuse_dir
	global eval_rerank_home_dir
	eval_rerank_home_dir=fuse_home_dir+'for_eval/'
	#positive_home_dir=rerank_home_dir+'positive/'
	fuse_home_dir=fuse_home_dir
	#creation:
	#create_sub_dir(positive_home_dir,True)
	create_sub_dir(fuse_home_dir,False)
	global qrels_dir
	global ql_residual_res
	global ql_residual_res_1000
	global ql_residual_eval
	qrels_dir=eval_rerank_home_dir+'qrels/'
	ql_residual_eval=eval_rerank_home_dir+'QL_residual_eval/'
	ql_residual_res=eval_rerank_home_dir+'QL_residual_res/'
	ql_residual_res_1000=eval_rerank_home_dir+'QL_residual_res_1000/'
	folders_paths+=[qrels_dir,ql_residual_eval,ql_residual_res,ql_residual_res_1000,eval_rerank_home_dir]
	for folder in folders_paths:
		if not os.path.exists( folder ):
			os.makedirs( folder )
	return True;

"""
flag_fixed- if true, will bring out the rel in top k results
k- number of docs returned
flag_rel- we might want relevant/non-relevant/all

"""


def getting_rel_docs_from_res_file(max_K, flag_fixed ,init_ret_path, qrels_path):
	global Rel_used
	global Neg_used
	global All_used
	Rel_used={}
	Neg_used={}
	All_used={}
	for k in range(1,max_K+1):
		Rel_from_qrel={}##key=query. for each key- list of docs the are positive.
		query_counters={}
		query_counters_neg={}
		## reading all rel docs
		binary_qrel_doc=open(qrels_path,'r')
		for line in binary_qrel_doc:
			queryNum=line.split()[0]
			if line.split()[3]==str(1):
				if (queryNum in Rel_from_qrel.keys()):
					Rel_from_qrel[queryNum]+=[line.split()[2]]
				else:
					Rel_from_qrel[queryNum]=[line.split()[2]]
		binary_qrel_doc.close()
		init_ret_doc=open(init_ret_path,"r")
		Rel_fbd=dict((key, []) for key in Rel_from_qrel.keys())
		all_fbd=dict((key, []) for key in Rel_from_qrel.keys())
		Neg_fbd=dict((key, []) for key in Rel_from_qrel.keys())
		query_counters=query_counters.fromkeys(Rel_from_qrel.keys(),0)
		query_counters_neg=query_counters_neg.fromkeys(Rel_from_qrel.keys(),0)
		if flag_fixed:
			for line in init_ret_doc:
				query=line.split()[0]
				if query_counters[query]<k:
					docnum=line.split()[2]
					query_counters[query]+=1
					if (docnum in Rel_from_qrel[query]):
						Rel_fbd[query]+=[docnum]
						all_fbd[query]+=[docnum]
					else:
						Neg_fbd[query]+=[docnum]
						all_fbd[query]+=[docnum]
		else:
			for line in init_ret_doc:
				query=line.split()[0]
				docnum=line.split()[2]
				if query_counters[query]<k:
					if (docnum in Rel_from_qrel[query]):
						Rel_fbd[query]+=[docnum]
						all_fbd[query]+=[docnum]
						query_counters[query]+=1
						continue
				if query_counters_neg[query]<k:
					if (docnum not in Rel_from_qrel[query]):
						Neg_fbd[query]+=[docnum]
						all_fbd[query]+=[docnum]
						query_counters_neg[query]+=1
		Rel_used[k]=Rel_fbd
		Neg_used[k]=Neg_fbd
		All_used[k]=all_fbd
		init_ret_doc.close()
	return Rel_used,Neg_used,All_used
