import sys
import os

scriptpath="/lv_local/home/elinor/pycharm/nf/MultiNeg/"
sys.path.append(os.path.dirname(scriptpath))

from qpos_eval import main as m




__author__='Elinor'
"""
This file is for the evaluation of the baseline setup(no re-ranking). can be use for: RM,SMM,SMMP,SMMPP


this file take the baseline setup folder and creates an eval folder with all the eval files needed:
1.normal_eval
2.normal_eval_top1000
3.eval_residual
4.eval_residual_at_top1000
5.Qrels_for_residual(for comparison later)
6.Ql_residual
7.Ql_residual_eval
Should work for every folder that is in the format, and using the documents we use in general
"""




"""
home_dir_input='/lv_local/home/elinor/negative_feedback/SMM/Accumulative_RelD'+'/'
setup="nonFixed"
"""
home_dir_input=sys.argv[1]



NRSeg_flag=0;
NRSeg_residual=0;
NRD_flag=0;
combined_flag=0;

if len(sys.argv)>2:
	for i in xrange(2,len(sys.argv)):
		if sys.argv[i]=="NRSeg" or sys.argv[i]=="nrseg":
			NRSeg_flag=1;
		if sys.argv[i]=="res" or sys.argv[i]=="nrsegres":
			NRSeg_residual=1;
		if sys.argv[i] == "NRD" or sys.argv[i] == "nrd":
			NRD_flag=1;
		if sys.argv[i] == "combined" or sys.argv[i] == "comb":
			combined_flag=1;
else:
	NRSeg_flag=1;
	NRD_flag=1;
	combined_flag=1;
	NRSeg_residual=1;

if NRSeg_flag:
	m.eval( home_dir_input, 1, '/single_neg/NRSeg_fus_res/' )

#if NRSeg_residual:
#	m.eval( home_dir_input, 2, '/single_neg/NRSeg_fus_res/' )



if NRD_flag:
	m.eval( home_dir_input, 2, '/single_neg/NRD_fus_res/' )

if combined_flag:
	m.eval( home_dir_input, 2, '/single_neg/combined_fuse_res/' )






#at this point all is ready for the CV_Loo part. baselines for comparison are ready as well
#cvloo
