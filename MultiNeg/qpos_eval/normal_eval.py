import eval_settings as es



__author__='Elinor'
import os
import subprocess

def chop_to_top_1000(source_dir,dest_dir):
	for res_file in os.listdir( source_dir ):
		batch_eval1="more "+source_dir+res_file+""" | awk '{if ($4<=1000) print;}' > """+dest_dir+res_file
		output1=subprocess.check_output( batch_eval1, shell=True )
	return

def trec_eval_normal_on_1000(source_dir,dest_dir):
	for res_file in os.listdir( source_dir ):
		batch_eval2=es.trec_file+" -q "+es.qrels_path+" "+source_dir+res_file+ " > "+dest_dir+res_file+"_temp"
		output1 = subprocess.check_output(batch_eval2, shell=True)
		batch_awk="more "+dest_dir+res_file+"_temp"+""" | awk '{if ($1=="map" || $1=="P5" || $1=="P10"  || $1=="P10" || $1=="num_rel"|| $1=="num_rel_ret") print;}' > """+dest_dir+res_file
		output1 = subprocess.check_output(batch_awk, shell=True)
		batch_rm="rm "+dest_dir+res_file+"_temp"
		output1=subprocess.check_output( batch_rm, shell=True )
	return
