import sys
import eval_settings as es
import normal_eval
import eval_residual
import os
import subprocess
#scriptpath="/lv_local/home/elinor/pycharm/nf/evaluation/"
#sys.path.append(os.path.dirname(scriptpath))
#from cvloo import CVLOO_baseline_eval_folder as cvl



"""
This file is for the evaluation of the baseline setup(no re-ranking). can be use for: RM,SMM,SMMP,SMMPP


this file take the baseline setup folder and creates an eval folder with all the eval files needed:
1.normal_eval
2.normal_eval_top1000
3.eval_residual
4.eval_residual_at_top1000
5.Qrels_for_residual(for comparison later)
6.Ql_residual
7.Ql_residual_eval
Should work for every folder that is in the format, and using the documents we use in general
"""




"""
home_dir_input='/lv_local/home/elinor/negative_feedback/SMM/Accumulative_RelD'+'/'
setup="nonFixed"
"""




def create_residual_res_file(source_file_path,dest_file_path,dic_docs_remove_per_q):
	file=open(source_file_path,'r')
	query_counters=dic_docs_remove_per_q.fromkeys(dic_docs_remove_per_q.keys(),0)
	query_moved=dic_docs_remove_per_q.fromkeys(dic_docs_remove_per_q.keys(),False)
	file_output=str()
	for line in file:
		queryNum=line.split()[0]
		docnum=line.split()[2]
		if docnum in dic_docs_remove_per_q[queryNum]:
			query_moved[queryNum]=True;
			query_counters[queryNum]+=1
			continue;
		if query_moved[queryNum]:
			templine=line.split()
			templine[3]=str(int(templine[3])-query_counters[queryNum])
			for word in templine:
				file_output+=word+' '
			file_output+=('\n')
		else:
			file_output+=line
	new_res_file=open(dest_file_path,'w+')
	new_res_file.write(file_output)
	new_res_file.close()

def create_residual_Qrels_file(source_file_path,dest_file_path,dic_docs_remove_per_q):
	file=open(source_file_path,'r')
	file_output=str()
	for line in file:
		queryNum=line.split( )[0]
		docnum=line.split( )[2]
		if docnum not in dic_docs_remove_per_q[queryNum]:
			file_output+=line
	new_res_file=open( dest_file_path, 'w+' )
	new_res_file.write( file_output )
	new_res_file.close( )
	return True



def eval(home_dir_input,files_used,folder):
	#if os.path.exists(home_dir_input+'/'+folder+'/eval/'):
	#	batch_rm="rm -fr "+home_dir_input+'/'+folder+'/eval/'
	#	subprocess.check_output( batch_rm, shell=True )
	full_setup_name=home_dir_input.rstrip( '/' ).rstrip( '/' ).split( '/' )[-1]
	setup=full_setup_name.split( '_' )[0]
	model=home_dir_input.rstrip( '/' ).rstrip( '/' ).split( '/' )[-2]
	print "sampling setup", setup
	print "model", model
	print "full_setup_name", full_setup_name
	flag_residual=True
	init_ret_path_input='/lv_local/home/elinor/negative_feedback/QL/ql_ret_INEX.txt'
	qrels_path_input='/lv_local/home/elinor/negative_feedback/Inex_qrelsAndParams/inex2009-2010-article.qrels_binary'
	##this if added on the 03/11/16:
	if "NRD" in full_setup_name:
		files_used=2;
	if files_used == 1: # #drop from residual: 0- nonrel 1-rel 2-all
		print "for residual evaluation, removing the --relevant-- documents used for feedback"
	elif files_used == 2: # #drop from residual: 0- nonrel 1-rel 2-all
		print "for residual evaluation, removing the --ALL-- documents used for feedback (both relevant and non relevant)"
	elif files_used == 0: # #drop from residual: 0- nonrel 1-rel 2-all
		print "for residual evaluation, removing the --NON-relevantONLY(why?)-- documents used for feedback"
	es.init_path( init_ret_path_input, qrels_path_input )
	home_dir_input=home_dir_input+folder
	if setup == "Fixed":
		print "fixed"
		flag_fixed=True
		max_K=10
	else:
		print "non-fixed"
		flag_fixed=False
		max_K=5
	es.create_eval_Folders( home_dir_input, flag_residual )
	es.getting_rel_docs_from_res_file( max_K, flag_fixed, es.init_ret_path, es.qrels_path )
	##normal eval: 1. chop files to 1000. 2. evaluate 3.cv_loo (different file)
	normal_eval.trec_eval_normal_on_1000(es.path_cut_res_normal,es.eval_dir_normal)
	eval_residual.sum_data_dir( es.eval_dir_normal, 'map', True, True, es.home_dir_eval, "normal_eval",max_K )
	eval_residual.sum_data_dir( es.eval_dir_normal, '"P5 "', True, True, es.home_dir_eval, "normal_eval",max_K )
	#cvl.cv_loo( es.eval_dir_normal, es.eval_dir_normal.rstrip('/')+'_cvloo/'  )
	#residual
	# #create Q_rels for each K (needed for eval) and chop to 1000
	##create res for each K and chop to 1000
	#eval
	if flag_residual:
		for k in range(1,max_K+1):##creating Qrels, needed for eval, QL_comparable baseline
			new_QL_file_path=es.ql_residual_res+str( k )+'_'+es.baseline_home_dir.rstrip( '/' ).split( '_' )[-1]+'_ql_residual_res'
			create_residual_res_file(es.QL_full_res_path,new_QL_file_path,[es.Neg_used,es.Rel_used,es.All_used][files_used][k])
			new_qrels_file_path=es.qrels_dir+str(k)+'_'+es.baseline_home_dir.rstrip('/').split('_')[-1]+'_residual_collection.qrels_binary'
			create_residual_Qrels_file( es.qrels_path , new_qrels_file_path, [es.Neg_used,es.Rel_used,es.All_used][files_used][k])
		eval_residual.QL_residual_eval()
		eval_residual.eval_residual_chopped(es.ret_res_dir,es.eval_dir_residual,es.qrels_dir)
		eval_residual.sum_data_dir(es.ql_residual_eval,"map",True,True,es.home_dir_eval,"ql_residual_eval",max_K )
		eval_residual.sum_data_dir(es.ql_residual_eval,'"P5 "',True,True,es.home_dir_eval,"ql_residual_eval",max_K )
		eval_residual.sum_data_dir(es.eval_dir_residual, "map", True, False, es.home_dir_eval, "residual_eval",max_K )
		eval_residual.sum_data_dir(es.eval_dir_residual, '"P5 "', True, False, es.home_dir_eval, "residual_eval",max_K )
		#cvl.cv_loo( es.eval_dir_residual, es.eval_dir_residual.rstrip('/')+'_cvloo/' )
	if ('NRSeg' in folder) and os.path.exists(home_dir_input+'/res_1000_residual_all/'):
		if files_used==2:
			for i in xrange(1,100):
				print "erooooooor\n"
		es.rename_to_added_residual_dirs_for_nrseg_fuse()
		print "new_dirs_eval"
		for k in range( 1, max_K+1 ):# #creating Qrels, needed for eval, QL_comparable baseline
			new_QL_file_path=es.ql_residual_res+str( k )+'_'+es.baseline_home_dir.rstrip( '/' ).split( '_' )[-1]+'_ql_residual_res'
			create_residual_res_file( es.QL_full_res_path, new_QL_file_path, [es.Neg_used, es.Rel_used, es.All_used][2][k] )
			new_qrels_file_path=es.qrels_dir+str( k )+'_'+es.baseline_home_dir.rstrip( '/' ).split( '_' )[-1]+'_residual_collection.qrels_binary'
			create_residual_Qrels_file( es.qrels_path, new_qrels_file_path, [es.Neg_used, es.Rel_used, es.All_used][2][k] )
		eval_residual.QL_residual_eval( )
		eval_residual.eval_residual_chopped( es.ret_res_dir, es.eval_dir_residual, es.qrels_dir )
		eval_residual.sum_data_dir( es.ql_residual_eval, "map", True, True, es.home_dir_eval, "ql_residual_eval", max_K )
		eval_residual.sum_data_dir( es.ql_residual_eval, '"P5 "', True, True, es.home_dir_eval, "ql_residual_eval", max_K )
		eval_residual.sum_data_dir( es.eval_dir_residual, "map", True, False, es.home_dir_eval, "residual_eval", max_K )
		eval_residual.sum_data_dir( es.eval_dir_residual, '"P5 "', True, False, es.home_dir_eval, "residual_eval", max_K )
	#cvl.cv_loo( es.eval_dir_residual, es.eval_dir_residual.rstrip('/')+'_cvloo/' )
	return 1








#at this point all is ready for the CV_Loo part. baselines for comparison are ready as well
#cvloo
