__author__='Elinor'
"""this is much more efficient than the other Fusion file, ####
but should do the same.
This implementation is correct for the normal evaluation
This implementation is correct for the residual evaluation under the asspmtion that all files used (relevant and non-relevant) in feedback should be removed because they are part of the model, whethere we choose to use them or not (by the leave-one-out-cross-validation)"""
import os
#import sys
import os.path
#import subprocess
#import itertools
import pandas as pd
#import numpy as np
#import scipy.stats
scriptpath="/lv_local/home/elinor/pycharm/nf/evaluation/"
from settings import *
sys.path.append(os.path.dirname(scriptpath))
from qpos_eval import eval_settings as es

#alpha_setup=(0.0,1.0)
#beta_setup=(0.0,1.0)



def create_dir(listdir):
	for dir in listdir:
		if not os.path.exists(dir):
			os.makedirs(dir)



def print_res_fuse_to_file(res_fuse,output_dir,file_name,alpha,beta):
	indri_file=str()
	for q_id in sorted(origin_q_dic.keys()):
		#res_fuse[q_id]['doc_name']=list(res_fuse[q_id].index)
		res_fuse[q_id].index.names=['Doc']
		res_fuse[q_id]=res_fuse[q_id].reset_index()
		temp_res=res_fuse[q_id].dropna().astype('float64').sort_index(axis=0, by=['results','Doc'],ascending=[False, True]).head(1000)
		for rank,(grade,docNo) in enumerate(zip(temp_res['results'],temp_res['Doc'])):
			indri_file+=str(q_id)+' Q0 '+str(int(docNo))+' '+str(rank+1)+' '+str(grade)+' indri \n'
	f=open(output_dir+file_name+'_MN_'+str(alpha)+'_NRD_'+str(beta)+'_NRSeg','w')##fuse_dir+'res_1000/'
	f.write(indri_file)
	f.close()

residual_docs_per_query={}
baseline_used_doc_for_fixed_rel={}




def aggregate(df,column_name):
	agg=pd.DataFrame((df.drop(['original'],axis=1).astype('float64')).apply(np.max,axis=1)).apply(np.exp)
	agg.columns=[column_name]
	return pd.concat([df['original'],agg],axis=1)




##fucnction that takes dictionary of nrd/nrseg with original and return after aggregation
def aggragate_per_query(dictionary,q_no_fb,col_name):
	for q_id in sorted( dictionary.keys( ) ):
		if (q_id not in q_no_fb):
			dictionary[q_id]=aggregate(dictionary[q_id] , col_name )
	#print dictionary["2010070"]
	return dictionary




def fusion_for_setup_file(NRD_per_q_dict,NRSeg_per_q_dict,original_per_q_dict,output , filename, alpha_setup, beta_setup, q_no_fb_NRSeg, q_no_fb_NRD):
	for alpha in alpha_setup:
		for beta in beta_setup:
			if alpha+beta <= 1:
				res_fuse={}
				for q_id in sorted( origin_q_dic.keys( ) ):
					if ((alpha == 1 and beta == 0 and (q_id in q_no_fb_NRD)) or (alpha == 0 and beta == 1 and (q_id in q_no_fb_NRSeg))): # #onlynrd and no nrd res for query or only nrseg and no rels for query- backoff to pos.
						res_fuse[q_id]=pd.DataFrame( (original_per_q_dict[q_id]["original"].astype( 'float64' )) )
					elif q_id in set(q_no_fb_NRSeg).intersection( q_no_fb_NRD):
						res_fuse[q_id]=pd.DataFrame((original_per_q_dict[q_id]["original"].astype( 'float64' )*(1-alpha-beta))-(1.0)*alpha-(1.0)*beta )
					elif q_id in q_no_fb_NRD:
						res_fuse[q_id]=pd.DataFrame( (original_per_q_dict[q_id]["original"].astype( 'float64' )*(1-alpha-beta)-(1.0)*alpha-NRSeg_per_q_dict[q_id]["NRSeg"].astype( 'float64' )*beta) )
					elif q_id in q_no_fb_NRSeg:
						res_fuse[q_id]=pd.DataFrame((original_per_q_dict[q_id]["original"].astype( 'float64' )*(1-alpha-beta)-NRD_per_q_dict[q_id]["NRD"].astype( 'float64' )*alpha-(1.0)*beta) )
					else:
						res_fuse[q_id]=pd.DataFrame((original_per_q_dict[q_id]["original"].astype( 'float64' )*(1-alpha-beta)-NRD_per_q_dict[q_id]["NRD"].astype( 'float64' )*alpha-NRSeg_per_q_dict[q_id]["NRSeg"].astype( 'float64' )*beta) )
						if res_fuse[q_id].dropna().shape[0]==0: print q_id, NRSeg_per_q_dict[q_id]
					res_fuse[q_id].columns=['results']
				print_res_fuse_to_file( res_fuse, output , filename, alpha, beta )





"""
takes all the data and bring back the data frames ready for fuse
"""
def prepare_data(origin_q_dic, orignaldict, NRSeg, NRD,used):
	NRD_per_q_dict={}
	NRSeg_per_q_dict={}
	original_per_q_dict={}
	q_no_fb_NRD=[]
	q_no_fb_NRSeg=[]
	for q_id in sorted( origin_q_dic.keys( ) ):
		# print q_id
		normal_q=orignaldict[q_id]
		if type(used) is not int:
			used[q_id]=[int( i ) for i in used[q_id]]
			#print q_id,used[q_id]
			##send to function to remove residuals, return normal_q
			drop_list_normal_q=list( set( normal_q["Doc"] ).intersection( set( used[q_id] ) ) )
			#print drop_list_normal_q
			normal_q=normal_q.set_index("Doc").drop(drop_list_normal_q, axis=0).reset_index()
		normal_q=pd.DataFrame( normal_q.astype( 'float64' ).sort( columns='rank', ascending=True ).head( 1000 ) )[["query", "Doc",'original']].set_index( ["query", "Doc"] ).apply( np.exp )
		original_per_q_dict[q_id]=normal_q.reset_index(level ='query',drop=True)
		#print pd.concat( [orignaldict["2009005"],original_per_q_dict["2009005"].reset_index()], axis=1).head(760) # ['Doc'=='2087185']
		#print normal_q

		if q_id in NRD.keys( ):
			n_NRD=NRD[q_id]
			#if type( used ) is not int:
				# #send to function to remove residuals, return normal_q
				#drop_list_NRD=list( set( n_NRD["Doc"] ).intersection( set( used[str(q_id)] ) ) )
				#n_NRD=n_NRD.set_index( "Doc" ).drop( drop_list_NRD, axis=0 ).reset_index( )
			n_NRD=n_NRD.set_index( ["query", "Doc"] )
			n_NRD=n_NRD.dropna( axis=1 ) # #if the query has no docs to rerank by, the whole column should be nan
			n_NRD=pd.concat( [normal_q, n_NRD], axis=1 )
			#print  n_NRD.shape, n_NRD['original'].dropna()##no idea how does that work. ['original'][pd.DataFrame(pd.isnull( n_NRD['original'] ))] # #drop everything that pos does not hold
			#print n_NRD.shape
			n_NRD=n_NRD.dropna( axis=0 ) # #drop everything that
			#  pos does not hold
			NRD_per_q_dict[q_id]=n_NRD.reset_index(level ='query',drop=True) ##all residuals will be dropped too. if exist in the original and not nofb must exist in rerank.
			#print n_NRD
		else:
			q_no_fb_NRD+=[q_id]

		if q_id in NRSeg.keys( ):
			n_NRSeg=NRSeg[q_id]
			#if type( used ) is not int:
				##send to function to remove residuals, return dataframe residual
				#drop_list_NRSeg=list( set( n_NRSeg["Doc"] ).intersection( set( used[q_id] ) ) )
				#n_NRSeg=n_NRSeg.set_index( "Doc" ).drop( drop_list_NRSeg, axis=0 ).reset_index( )
			n_NRSeg['Doc']=n_NRSeg['Doc'].astype(int)
			n_NRSeg=n_NRSeg.set_index( ["query", "Doc"] )
			n_NRSeg=n_NRSeg.dropna( axis=1 ) # #if the query has no docs to rerank by, the whole column should be nan
			n_NRSeg=pd.concat( [normal_q, n_NRSeg], axis=1 )
			n_NRSeg=n_NRSeg.dropna( axis=0 ) # #drop everything that pos does not hold
			#print n_NRSeg[pd.isnull(n_NRSeg['1_NRSeg'] )] # #drop everything that pos does not hold
			NRSeg_per_q_dict[q_id]=n_NRSeg.reset_index(level ='query',drop=True)
			#print n_NRSeg
		else:
			q_no_fb_NRSeg+=[q_id]
	#print sorted(origin_q_dic.keys())
	#print q_no_fb_NRSeg
	#print q_no_fb_NRD
	NRD_per_q_dict=aggragate_per_query( NRD_per_q_dict, q_no_fb_NRD, 'NRD' )
	NRSeg_per_q_dict=aggragate_per_query( NRSeg_per_q_dict, q_no_fb_NRSeg, 'NRSeg' )
	#print pd.concat( [orignaldict["2009001"], original_per_q_dict["2009001"].reset_index( )], axis=1 ).head( 760 )
	return NRD_per_q_dict, NRSeg_per_q_dict, original_per_q_dict, alpha_setup, beta_setup, q_no_fb_NRSeg, q_no_fb_NRD






def process_mn_fusion_residual(origin_q_dic,orignaldict,NRSeg,NRD,multineg,alpha_setup,beta_setup,file,uflagcombine,uflagNRD,uflagNRSeg,uflagNRSegResid,used):
	NRD_per_q_dict, NRSeg_per_q_dict, original_per_q_dict, alpha_setup, beta_setup, q_no_fb_NRSeg, q_no_fb_NRD=prepare_data( origin_q_dic, orignaldict, NRSeg, NRD, used )
	if uflagcombine:
		output=multineg+'/combined_fuse_res/res_1000_residual/'
		if not os.path.exists( output ):
			os.makedirs( output )
		fusion_for_setup_file( NRD_per_q_dict, NRSeg_per_q_dict, original_per_q_dict, output, file, alpha_setup, beta_setup, q_no_fb_NRSeg, q_no_fb_NRD )
	if uflagNRD:
		output=multineg+'/NRD_fus_res/res_1000_residual/'
		if not os.path.exists( output ):
			os.makedirs( output )
		fusion_for_setup_file( NRD_per_q_dict, NRSeg_per_q_dict, original_per_q_dict, output, file, alpha_setup, [0.0], q_no_fb_NRSeg, q_no_fb_NRD )
	if uflagNRSeg:
		output=multineg+'/NRSeg_fus_res/res_1000_residual/'
		if not os.path.exists( output ):
			os.makedirs( output )
		fusion_for_setup_file( NRD_per_q_dict, NRSeg_per_q_dict, original_per_q_dict, output, file, [0.0], beta_setup, q_no_fb_NRSeg, q_no_fb_NRD )
	if uflagNRSegResid:
		output=multineg+'/NRSeg_fus_res/res_1000_residual_all/'
		if not os.path.exists( output ):
			os.makedirs( output )
		fusion_for_setup_file( NRD_per_q_dict, NRSeg_per_q_dict, original_per_q_dict, output, file, [0.0], beta_setup, q_no_fb_NRSeg, q_no_fb_NRD )
	return



def process_mn_fusion_normal( origin_q_dic, orignaldict, NRSeg, NRD, output, alpha_setup, beta_setup,filename,uflagcombine,uflagNRD,uflagNRSeg ):
	#takes all the data and bring back the data frames ready for fuse
	NRD_per_q_dict, NRSeg_per_q_dict, original_per_q_dict, alpha_setup, beta_setup, q_no_fb_NRSeg, q_no_fb_NRD=prepare_data( origin_q_dic, orignaldict, NRSeg, NRD,0 )
	#print NRSeg_per_q_dict#.reset_index( level='query', drop=True )
	#all that is left is fusing by the different cases- where output to? what is the parameter value?
	if uflagcombine:
		output=multineg+'/combined_fuse_res/res_1000/'
		if not os.path.exists( output ):
			os.makedirs( output )
		fusion_for_setup_file( NRD_per_q_dict, NRSeg_per_q_dict, original_per_q_dict, output, filename, alpha_setup, beta_setup, q_no_fb_NRSeg, q_no_fb_NRD )
	if uflagNRD:
		output=multineg+'/NRD_fus_res/res_1000/'
		if not os.path.exists( output ):
			os.makedirs( output )
		fusion_for_setup_file( NRD_per_q_dict, NRSeg_per_q_dict, original_per_q_dict, output, filename, alpha_setup, [0.0], q_no_fb_NRSeg, q_no_fb_NRD )
	if uflagNRSeg:
		output=multineg+'/NRSeg_fus_res/res_1000/'
		if not os.path.exists( output ):
			os.makedirs( output )
		fusion_for_setup_file( NRD_per_q_dict, NRSeg_per_q_dict, original_per_q_dict, output, filename,[0.0], beta_setup, q_no_fb_NRSeg, q_no_fb_NRD )
	#print "###############a############"
	##fucnction that takes dictionary of nrd/nrseg with original and return after aggregation
	#fusion_for_setup_file( NRD_per_q_dict,NRSeg_per_q_dict,original_per_q_dict,output , filename, alpha_setup, beta_setup, q_no_fb_NRSeg, q_no_fb_NRD )##outputdir should be also is residual/normal/combined/sep
	##fusion and printing. here I will have flags that will use different fusions.in the residual- more challenged, cause different when dropping. since done for all files simoltanioasly. no problem to recall the residual function one after the other.
	return
"""
def process_mn_fusion_normal():
	#for file:
	#begin_fuse()
		#loading all positive results for file.
		#loading all rerank results per file
	#normal part:
	#for query copy all the relevant results from pos and from summery- 1. nrseg 2. nrd
	#concat with pos (1. nrd, 2.nrseg)
	#sort by pos, take top1000 (1. nrd, 2.nrseg)
	#maybe drop nan columns at this point. if after this the df of the query if empty, should be listed as no fb docs for this rerank type (or flaged so the fusion will use only original)
	#without the original column, produce score with aggregate function. Now df should hold 2 df, one of NRSeg and original and one of NRD and original.
	#fusion by formula. will also receive folder and alpha beta so I can do the sub cases here as well (using this function again for same).
	#print

def process_mn_fusion_residual():
	# for file:
	#begin_fuse()
	#loading all positive results for file.
	#loading all rerank results per file
	#residual part:
	#for query copy all  the relevant results from pos and from summery- 1. nrseg 2. nrd
	#concat with pos (1. nrd, 2.nrseg)
	#remove residuals if exist. (1. nrd, 2.nrseg)
	#sort by pos, take top1000 (1. nrd, 2.nrseg)
	# maybe drop nan columns at this point. if after this the df of the query if empty, should be listed as no fb docs for this rerank type (or flaged so the fusion will use only original)
	#without the original column, produce score with aggregate function. Now df should hold 2 df, one of NRSeg and original and one of NRD and original.
	#fusion by formula. will also receive folder and alpha beta so I can do the sub cases here as well (using this function again for same).
	#print


"""









def read_original(path):
	dict={}
	df=pd.read_csv( path, sep=" ", parse_dates=False, header=None, index_col=False,names=['q_id', 'Q0', 'Doc', 'rank', 'score', 'indri'], dtype={'q_id':str, 'Q0':str, 'Doc':int, 'rank':int, 'score':np.float64, 'indri':str})# read file, use for something ##also possible to do ,index_col= [0,2] straight, but will have to rename somehow
	df=df[[0, 2,3, 4]]
	df.columns=["query", "Doc","rank", 'original']
	grouped=df.groupby( 'query' )
	for name, group in grouped:
		dict[str(name)]=group
	#df=df.set_index( ["query", "Doc"] )
	return dict


def read_into_dict(dict, path):
	df=pd.read_csv( path, parse_dates=False,dtype={'q_id':str})
	df=df[df['Doc'].notnull()]
	#df['Doc']=df['Doc'].astype(int)
	grouped=df.groupby( 'query' )
	for name, group in grouped:
		dict[str(name)]=group
	return dict
"""
	df=pd.read_csv( path, parse_dates=False )
	for q_id  in sorted( origin_q_dic.keys( ) ):
		temp=df[df["query"].astype( 'str' ) == q_id].set_index( ["query", "Doc"] )
		temp=temp.dropna( axis=1 ) # #if the query has no docs to rerank by, the whole column should be nan
		dict[q_id]=temp"""






""""""""""
calling all
"""""""""""

#baseline_used_doc_for_fixed()
origin_q_dic={}
read_queries_to_dict(str_queries_path,origin_q_dic)



flagcombine=1
flagNRD=1
flagNRSeg=1
flagNormal=1
flag_residual=1

multineg=multi
if flag_residual:
	es.getting_rel_docs_from_res_file( max_K, flag_fixed, init_ret_path, binary_qrels_path )

#results dir- if combine is different, than different folders. if want only nrd: used=neg alpha=[1],beta=[0], folder=negonly
NRD_res_dir=multineg+'/rerank_with_NRD/res_summery/'
NRSeg_res_dir=multineg+'/rerank_with_NRSeg/res_summery/'
res_dir=home+'/res/'

i=0
QL_dict=read_original(init_ret_path)
for file in sorted(os.listdir(res_dir)):
	k=int( file.split( '_' )[0] )
	all=len (os.listdir(res_dir))*(float(len(klist))/float(max_K))
	if k in klist:#"7_0.5_0.9_50" in file:
		NRSeg={}
		NRD={}
		i+=1
		print file, str((float(i)/float(all))*100.0)+"%"
		if QLFLAG:
			if not os.path.exists(NRSeg_res_dir+file+'.csv'):
				#print "not here",NRSeg_res_dir+file+'.csv'
				continue;
			else:
				orignaldict=QL_dict
		else:
			orignaldict=read_original( res_dir+file )
		read_into_dict(NRSeg, NRSeg_res_dir+file+'.csv')
		read_into_dict(NRD, NRD_res_dir+file+'.csv')
		#print len(NRSeg.keys())
		if flagNormal:
			#print "normal"
			process_mn_fusion_normal(origin_q_dic,orignaldict,NRSeg,NRD,multineg,alpha_setup,beta_setup,file,flagcombine,flagNRD,flagNRSeg)
		if flag_residual:
			#print "residual"
			if flagcombine or flagNRD:
				process_mn_fusion_residual(origin_q_dic,orignaldict,NRSeg,NRD,multineg,alpha_setup,beta_setup,file,flagcombine,flagNRD,0,0,es.All_used[k])
			if flagNRSeg:
				if 'NRD' in full_setup_name:
					process_mn_fusion_residual( origin_q_dic, orignaldict, NRSeg, NRD, multineg, alpha_setup, beta_setup, file, 0, 0, flagNRSeg,0, es.All_used[k] )
				else:
					process_mn_fusion_residual( origin_q_dic, orignaldict, NRSeg, NRD, multineg, alpha_setup, beta_setup, file, 0, 0, flagNRSeg,0, es.Rel_used[k] )
					process_mn_fusion_residual( origin_q_dic, orignaldict, NRSeg, NRD, multineg, alpha_setup, beta_setup, file, 0, 0, 0,1, es.All_used[k] )

