__author__='Elinor'
__author__='Elinor'
import os
import os.path
import sys
import numpy as np
import pandas as pd
import subprocess
import itertools

"""
getting outside variables:
"""
klist=xrange(1,11)
if len(sys.argv)<2:
	print "this function expects 1 or more arguments: \n\t(1) source path for the reranking directory of MultiNeg. \n\t **such directory holds the folder parameters and res, with folders that hold the reranking parameters and their results, subfolder for each positive setup file**. \n\t if you wish to run only on part of the sub folder, specify the number of docs used with a space: 1 3 will run only sub-folders that begin with 1 or 3.  "
else:
	rerank_dir=sys.argv[1]+'/'
	# /lv_local/home/elinor/negative_feedback/SMM/Fixed_RelSeg/multi_neg/rerank_with_NRD
if len(sys.argv)>=3:
	klist=[]
	for i in range(2,len(sys.argv)):
		klist+=[int(sys.argv[i])]

print klist

"""
##based on outside variables:
effect functions below.
"""
#folder_source='/lv_local/home/elinor/negative_feedback/trials/trial1/param_chunk/.4'
param_dir=rerank_dir+'parameters/'
res_dir=rerank_dir+'res/'
res_sum=rerank_dir+'res_summery/'

def create_dir(listdir):
	for dir in listdir:
		if not os.path.exists(dir):
			os.makedirs(dir)


def sum_rerank_folder(rerank_dir):
	i=0
	create_dir([res_sum])
	for file_dir in os.listdir(res_dir):
		###additional condition to split the load
		if int(file_dir.split('_')[0]) in klist:
			i+=1
			print file_dir, i
			integrate=pd.DataFrame()
			##here we want to open a file in res_dir that will hold all of the information in the end
			for res_file in sorted(os.listdir(param_dir+file_dir)):
				res_path=res_dir+file_dir+'/'+res_file
				out_path=res_sum+file_dir+'.csv'
				df=pd.read_csv( res_path,sep=" ", parse_dates=False,  header=None ,na_values='<has_no_information_for_expansion>',names=['query','Q0','Doc','rank',res_file,'indri'],dtype=str)#read file, use for something ##also possible to do ,index_col= [0,2] straight, but will have to rename somehow
				df=df[[0,2,4]]
				df=df.dropna(axis=0)
				#df.columns=["query","Doc",res_file]
				df=df.set_index(["query","Doc"])
				#print df
				integrate=pd.concat([integrate,df],axis=1)#add to df
			#print integrate.shape
			#print integrate
			integrate.to_csv(out_path)


sum_rerank_folder(rerank_dir)