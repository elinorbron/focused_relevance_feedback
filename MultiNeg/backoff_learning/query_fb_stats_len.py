__author__='Elinor'
import os
import sys
import os.path
import subprocess
import itertools
import pandas as pd
import pickle
scriptpath="/lv_local/home/elinor/pycharm/nf/evaluation/"
sys.path.append(os.path.dirname(scriptpath))
from qpos_eval import eval_settings as es



##Accumulative:
#
#es.getting_rel_docs_from_res_file( max_K, flag_fixed, init_ret_path, binary_qrels_path )
def creat_dict_query_seperation(flag_fixed,dicti):
	if flag_fixed:
		max_K=10
	else:
		max_K=5
	es.getting_rel_docs_from_res_file( max_K, flag_fixed, '/lv_local/home/elinor/negative_feedback/QL/ql_ret_INEX.txt','/lv_local/home/elinor/negative_feedback/Inex_qrelsAndParams/inex2009-2010-article.qrels_binary')
	Accu_path_output='/lv_local/home/elinor/negative_feedback/Inex_qrelsAndParams/Accumulative_query_separation.p'
	path_fixed_output='/lv_local/home/elinor/negative_feedback/Inex_qrelsAndParams/Fixed_query_separation.p'
	for k in xrange(1,max_K+1):
		Accu_path_input='/lv_local/home/elinor/negative_feedback/SMM/Accumulative_RelD/rerank_with_NRSeg/res/'+str(k)+'_0.5_0.1_50_RelD_NRSeg'
		path_fixed_input='/lv_local/home/elinor/negative_feedback/SMM/Fixed_RelSeg/rerank_with_NRSeg/res/'+str( k )+'_0.5_0.1_50_RelSeg_NRSeg'
		if flag_fixed:
			path=path_fixed_input
			output_path=path_fixed_output
		else:
			path=Accu_path_input
			output_path=Accu_path_output
		dfRel=pd.DataFrame.from_dict(es.Rel_used[k],orient='Index')
		#print dfRel
		#print dfRel.dropna()
		dfRelSeg=pd.DataFrame.from_dict(es.Rel_used[k],orient='Index')
		dfNeg=pd.DataFrame.from_dict(es.Neg_used[k],orient='Index')
		dfAll=pd.DataFrame.from_dict(es.All_used[k],orient='Index')
		bashgrep='grep e '+path+"  | awk '{print $1}'"
		output1=subprocess.check_output( bashgrep, shell=True )
		#print output1
		if "No such file or directory" in output1:
			print "eroooooooooooor"
			return;
		query_no_fb_nrseg=output1.split('\n')[:-1]
		#print dfRelSeg
		dfRelSeg=dfRelSeg.drop(query_no_fb_nrseg)
		dfRel=pd.DataFrame(dfRel[0].dropna())
		dfRelSeg=pd.DataFrame(dfRelSeg[0].dropna())
		dfNeg=pd.DataFrame(dfNeg[0].dropna())
		dfAll=pd.DataFrame(dfAll[0].dropna())
		dfRel.columns=['RelD']
		dfRelSeg.columns=['NRSeg']
		dfNeg.columns=['NRD']
		dfAll.columns=['All']
		#print dfRel
		#dfNeg=dfNeg.rename(columns={0: 'NRD'})
		#dfRelSeg=dfRelSeg.rename(columns={0: 'NRSeg'})
		#dfAll=dfAll.rename(columns={0: 'All'})
		data=pd.concat([dfRelSeg['NRSeg'],dfRel['RelD'],dfNeg['NRD'],dfAll['All']],axis=1).fillna(False)
		data['all']=data['All'].astype(bool)
		data['NRD']=data['NRD'].astype(bool)
		data['NRSeg']=data['NRSeg'].astype(bool)
		data['Pos+Neg_all']=data['NRSeg'].astype(bool)&data['RelD'].astype(bool)&data['NRD'].astype(bool)
		data['Pos+Neg_NRD']=(~data['NRSeg'].astype(bool))&data['RelD'].astype(bool)&data['NRD'].astype(bool)
		data['Pos+Neg_NRSeg']=(data['NRSeg'].astype(bool))&data['RelD'].astype(bool)&(~data['NRD'].astype(bool))
		data['only_Pos']=(~data['NRSeg'].astype(bool))&data['RelD'].astype(bool)&(~data['NRD'].astype(bool))
		data['noPos+Neg_all']=data['NRSeg'].astype(bool)&(~data['RelD'].astype(bool))&data['NRD'].astype(bool)
		data['noPos+Neg_NRD']=(~data['NRSeg'].astype(bool))&(~data['RelD'].astype(bool))&data['NRD'].astype(bool)
		data['noPos+Neg_NRSeg']=(data['NRSeg'].astype(bool))&(~data['RelD'].astype(bool))&(~data['NRD'].astype(bool))
		data['no_feedback']=(~data['NRSeg'].astype(bool))&(~data['RelD'].astype(bool))&(~data['NRD'].astype(bool))
		data['Pos']=data['Pos+Neg_all']|data['Pos+Neg_NRD']|data['Pos+Neg_NRSeg']|data['only_Pos']
		data['noPos']=~data['Pos']
		data['test']=data['Pos+Neg_all']^data['Pos+Neg_NRD']^data['Pos+Neg_NRSeg']^data['only_Pos']^data['noPos+Neg_all']^data['noPos+Neg_NRD']^data['noPos+Neg_NRSeg']^data['no_feedback']
		#print data.drop(['All'],axis=1)
		dicti[k]={}
		for type in data[['all','NRD','NRSeg','Pos+Neg_all','Pos+Neg_NRD','Pos+Neg_NRSeg','only_Pos','noPos+Neg_all','noPos+Neg_NRD','noPos+Neg_NRSeg','no_feedback','Pos','noPos','NRSeg','NRD']].columns:
			dicti[k][type]=len(sorted(list(data[type][data[type]==True].index)))
			#dicti[k][type]=sorted( list( data[type][data[type] == True].index ) )
	print dicti
	pickle.dump( dicti, open(output_path, "wb" ) )
	return output_path



dicti_fixed={}
dicti_accu={}
creat_dict_query_seperation(1,dicti_fixed)
print pd.DataFrame.from_dict( dicti_fixed,orient='index').to_csv('/lv_local/home/elinor/negative_feedback/Inex_qrelsAndParams/Fixed_query_separation_len.csv')
creat_dict_query_seperation(0,dicti_accu)
print pd.DataFrame.from_dict( dicti_accu,orient='index').to_csv('/lv_local/home/elinor/negative_feedback/Inex_qrelsAndParams/Accumulative_query_separation_len.csv')

