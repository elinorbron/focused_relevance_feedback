__author__='Elinor'
import os
import sys
import os.path
import subprocess
import itertools
import pandas as pd
import pickle




"""
flag_fixed- if true, will bring out the rel in top k results
k- number of docs returned
flag_rel- we might want relevant/non-relevant/all

"""


def getting_rel_docs_from_res_file(max_K, flag_fixed ,init_ret_path, qrels_path):
	global Rel_used
	global Neg_used
	global All_used
	Rel_used={}
	Neg_used={}
	All_used={}
	for k in range(1,max_K+1):
		Rel_from_qrel={}##key=query. for each key- list of docs the are positive.
		query_counters={}
		query_counters_neg={}
		## reading all rel docs
		binary_qrel_doc=open(qrels_path,'r')
		for line in binary_qrel_doc:
			queryNum=line.split()[0]
			if line.split()[3]!=str(0):
				if (queryNum in Rel_from_qrel.keys()):
					Rel_from_qrel[queryNum]+=[line.split()[2]]
				else:
					Rel_from_qrel[queryNum]=[line.split()[2]]
		binary_qrel_doc.close()
		init_ret_doc=open(init_ret_path,"r")
		Rel_fbd=dict((key, []) for key in Rel_from_qrel.keys())
		all_fbd=dict((key, []) for key in Rel_from_qrel.keys())
		Neg_fbd=dict((key, []) for key in Rel_from_qrel.keys())
		query_counters=query_counters.fromkeys(Rel_from_qrel.keys(),0)
		query_counters_neg=query_counters_neg.fromkeys(Rel_from_qrel.keys(),0)
		if flag_fixed:
			for line in init_ret_doc:
				query=line.split()[0]
				if query_counters[query]<k:
					docnum=line.split()[2]
					query_counters[query]+=1
					if (docnum in Rel_from_qrel[query]):
						Rel_fbd[query]+=[docnum]
						all_fbd[query]+=[docnum]
					else:
						Neg_fbd[query]+=[docnum]
						all_fbd[query]+=[docnum]
		else:
			for line in init_ret_doc:
				query=line.split()[0]
				docnum=line.split()[2]
				if query_counters[query]<k:
					if (docnum in Rel_from_qrel[query]):
						Rel_fbd[query]+=[docnum]
						all_fbd[query]+=[docnum]
						query_counters[query]+=1
						continue
				if query_counters_neg[query]<k:
					if (docnum not in Rel_from_qrel[query]):
						Neg_fbd[query]+=[docnum]
						all_fbd[query]+=[docnum]
						query_counters_neg[query]+=1
		Rel_used[k]=Rel_fbd
		Neg_used[k]=Neg_fbd
		All_used[k]=all_fbd
		init_ret_doc.close()
	return Rel_used,Neg_used,All_used




##Accumulative:
#
#es.getting_rel_docs_from_res_file( max_K, flag_fixed, init_ret_path, binary_qrels_path )
def creat_dict_query_seperation(flag_fixed,outpath):
	if flag_fixed:
		max_K=10
	else:
		max_K=5
	getting_rel_docs_from_res_file( max_K, flag_fixed, '/lv_local/home/elinor/negative_feedback/QL/ql_ret_INEX.txt','/lv_local/home/elinor/negative_feedback/Inex_qrelsAndParams/inex2009-2010-article.qrels_binary' )
	#getting_rel_docs_from_res_file( max_K, flag_fixed, '/lv_local/home/elinor/negative_feedback/QL/ql_ret_INEX.txt', '/lv_local/home/elinor/negative_feedback/Inex_qrelsAndParams/inex2009-2010-article.qrels_binary' )
	dfRel=pd.DataFrame.from_dict(Rel_used[max_K],orient='Index').sort_index(axis=0, ascending=True)
	dfRel['count']=dfRel.count(axis=1)
	#print dfRel
	#print dfRel
	#print dfRel.dropna()
	#print dfRelSeg
	#dfRel=pd.DataFrame(dfRel[0].dropna())
	print pd.DataFrame(dfRel).to_csv(outpath)
	#print dfRel
	#dfNeg=dfNeg.rename(columns={0: 'NRD'})
	#dfRelSeg=dfRelSeg.rename(columns={0: 'NRSeg'})
	#dfAll=dfAll.rename(columns={0: 'All'})
	#print data.drop(['All'],axis=1)



dicti_fixed={}
dicti_accu={}
creat_dict_query_seperation(1,'/lv_local/home/elinor//Fixed_query_data_attop.csv')

creat_dict_query_seperation(0,'/lv_local/home/elinor/Accumulative_query_sdata_attop.csv')

#creat_dict_query_seperation(1,'/lv_local/home/elinor/Fixed_query_separation_INEX.csv')

#creat_dict_query_seperation(0,'/lv_local/home/elinor/Accumulative_query_separation_INEX.csv')