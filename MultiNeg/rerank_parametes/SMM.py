__author__='Elinor'
import os
import os.path
import sys
import subprocess
import itertools
scriptpath="/lv_local/home/elinor/pycharm/nf/evaluation/"
sys.path.append(os.path.dirname(scriptpath))
from qpos_eval import eval_settings as es
"""
getting outside variables:
"""
baseline_home_dir=sys.argv[1]+'/'

"""
##based on outside variables:
"""

initial_feedback_setup_name=baseline_home_dir.rstrip('/').rstrip('/').split('_')[-1]
print initial_feedback_setup_name

setup=baseline_home_dir.rstrip('/').rstrip('/').split('/')[-1].split('_')[0]
print setup

if initial_feedback_setup_name=='RelSeg':
	initial_feedback_setup=1
elif initial_feedback_setup_name=='RelD':
	initial_feedback_setup=4
else:
	print 'error'

if setup == "Fixed":
	flag_fixed=True
	max_K=10
else:
	flag_fixed=False
	max_K=5

"""
global constants
"""
rerank_setups=[3,2]

start_param_file_path="/lv_local/home/elinor/negative_feedback/Inex_qrelsAndParams/param_part1_dropping_origQW.xml"
orig_queries="/lv_local/home/elinor/negative_feedback/Inex_qrelsAndParams/INEX_params_b_segQ.xml"
init_ret_path='/lv_local/home/elinor/negative_feedback/QL/ql_ret_INEX.txt'
qrels_path='/lv_local/home/elinor/negative_feedback/Inex_qrelsAndParams/inex2009-2010-article.qrels_binary'
baseline_res=baseline_home_dir+'res'+'/'

fbDocs=range(1,max_K+1)
fbOrigWeight=[0.2,0.5,0.8] #for file name only. within we pass
weights=[0,0.1,0.5,0.9]
lambda1=lambda2=0
fbTerms=[10,25,50]
feedback_setup_name=['RelSeg','NRSeg','NRD','RelD','PRF']

"""
structuring folders
"""
def define_paths_formulti(ql_based):
	global m_dir
	if ql_based:
		m_dir=baseline_home_dir+'multi_negQL/'
	else:
		m_dir=baseline_home_dir+'multi_neg/'
	return [m_dir]

def define_paths(rerank_setup):
	global home_dir
	global output_param_files_dir
	global output_res_files_dir
	home_dir=m_dir+'rerank_with_'+feedback_setup_name[rerank_setup-1]+'/'
	output_param_files_dir=home_dir+"parameters/"
	output_res_files_dir=home_dir+"res/"
	return [home_dir,output_param_files_dir,output_res_files_dir]

def create_dir(listdir):
	for dir in listdir:
		if not os.path.exists(dir):
			os.makedirs(dir)

#define arg



def read_query_info(orig_queries):
	#step one- for each query create a dictionary of the query words.
	global q_info
	q_info={}
	q_file=open(orig_queries,'r')
	query_str_a='\t<query>\n'
	query_str_b=str()
	for line in q_file:
		if 'number' in line:
			q_id=line.strip('\t\t<number>')
			q_id=q_id.rstrip('</number>\n\t\t')
			query_str_a+=line;
		if '<text>' in line:
			query_str_a+=line;
		if '<relseg>' in line:
			query_str_b+=line;
		if '</query>' in line:
			query_str_b+=line;
			q_info[q_id]=[query_str_a,query_str_b]
			query_str_a='\t<query>\n'
			query_str_b=str()
	q_file.close()
	return




def create_parameters_fixed(ql_based=0):
	l=define_paths_formulti(ql_based)
	if ql_based:
		fbOrigWeight=[1.0]
	create_dir( l )
	read_query_info(orig_queries)
	es.getting_rel_docs_from_res_file( max_K, flag_fixed, init_ret_path, qrels_path )
	for rerank_setup in rerank_setups:
		l=define_paths( rerank_setup )
		create_dir( l)
		#start code:
		i=0;
		start_param_file=open(start_param_file_path,'r')
		start_param=start_param_file.read()
		start_param_file.close()
		for fbDocs_parm in fbDocs:
			for fbOrigWeight_parm in fbOrigWeight:
				for term,setup in list(itertools.product(fbTerms,[rerank_setup])):
					for lambda3orig in weights:
						this_input_name=str( fbDocs_parm )+'_'+str( fbOrigWeight_parm )+'_'+str( lambda3orig )+'_'+str( term )+'_'+feedback_setup_name[initial_feedback_setup-1]
						this_paramere_dir=output_param_files_dir+this_input_name+'/'
						this_res_dir=output_res_files_dir+this_input_name+'/'
						create_dir([this_paramere_dir,this_res_dir])
						for lambda3 in [lambda3orig]:# weights: this if we want to break coupling between original corpus lambda and corpus lambda for rerank
							mixtureCorpusWeight_param_t=lambda1+lambda2+lambda3 # lambda bg
							for j in xrange(1,fbDocs_parm+1):
								#writing parameter file part 1
								this_output=str()
								this_output_name=str(j)+'_'+feedback_setup_name[setup-1]
								this_output_path=this_paramere_dir+this_output_name
								this_output+=start_param
								this_output+='\t<mixtureCorpusWeight>'+str(mixtureCorpusWeight_param_t)+'</mixtureCorpusWeight>\n'
								this_output+='\t<fbTerms>'+str(term)+'</fbTerms>\n'
								this_output+='\t<fbOrigWeight>'+str(0)+'</fbOrigWeight>\n'
								this_output+='\t<feedback_setup>'+str(setup)+'</feedback_setup>\n'
								this_output+='\t<fbDocs>'+str(1)+'</fbDocs>\n'
								for query in sorted(q_info.keys()):
									if rerank_setup == 2:
										D_f=es.Rel_used[fbDocs_parm][query]
									elif rerank_setup == 3:
										D_f=es.Neg_used[fbDocs_parm][query]
									if len(D_f)>=j:
										doc=D_f[j-1]
										this_output+=q_info[query][0]
										this_output+='\t\t<feedbackDocno>'+str(doc)+'</feedbackDocno>\n'
										if ql_based:
											batch_cut_top='grep "'+query+' Q0 " '+init_ret_path+" | awk '{ if ($4<1200) print $3;}'"
										else:
											batch_cut_top='grep "'+query+' Q0 " '+baseline_res+this_input_name+" | awk '{ if ($4<1200) print $3;}'"
										output2=subprocess.check_output( batch_cut_top, shell=True )
										for Docno in output2.rstrip( '\n' ).split( '\n' ):
											this_output+='\t\t<workingSetDocno>'+Docno+'</workingSetDocno>\n'
										this_output+=q_info[query][1]
									#else:
										#print "for query "+str(query)+" param "+str(this_output_path) +"-- no documents"
								this_output+="</parameters>\n"
								out_f=open(this_output_path,'w+')
								out_f.write(this_output)
								out_f.close()
								i=i+1
								print i
		print "done"
	return 1

create_parameters_fixed(1 )#EXPERIMENT TO TEST IF HOW MUCH THE POSITIVE COMPONENT EFFECTS

####this is ok!!!!!!
#if flag_fixed:
create_parameters_fixed( )##same for fixed and accumulative here. only difference is the used documents, determent by the flag(fixed_flag)
#else:
	#create_parameters_accumulative();


