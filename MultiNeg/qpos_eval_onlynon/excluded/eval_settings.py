"""__author__ = 'Elinor'
import os
import subprocess




def init_path(init_ret_path_input,qrels_path_input):
	global init_ret_path
	global qrels_path
	init_ret_path=init_ret_path_input
	qrels_path=qrels_path_input
	return


def rename_to_added_residual_dirs():
	global baseline_home_dir
	global home_dir_eval
	global eval_dir_residual
	global path_cut_res
	global ret_res_dir
	global qrels_dir
	global ql_residual_eval
	global ql_residual_res
	global ql_residual_res_1000
	eval_dir_residual=home_dir_eval+'residual_all_collection_eval/'
	path_cut_res=home_dir_eval+'residual_all_collection_res_1000/'
	ret_res_dir=home_dir_eval+'residual_all_collection_res/'
	qrels_dir=home_dir_eval+'qrels_residual_all/'
	ql_residual_eval=home_dir_eval+'QL_residualall_eval/'
	ql_residual_res=home_dir_eval+'QL_residualall_res/'
	ql_residual_res_1000=home_dir_eval+'QL_residualall_res_1000/'
	folders_paths=[eval_dir_residual, path_cut_res, ret_res_dir, qrels_dir, ql_residual_res,ql_residual_eval,ql_residual_res_1000]
	for folder in folders_paths:
		if not os.path.exists( folder ):
			os.makedirs( folder )

def create_eval_Folders(home_dir_input, flag_residual):
	folders_paths=[]
	global trec_file
	global QL_full_res_path
	QL_full_res_path='/lv_local/home/elinor/negative_feedback/QL/ql_ret_INEX.txt'
	trec_file="/lv_local/home/elinor/trec_eval.8.1/trec_eval"
	global baseline_home_dir
	global home_dir_eval
	global eval_dir_normal
	global path_cut_res_normal
	global full_res_dir
	baseline_home_dir=home_dir_input
	full_res_dir=baseline_home_dir+'res/'
	home_dir_eval=baseline_home_dir+'eval/'
	path_cut_res_normal=home_dir_eval+'res_1000/'
	eval_dir_normal=home_dir_eval+'normal_eval/'
	folders_paths+=[home_dir_eval, eval_dir_normal,full_res_dir,path_cut_res_normal]
	if flag_residual:
		global ql_residual_eval
		global eval_dir_residual
		global path_cut_res
		global ret_res_dir
		global qrels_dir
		global ql_residual_res
		global ql_residual_res_1000
		eval_dir_residual=home_dir_eval+'residual_collection_eval/'
		path_cut_res=home_dir_eval+'residual_collection_res_1000/'
		ret_res_dir=home_dir_eval+'residual_collection_res/'
		qrels_dir=home_dir_eval+'qrels/'
		ql_residual_eval=home_dir_eval+'QL_residual_eval/'
		ql_residual_res=home_dir_eval+'QL_residual_res/'
		ql_residual_res_1000=home_dir_eval+'QL_residual_res_1000/'
		folders_paths+=[eval_dir_residual, path_cut_res, ret_res_dir, qrels_dir, ql_residual_res,ql_residual_eval,ql_residual_res_1000]
	for folder in folders_paths:
		if not os.path.exists( folder ):
			os.makedirs( folder )
	return True;

"""
##flag_fixe- if true, will bring out the rel in top k results
#k- number of docs returned
#flag_rel- we might want relevant/non-relevant/all

"""


def getting_rel_docs_from_res_file(max_K, flag_fixed ,init_ret_path, qrels_path):
	global Rel_used
	global Neg_used
	global All_used
	Rel_used={}
	Neg_used={}
	All_used={}
	for k in range(1,max_K+1):
		Rel_from_qrel={}##key=query. for each key- list of docs the are positive.
		query_counters={}
		query_counters_neg={}
		## reading all rel docs
		binary_qrel_doc=open(qrels_path,'r')
		for line in binary_qrel_doc:
			queryNum=line.split()[0]
			if line.split()[3]==str(1):
				if (queryNum in Rel_from_qrel.keys()):
					Rel_from_qrel[queryNum]+=[line.split()[2]]
				else:
					Rel_from_qrel[queryNum]=[line.split()[2]]
		binary_qrel_doc.close()
		init_ret_doc=open(init_ret_path,"r")
		Rel_fbd=dict((key, []) for key in Rel_from_qrel.keys())
		all_fbd=dict((key, []) for key in Rel_from_qrel.keys())
		Neg_fbd=dict((key, []) for key in Rel_from_qrel.keys())
		query_counters=query_counters.fromkeys(Rel_from_qrel.keys(),0)
		query_counters_neg=query_counters_neg.fromkeys(Rel_from_qrel.keys(),0)
		if flag_fixed:
			for line in init_ret_doc:
				query=line.split()[0]
				if query_counters[query]<k:
					docnum=line.split()[2]
					query_counters[query]+=1
					if (docnum in Rel_from_qrel[query]):
						Rel_fbd[query]+=[docnum]
						all_fbd[query]+=[docnum]
					else:
						Neg_fbd[query]+=[docnum]
						all_fbd[query]+=[docnum]
		else:
			for line in init_ret_doc:
				query=line.split()[0]
				docnum=line.split()[2]
				if query_counters[query]<k:
					if (docnum in Rel_from_qrel[query]):
						Rel_fbd[query]+=[docnum]
						all_fbd[query]+=[docnum]
						query_counters[query]+=1
						continue
				if query_counters_neg[query]<k:
					if (docnum not in Rel_from_qrel[query]):
						Neg_fbd[query]+=[docnum]
						all_fbd[query]+=[docnum]
						query_counters_neg[query]+=1
		Rel_used[k]=Rel_fbd
		Neg_used[k]=Neg_fbd
		All_used[k]=all_fbd
		init_ret_doc.close()
	return Rel_used,Neg_used,All_used
"""