__author__='Elinor'
import os
import os.path
import sys
import subprocess
import itertools

"""
getting outside variables:
"""
"""
getting outside variables:
"""
klist=xrange(1,11)
if len(sys.argv)<2:
	print "this function expects 1 or more arguments: \n\t(1) source path for the home_dir directory of MultiNeg. \n\t **rerank directory holds the folder parameters and res, with folders that hold the reranking parameters and their results, subfolder for each positive setup file**. \n\t if you wish to run only on part of the sub folder, specify the number of docs used with a space: 1 3 will run only sub-folders that begin with 1 or 3. \n\n\t runs for NRSeg and NRD both simultaneously  "
else:
	if "QL" in sys.argv[1]:
		home_dir='/'.join(sys.argv[1].split('/')[:-1])+'/'
		print home_dir
		rerank_dir=sys.argv[1]+'/rerank_with_NRSeg/'
	else:
		home_dir=sys.argv[1]+'/'
		rerank_dir=home_dir+'/multi_neg/rerank_with_NRSeg/'
	# /lv_local/home/elinor/negative_feedback/SMM/Fixed_RelSeg/multi_neg/rerank_with_NRD
if len(sys.argv)>=3:
	klist=[]
	for i in range(2,len(sys.argv)):
		klist+=[int(sys.argv[i])]

print klist
#/lv_local/home/elinor/negative_feedback/SMM/Fixed_RelSeg/multi_neg/rerank_with_NRD
"""
##based on outside variables:
"""
#folder_source='/lv_local/home/elinor/negative_feedback/trials/trial1/param_chunk/.4'
param_dir=rerank_dir+'parameters/'
res_dir=rerank_dir+'res/'


def run_indri(param_path,res_path):
	batch_indri="/lv_local/home/elinor/indri-5.3/IndriRunQuery_with_mixture_segmants "+param_path+" > "+res_path
	output1 = subprocess.check_output(batch_indri, shell=True)

i=0
for file_dir in sorted(os.listdir(param_dir)):
	if int(file_dir.split('_')[0]) in klist:
		i+=1
		print file_dir, i
		for param_file in os.listdir(param_dir+file_dir):
			res_path=res_dir+file_dir+'/'+param_file
			res_path_NRD=rerank_dir.rstrip('NRSeg//')+'NRD/res/'+file_dir+'/'+param_file.replace("NRSeg","NRD" )
			param_path= param_dir+file_dir+'/'+param_file
			param_path_NRD= rerank_dir.rstrip('NRSeg//')+'NRD/parameters/'+file_dir+'/'+param_file.replace("NRSeg","NRD" )
			if not os.path.exists(res_path):
				run_indri(param_path,res_path)
			if not os.path.exists( res_path_NRD ):
				run_indri(param_path_NRD,res_path_NRD)