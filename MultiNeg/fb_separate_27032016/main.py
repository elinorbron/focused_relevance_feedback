__author__='Elinor'

import sys
import os

"""
In this version the learning is done from all queries.
for the fixed setup, we will drop the queries that does not have relevant documents in top 3 QL, and look at the results of 3 and 5 documents only.
residual removed all feedback documents
"""

scriptpath="/lv_local/home/elinor/pycharm/nf/evaluation/"
#scriptpath="/lv_local/home/elinor/pycharm/nf/MultiNeg/" #there is a reason to that

sys.path.append( os.path.dirname( scriptpath ) )
import subprocess
from cvloo import CVLOO_baseline_eval_folder as cvl
import pickle
# scriptpath="/lv_local/home/elinor/pycharm/evaluation/"
#sys.path.append(os.path.dirname(scriptpath))





def copy_split(source_dir, dest_dir):#dest_dir contains max_K dirs to which we will cpy
	global max_K
	print source_dir
	for i in range( max_K ):
		j=i+1
		batch_spltcp="scp "+source_dir+'/'+str( j )+'_* '+dest_dir+'/'+str( j )+'/ '
		output1=subprocess.check_output( batch_spltcp, shell=True )
	return 1;



def evalfile_to_query_scores_dict(measure_name, eval_file_path):
	dictQueryScore={}
	evalfile=open( eval_file_path, "r" )
	for line in evalfile:
		array_line=line.split( )
		if array_line[0] == str( measure_name ):
			if array_line[1] != 'all':
				dictQueryScore[array_line[1]]=array_line[2].rstrip( '\n' )
	return dictQueryScore;



def build_ql_residual_map_p5(q_list):##goes to the residual directory and takes map all.
	global ql_map_dict
	global ql_p5_dict
	ql_map_dict={}
	ql_p5_dict={}
	for ql_eval_k in os.listdir( ql_residual_evald ):
		j=ql_eval_k.split( '_' )[0]
		templ=[]
		dict_temp_map=evalfile_to_query_scores_dict( 'map', ql_residual_evald+'/'+ql_eval_k )
		for query in sorted( dict_temp_map.keys( ) ):
			if query in q_list:
				templ+=[float( dict_temp_map[query] )]
		ql_map_dict[str( j )]=templ
		templ=[]
		dict_temp_p5=evalfile_to_query_scores_dict( 'P5', ql_residual_evald+'/'+ql_eval_k )
		for query in sorted( dict_temp_p5.keys( ) ):
			if query in q_list:
				templ+=[float( dict_temp_p5[query] )]
		ql_p5_dict[str( j )]=templ
	return 1;

def build_ql_normal_map_p5(ql_normal_Eval,q_list):##goes to the residual directory and takes map all.
	global ql_map_dictN
	global ql_p5_dictN
	ql_map_dictN={}
	ql_p5_dictN={}
	for i in xrange(1,max_K+1): ## all the same but might change if decide to change the query number for each k
		j=str(i)
		templ=[]
		dict_temp_map=evalfile_to_query_scores_dict( 'map', ql_normal_Eval )
		for query in sorted( dict_temp_map.keys( ) ):
			if query in q_list:
				templ+=[float( dict_temp_map[query] )]
		ql_map_dictN[str( j )]=templ
		templ=[]
		dict_temp_p5=evalfile_to_query_scores_dict( 'P5', ql_normal_Eval )
		for query in sorted( dict_temp_p5.keys( ) ):
			if query in q_list:
				templ+=[float( dict_temp_p5[query] )]
		ql_p5_dictN[str( j )]=templ
	return 1;


def cvloo_to_dir(source_dir, Qstat, modeltype,flag_fixed):#dest_dir contains max_K dirs with eval files
	folder=temp_sep_fuse+'tempcvloo/'
	map_dict={}
	p5_dict={}
	cvloo_dict={}
	for eval_dir in sorted(os.listdir( source_dir )):
		k=eval_dir
		#print k
		if flag_fixed:
			temp_res=cvl.cv_loo( source_dir+eval_dir+'/', folder, Qstat[int( 3 )], "Fixed", k,qrels_path_input )
		else:
			temp_res=cvl.cv_loo( source_dir+eval_dir+'/', folder, Qstat[int( k )], "Accu", k ,qrels_path_input)
		map_dict[k]=temp_res['map']
		p5_dict[k]=temp_res['P5']
		cvloo_dict[k]=temp_res
		#person=raw_input(( 'about to erase temp folders for k='+k+': ' ))
		#print('ereasing', person)
		if os.path.exists( folder ):
			if os.path.exists( folder[:-1]+str(k)+"/" ):
				batch_rm="mv "+folder+" "+folder[:-1]+str( k )+"_r/"
			else:
				batch_rm="mv "+folder+" "+folder[:-1]+str(k)+"/"
			subprocess.check_output( batch_rm, shell=True )
		#person=raw_input( 'confirm erased k='+k+': ' )
		#print('continuing', person)
	return cvloo_dict, map_dict, p5_dict;

"""
def cvloo_to_dir(source_dir, Qstat, modeltype):#dest_dir contains max_K dirs with eval files
	folder=temp_sep_fuse+'tempcvloo/'
	map_dict={}
	p5_dict={}
	cvloo_dict={}
	for eval_dir in sorted(os.listdir( source_dir )):
		k=eval_dir
		#print k
		temp_res=cvl.cv_loo( source_dir+eval_dir+'/', folder, Qstat[int( k )], modeltype, k )
		map_dict[k]=temp_res['map']
		p5_dict[k]=temp_res['P5']
		cvloo_dict[k]=temp_res
		#person=raw_input(( 'about to erase temp folders for k='+k+': ' ))
		#print('ereasing', person)
		if os.path.exists( folder ):
			batch_rm="rm -fr "+folder
			subprocess.check_output( batch_rm, shell=True )
		#person=raw_input( 'confirm erased k='+k+': ' )
		#print('continuing', person)
	return cvloo_dict, map_dict, p5_dict;
"""
def cv_loo_fixed_dir(source_dir, Qstat):
	folder=temp_sep_fuse+'tempcvloo/'
	map_dict={}
	p5_dict={}
	cvloo_dict={}
	cvl.define_paths_and_arguments()
	queries_per_cat=Qstat[int( 3 )]["Pos"]
	print "number of queries: "+len(queries_per_cat)
	for eval_dir in sorted( os.listdir( source_dir ) ):
		k=eval_dir
		pureCatdir=pureCatDir= source_dir+'/'+eval_dir+'/pure_pos/'
		if not os.path.exists( pureCatDir ):
			os.makedirs( pureCatDir )
		for evalfile in os.listdir( source_dir ):
			batch_eval="grep -w '"+'\|'.join( queries_per_cat )+"' "+source_dir+evalfile+" | grep -w 'map\|P5 ' > "+pureCatDir+evalfile
			subprocess.check_output( batch_eval, shell=True )
		temp_res=cvl.cv_loo_onedir(pureCatdir, cvl.reported_param_list,folder  )
		map_dict[k]=temp_res['map']
		p5_dict[k]=temp_res['P5']
		cvloo_dict[k]=temp_res
		#person=raw_input(( 'about to erase temp folders for k='+k+': ' ))
		#print('ereasing', person)
		if os.path.exists( folder ):
			batch_rm="rm -fr "+folder
			subprocess.check_output( batch_rm, shell=True )
		#person=raw_input( 'confirm erased k='+k+': ' )
		#print('continuing', person)
	return cvloo_dict, map_dict, p5_dict;

def top_in_dir(source_dir):#dest_dir contains max_K dirs with eval files
	map_dict={}
	p5_dict={}
	for eval_dir in os.listdir( source_dir ):
		for measure, d in zip( ['map', '"P5 "'], [map_dict, p5_dict] ):
			batch_grep="grep "+measure+" "+source_dir+eval_dir+"""/* | grep all | sort -g -k3 | tail -n 1 |  awk '{print $3;}'"""
			val=subprocess.check_output( batch_grep, shell=True )
			batch_setup="grep "+measure+" "+source_dir+eval_dir+"""/* | grep all | sort -g -k3 | tail -n 1 |  awk '{print $1;}'"""
			setup=subprocess.check_output( batch_setup, shell=True )
			setup=setup.split( ':' )[-2].split( '/' )[-1]
			d[eval_dir]={}
			d[eval_dir]['setup']=setup
			d[eval_dir]['val']=val.rstrip( '\n' )
	#print "map", map_dict
	#print "p5", p5_dict
	return map_dict, p5_dict;



def define_folders_for_singleneg(home_dir_input, rerank_setup_name):
	global max_K
	global residual_map_dict
	residual_map_dict={}
	global normal_map_dict
	normal_map_dict={}
	global residual_p5_dict
	residual_p5_dict={}
	global normal_cvloo_dict
	normal_cvloo_dict={}
	global residual_cvloo_dict
	residual_cvloo_dict={}
	global normal_p5_dict
	normal_p5_dict={}
	global home_baseline_dir
	global home_sig_dir
	global ql_residual_evald
	global fusion_residual_evald
	global fusion_normal_evald
	home_baseline_dir=home_dir_input
	home_sig_dir=home_dir_input+rerank_setup_name+'/'

	ql_residual_evald=home_sig_dir+'/eval/residual_eval_all/QL_residual_eval/'
	if not os.path.exists(ql_residual_evald):
		ql_residual_evald=home_sig_dir+'/eval/QL_residual_eval/'

	fusion_residual_evald=home_sig_dir+'eval/residual_eval_all/res_1000_residual_eval/'
	if not os.path.exists( fusion_residual_evald ):
		fusion_residual_evald=home_sig_dir+'eval/res_1000_residual_eval/'

	fusion_normal_evald=home_sig_dir+'eval/res_1000_eval/'
	##dir to make
	dir=[]
	global temp_sep
	global temp_sep_fuse
	global temp_sep_fuse_normal
	global temp_sep_fuse_residual
	global normal_folders
	global residual_folders
	temp_sep=home_dir_input+'temp_sep/'
	temp_sep_fuse=temp_sep+rerank_setup_name+'/'
	temp_sep_fuse_normal=temp_sep_fuse+'normal/'
	temp_sep_fuse_residual=temp_sep_fuse+'residual/'
	if os.path.exists( temp_sep_fuse ):# ##change here and in the line below if no running in parallel and have a disk space problem. after each setup will erase all.
		batch_rm="rm -fr "+temp_sep_fuse
		outputmap=subprocess.check_output( batch_rm, shell=True )
	normal_folders=[]
	residual_folders=[]##add the relative path
	for i in range( max_K ):
		normal_folders+=[temp_sep_fuse_normal+str( i+1 )+'/']
		residual_folders+=[temp_sep_fuse_residual+str( i+1 )+'/']
	dir+=[temp_sep, temp_sep_fuse, temp_sep_fuse_normal, temp_sep_fuse_residual]
	dir+=normal_folders+residual_folders
	for folder in dir:
		print folder
		if not os.path.exists( folder ):
			os.makedirs( folder )
	return 1;



def define_folders_for_base(home_dir_input):
	global max_K
	global residual_map_dict
	residual_map_dict={}
	global normal_map_dict
	normal_map_dict={}
	global normal_cvloo_dict
	normal_cvloo_dict={}
	global residual_cvloo_dict
	residual_cvloo_dict={}
	global residual_p5_dict
	residual_p5_dict={}
	global normal_p5_dict
	normal_p5_dict={}
	global home_baseline_dir
	global ql_residual_evald
	global residual_evald
	global normal_evald
	home_baseline_dir=home_dir_input
	ql_residual_evald=home_baseline_dir+'eval/residual_all/QL_residual_eval/'
	if not os.path.exists( ql_residual_evald ):
		ql_residual_evald=home_baseline_dir+'eval/QL_residual_eval/'

	residual_evald=home_baseline_dir+'eval/residual_all/residual_collection_eval/'
	if not os.path.exists( residual_evald ):
		residual_evald=home_baseline_dir+'eval/residual_collection_eval/'

	normal_evald=home_baseline_dir+'eval/normal_eval/'
	##dir to make
	dir=[]
	global temp_sep
	global temp_sep_fuse
	global temp_sep_fuse_normal
	global temp_sep_fuse_residual
	global normal_folders
	global residual_folders
	rerank_setup_name='non'
	temp_sep=home_dir_input+'temp_sep/'
	temp_sep_fuse=temp_sep+rerank_setup_name+'/'
	temp_sep_fuse_normal=temp_sep_fuse+'normal/'
	temp_sep_fuse_residual=temp_sep_fuse+'residual/'
	if os.path.exists( temp_sep_fuse ):###change here and in the line below if no running in parallel and have a disk space problem. after each setup will erase all.
		batch_rm="rm -fr "+temp_sep_fuse
		outputmap=subprocess.check_output( batch_rm, shell=True )
	normal_folders=[]
	residual_folders=[]##add the relative path
	for i in range( max_K ):
		normal_folders+=[temp_sep_fuse_normal+str( i+1 )+'/']
		residual_folders+=[temp_sep_fuse_residual+str( i+1 )+'/']
	dir+=[temp_sep, temp_sep_fuse, temp_sep_fuse_normal, temp_sep_fuse_residual]
	dir+=normal_folders+residual_folders
	for folder in dir:
		print folder
		if not os.path.exists( folder ):
			os.makedirs( folder )
	return 1;


"""
this code approaches the directory given as input, and for the dirtofb, which is a subset of this list :['multi_neg-NRD_fus_res', 'non', 'multi_neg-combined_fuse_res', 'multi_neg-NRSeg_fus_res'], which specifies the different models we have, creates a temporary folder to which we copy the evaluation files with the same fb of this model, and perform cross-validation leave-one out, after dividing them to subsets of queries according to which feedback information used (i.e. was found in QL). The assumption is that the queries that have different feedback in QL are of different distributions, therefore learning the parameters according to the subset should be more truthful. The learning is done through the CLVOO code, and there if a subset is too small a backoff is made according to are decisions documented in a letex file named learning parameter, attached to the email: "Learning the parameters of the model", 18/09/15. The output of the code is a report including the scores of all queries and the top scores as well, that will be found in reports folder, in sub-directory defined by subreportsdir.
"""

def fb_Sep(subreportsdir,dirtofb,flag_inex):
	#home_dir_input='/lv_local/home/elinor/negative_feedback/baselines/Fixed_RelSeg/'
	reports_home= '/lv_local/home/elinor/'+['robuste_','inex_'][flag_inex]+'reports/'
	if not os.path.exists( reports_home ):
		os.makedirs(  reports_home)
	if not os.path.exists( reports_home+subreportsdir+'/cvloo_mean/' ):
		os.makedirs(reports_home+subreportsdir+'/cvloo_mean/' )
	if not os.path.exists( reports_home+subreportsdir+'/cvloo_list/' ):
		os.makedirs( reports_home+subreportsdir+'/cvloo_list/' )
	home_dir_input=sys.argv[1]+'/'
	setup=home_dir_input.rstrip( '/' ).rstrip( '/' ).split( '/' )[-1].split( '_' )[0]
	full_setup_name=home_dir_input.rstrip( '/' ).rstrip( '/' ).split( '/' )[-1]
	model=home_dir_input.rstrip( '/' ).rstrip( '/' ).split( '/' )[-2]
	print full_setup_name
	sampling=setup
	flag_residual=True
	print setup
	global max_K
	#modeltypedic={'multi_neg-NRD_fus_res': "pos+NRD", 'multi_neg-combined_fuse_res': "full", 'multi_neg-NRSeg_fus_res': "pos+NRSeg",'multi_neg_avg-NRD_fus_res': "pos+NRD", 'multi_neg_avg-combined_fuse_res': "full", 'multi_neg_avg-NRSeg_fus_res': "pos+NRSeg",'single_neg-combined_fuse_res': "full",'single_neg-NRD_fus_res': "pos+NRD", 'single_neg-NRSeg_fus_res': "pos+NRSeg"}
	if setup == "Fixed":
		flag_fixed=True
		Qstat=pickle.load( open( path_inex_fixed, "rb" ) )
		max_K=5
		q_list_QL=Qstat[int( 3 )]["Pos"]
		print "number of queries reported: ",len(q_list_QL)
	else:
		flag_fixed=False
		Qstat=pickle.load( open(path_inex_accu, "rb" ) )
		max_K=5
		q_list_QL=Qstat[int( 1 )]["Pos"] ##accumulative shouldn't matter, always have pos. check when moving on from Inex
	output_text='sampling,model,full_setup_name,fb_docs,rerank_setup,map_normal,p5_normal,map_residual,QLmap_residual,p5_residual,QLp5_residual,,'+'top_map_normal_setup,top_map_normal,top_p5_normal_setup,top_p5_normal,top_map_residual_setup,top_map_residual,top_p5_residual_setup,top_p5_residual,mapQLN,p5QLN'+'\n'
	cvloo_grade_list='sampling,model,full_setup_name,fb_docs,rerank_setup,map_normal,p5_normal,map_residual,p5_residual,QL_map_residual,QL_p5_residual,QL_map_normal,QL_p5_normal'+'\n'
	for rerank_setup in dirtofb:
		print rerank_setup
		if rerank_setup == 'non':
			rerank_setup_name='non'
			modeltype='posonly'
			define_folders_for_base( home_dir_input )
			build_ql_residual_map_p5( q_list_QL )
			build_ql_normal_map_p5(ql_normal_Eval,q_list_QL)
			copy_split( normal_evald, temp_sep_fuse_normal )
			copy_split( residual_evald, temp_sep_fuse_residual )
			normal_map_top, normal_p5_top=top_in_dir( temp_sep_fuse_normal )
			resi_map_top, resi_p5_top=top_in_dir( temp_sep_fuse_residual )
			normal_cvloo_dict, normal_map_dict, normal_p5_dict=cvloo_to_dir( temp_sep_fuse_normal, Qstat, modeltype,flag_fixed )
			residual_cvloo_dict, resi_map_dict, resi_p5_dict=cvloo_to_dir( temp_sep_fuse_residual, Qstat, modeltype,flag_fixed )
		else:
			modeltype='posonly'
			rerank_setup_name=rerank_setup
			define_folders_for_singleneg( home_dir_input, rerank_setup.replace( '-', '/' ) )
			build_ql_residual_map_p5( q_list_QL  )
			build_ql_normal_map_p5( ql_normal_Eval, q_list_QL )
			copy_split( fusion_normal_evald, temp_sep_fuse_normal )
			copy_split( fusion_residual_evald, temp_sep_fuse_residual )
			normal_map_top, normal_p5_top=top_in_dir( temp_sep_fuse_normal )
			resi_map_top, resi_p5_top=top_in_dir( temp_sep_fuse_residual )
			normal_cvloo_dict, normal_map_dict, normal_p5_dict=cvloo_to_dir( temp_sep_fuse_normal, Qstat, modeltype,flag_fixed )
			residual_cvloo_dict, resi_map_dict, resi_p5_dict=cvloo_to_dir( temp_sep_fuse_residual, Qstat, modeltype ,flag_fixed)
		for i in range( max_K ):
			j=str( i+1 )
			cvloo_grade_list+=sampling+','+model+','+full_setup_name+','+str( j )+','+rerank_setup_name+',"'+str( normal_cvloo_dict[j]['map_Glist'] )+'","'+str( normal_cvloo_dict[j]['P5_Glist'] )+'","'+str(residual_cvloo_dict[j]['map_Glist'] )+'","'+str( residual_cvloo_dict[j]['P5_Glist'] )+'","'+str( ql_map_dict[j] )+'","'+str( ql_p5_dict[j] )+'","'+str( ql_map_dictN[j] )+'","'+str( ql_p5_dictN[j] )+'"\n'
			output_text+=sampling+','+model+','+full_setup_name+','+str( j )+','+rerank_setup_name+','+normal_map_dict[j]+','+normal_p5_dict[j]+','+resi_map_dict[j]+','+str(sum( ql_map_dict[j] )/float( len( ql_map_dict[j] ) ) )+','+resi_p5_dict[j]+','+str( sum( ql_p5_dict[j] )/float( len( ql_p5_dict[j] ) ) )+',,'+normal_map_top[j]['setup']+','+normal_map_top[j]['val']+','+normal_p5_top[j]['setup']+','+normal_p5_top[j]['val']+','+resi_map_top[j]['setup']+','+resi_map_top[j]['val']+','+resi_p5_top[j]['setup']+','+resi_p5_top[j]['val']+','+str(sum( ql_map_dictN[j] )/float( len( ql_map_dictN[j] ) ) )+','+str( sum( ql_p5_dictN[j] )/float( len( ql_p5_dictN[j] ) ) )+'\n'
	file=open(reports_home+subreportsdir+'/cvloo_mean/cvloo_'+model+'_'+full_setup_name+'_fb.csv', 'w' )
	file.write( output_text )
	file.close( )
	file=open( reports_home+subreportsdir+'/cvloo_list/cvloo_'+model+'_'+full_setup_name+'_fb.csv', 'w' )
	file.write( cvloo_grade_list )
	file.close( )
	#person=raw_input( 'about to erase temp for all!! ' )
	#print('erasing', person)
	#if os.path.exists( temp_sep ):
	#	batch_rm="rm -fr "+temp_sep
	#	outputmap=subprocess.check_output( batch_rm, shell=True )
	#person=raw_input( 'done and moving on ' )
	#print('erasing', person)

flag_inex=1


if flag_inex:
		init_ret_path_input='/lv_local/home/elinor/negative_feedback/QL/ql_ret_INEX.txt'
		ql_normal_Eval='/lv_local/home/elinor/negative_feedback/QL/ql_ret_INEX_1000_eval.txt'
		qrels_path_input='/lv_local/home/elinor/negative_feedback/Inex_qrelsAndParams/inex2009-2010-article.qrels_binary'
		path_inex_accu= "/lv_local/home/elinor/negative_feedback/Inex_qrelsAndParams/Accumulative_query_separation.p"
		path_inex_fixed="/lv_local/home/elinor/negative_feedback/Inex_qrelsAndParams/Fixed_query_separation.p"
else:
		init_ret_path_input='/lv_local/home/elinor/negative_feedback_experiment_robust/QL/QL_robust_1500.txt'
		ql_normal_Eval='/lv_local/home/elinor/negative_feedback_experiment_robust/QL/QL_robust_1000_eval.txt'
		qrels_path_input='/lv_local/home/elinor/negative_feedback_experiment_robust/robust_qrelsAndParams/qrelsROBUST'
		path_inex_accu="/lv_local/home/elinor/negative_feedback_experiment_robust/robust_qrelsAndParams/Accumulative_query_separation.p"
		path_inex_fixed="/lv_local/home/elinor/negative_feedback_experiment_robust/robust_qrelsAndParams/Fixed_query_separation.p"


sn_flag=0;
mnmax_flag=0;
mn_avg_flag=0;
non_flag=0;
mnmax_flagql=0;
snc_flag=0;
non_noanchoring_flag=0
if "no_anchoring" in sys.argv[1]:
	non_noanchoring_flag=1
elif len(sys.argv)>2:
	for i in xrange(2,len(sys.argv)):
		if sys.argv[i]=="single_neg" or sys.argv[i]=="sn":
			sn_flag=1;
		if sys.argv[i] == "multy_neg" or sys.argv[i] == "mn" or sys.argv[i] == "max" or sys.argv[i] == "mn_max":
			mnmax_flag=1
		if sys.argv[i] == "multy_neg_avg" or sys.argv[i] == "mn_avg" or sys.argv[i] == "avg" or sys.argv[i] == "mn_avg":
			mn_avg_flag=1;
		if sys.argv[i] == "non" or sys.argv[i] == "pos" :
			non_flag=1;
		if sys.argv[i] == "multy_negQL" or sys.argv[i] == "mnql" or sys.argv[i] == "maxql" or sys.argv[i] == "mn_max_ql":
			mnmax_flagql=1;
		if sys.argv[i] == "single_neg_concat" or sys.argv[i] == "snc" :
			snc_flag=1;
else:
	sn_flag=1;
	mnmax_flag=1;
	mn_avg_flag=1;
	non_flag=1;
	mnmax_flagql=1;
	snc_flag=1;
	if os.path.exists( sys.argv[1]+'/temp_sep/' ):
		batch_rm="rm -fr "+sys.argv[1]+'/temp_sep/'
		outputmap=subprocess.check_output( batch_rm, shell=True )



if mnmax_flag:
	if os.path.exists( sys.argv[1]+'/temp_sep/multi_neg/' ):
		batch_rm="rm -fr "+sys.argv[1]+'/temp_sep/multi_neg/'
		outputmap=subprocess.check_output( batch_rm, shell=True )
	fb_Sep( 'multi_neg',['multi_neg-NRD_fus_res', 'multi_neg-combined_fuse_res', 'multi_neg-NRSeg_fus_res'] ,flag_inex)


if non_noanchoring_flag:
	if os.path.exists( sys.argv[1]+'/temp_sep/no_anchoring_non/'):
		batch_rm="rm -fr "+sys.argv[1]+'/temp_sep/no_anchoring_non/'
		outputmap=subprocess.check_output( batch_rm, shell=True )
	fb_Sep( 'no_anchoring_non',['non'],flag_inex  )

if non_flag:
	if os.path.exists( sys.argv[1]+'/temp_sep/non/'):
		batch_rm="rm -fr "+sys.argv[1]+'/temp_sep/non/'
		outputmap=subprocess.check_output( batch_rm, shell=True )
	fb_Sep( 'non',['non'],flag_inex  )

if mn_avg_flag:
	if os.path.exists( sys.argv[1]+'/temp_sep/multi_neg_avg/' ):
		batch_rm="rm -fr "+sys.argv[1]+'/temp_sep/multi_neg_avg/'
		outputmap=subprocess.check_output( batch_rm, shell=True )
	fb_Sep( 'multi_neg_avg',['multi_neg_avg-NRD_fus_res', 'multi_neg_avg-combined_fuse_res', 'multi_neg_avg-NRSeg_fus_res'],flag_inex )
if sn_flag:
	if os.path.exists( sys.argv[1]+'/temp_sep/single_neg/' ):
		batch_rm="rm -fr "+sys.argv[1]+'/temp_sep/single_neg/'
		outputmap=subprocess.check_output( batch_rm, shell=True )
	fb_Sep( 'single_neg',[ 'single_neg-combined_fuse_res', 'single_neg-NRSeg_fus_res', 'single_neg-NRD_fus_res'],flag_inex )

if snc_flag:
	if os.path.exists( sys.argv[1]+'/temp_sep/single_neg_concat/' ):
		batch_rm="rm -fr "+sys.argv[1]+'/temp_sep/single_neg_concat/'
		outputmap=subprocess.check_output( batch_rm, shell=True )
	if os.path.exists( sys.argv[1]+'/single_neg_concat/' ):
		fb_Sep( 'single_neg_concat',[ 'single_neg_concat-combined_fuse_res', 'single_neg_concat-NRD_fus_res'] ,flag_inex)

if mnmax_flagql:
	if os.path.exists( sys.argv[1]+'/temp_sep/multi_negQL/' ):
		batch_rm="rm -fr "+sys.argv[1]+'/temp_sep/multi_negQL/'
		outputmap=subprocess.check_output( batch_rm, shell=True )
	if os.path.exists( sys.argv[1]+'/multi_negQL/' ):
		fb_Sep( 'multi_negQL',['multi_negQL-NRD_fus_res', 'multi_negQL-combined_fuse_res', 'multi_negQL-NRSeg_fus_res'],flag_inex )