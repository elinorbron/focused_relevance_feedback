__author__='Elinor'
import os
import os.path
import sys
import numpy as np
"""
getting outside variables:
"""


#for setup
##rerank with nrseg:
klist=xrange(1,11)
if len(sys.argv)<2:
	print "this function expects 1 or more arguments: \n\t(1) source path for the home directory of the model. \n\t example:/lv_local/home/elinor/negative_feedback/SMM/Fixed_RelSeg/ . \n\t if you wish to run only on part of the files, specify the number of docs used with a space: 1 3 will run only files that begin with 1 or 3.  "
else:
	home=sys.argv[1]+'/'
	# /lv_local/home/elinor/negative_feedback/SMM/Fixed_RelSeg/
if len(sys.argv)>=3:
	klist=[]
	for i in range(2,len(sys.argv)):
		klist+=[int(sys.argv[i])]

print klist
"""
##based on outside variables:
effect functions below.
"""
home=sys.argv[1]+'/'
full_setup_name=home.rstrip( '/' ).rstrip( '/' ).split( '/' )[-1]
setup=full_setup_name.split( '_' )[0]
model=home.rstrip( '/' ).rstrip( '/' ).split( '/' )[-2]
multy=home+'/multi_neg/'
print "sampling setup", setup
print "model", model
print "full_setup_name", full_setup_name

#print "setup input is"+setup
flag_residual=True

if setup=="Fixed":
	print "Fixed"
	flag_fixed=True
	max_K=10
else:
	print "NonFixed"
	flag_fixed=False
	max_K=5
################

str_queries_path='/lv_local/home/elinor/negative_feedback/Inex_qrelsAndParams/INEX_params_b_segQ.xml'
init_ret_path='/lv_local/home/elinor/negative_feedback/QL/ql_ret_INEX.txt'
binary_qrels_path='/lv_local/home/elinor/negative_feedback/Inex_qrelsAndParams/inex2009-2010-article.qrels_binary'
alpha_setup=np.arange(0.0,1.2,0.2)
beta_setup=np.arange(0.0,1.2,0.2)


def create_folders(home_rerank_dir):
	home_rerank_pos=home_rerank_dir
	if not os.path.exists(home_rerank_pos):
		os.makedirs(home_rerank_pos)
	if not os.path.exists( home_rerank_pos+'res_1000/' ):
		os.makedirs(home_rerank_pos+'res_1000/')
	if not os.path.exists( home_rerank_pos+'res_1000_residual/' ):
		os.makedirs(home_rerank_pos+'res_1000_residual/')


def read_queries_to_dict( str_path, origin_q_dic ):
	f=open(str_path,'r')
	for line in f:
		if 'number' in line:
			q_id=line.strip('\t\t<number>')
			q_id=q_id.rstrip('</number>\n\t\t')
			origin_q_dic[q_id]=str()
	return origin_q_dic