__author__='Elinor'
scriptpath="/lv_local/home/elinor/pycharm/nf/evaluation/"
sys.path.append(os.path.dirname(scriptpath))
import os
import os.path
import sys

from cvloo import CVLOO_baseline_eval_folder as cvl
from single_neg_eval import normal_eval_rerank



source_dir=sys.argv[1]+'/'
source=source_dir+'/single_neg/res_1000/'
dest=source_dir+'/single_neg/eval/res_1000/'
normal_eval_rerank.trec_eval_normal_on_1000(source,dest)
cvl.cv_loo( dest, dest.rstrip( '/' )+'_cvloo/' )



