from drafts.single_neg_eval import rerank_eval_settings as es, normal_eval_rerank, eval_residual_for_rerank



__author__='Elinor'
import sys
import os
scriptpath="/lv_local/home/elinor/pycharm/evaluation/"
sys.path.append(os.path.dirname(scriptpath))
from cvloo import CVLOO_baseline_eval_folder as cvl

########################## when both, do 2,3. don't forget to pass the NRD concat flag
rerank_setup_list=[3]
##########################

"""
this file take the baseline setup folder and creates an eval folder with all the eval files needed- for both rerank folders:
for positive and negative
same(For positive and negative):
Qrels_for_residual(for comparison later)
1.Ql_residual
2.Ql_residual_eval
diff:
1.normal_eval
2.normal_eval_top1000
3.eval_residual
4.eval_residual_at_top1000
"""
feedback_setup_name_list=['RelSeg','NRSeg','NRD','RelD','PRF']
rerank_setup_name_list=['RelSeg','NRSeg','NRD','RelD','PRF']
init_ret_path_input='/lv_local/home/elinor/negative_feedback/QL/ql_ret_INEX.txt'
qrels_path_input='/lv_local/home/elinor/negative_feedback/Inex_qrelsAndParams/inex2009-2010-article.qrels_binary'

"""
home_dir_input='/lv_local/home/elinor/negative_feedback/baselines/baseline_retrival_with_Rdocs'+'/'
setup="Fixed"
"""

home_dir_input=sys.argv[1]+'/'
setup=home_dir_input.rstrip('/').rstrip('/').split('_')[-2].split('/')[-1]

flag_SMMP= "SMMP" in home_dir_input
if flag_SMMP:
	print "this is done under SMMP setup"
	initial_feedback_setup_name=home_dir_input.rstrip( '/' ).rstrip( '/' ).split( '_' )[-2]+'_'+home_dir_input.rstrip( '/' ).rstrip( '/' ).split( '_' )[-1]
	setup=home_dir_input.rstrip( '/' ).rstrip( '/' ).split( '/' )[-1].split( '_' )[0]
else:
	print "this is NOT done under SMMP setup"

flag_residual=True
print setup
if setup=="Fixed":
	flag_fixed=True
	max_K=10
else:
	flag_fixed=False
	max_K=5



def create_residual_res_file(source_file_path,dest_file_path,dic_docs_remove_per_q):
	file=open(source_file_path,'r')
	query_counters=dic_docs_remove_per_q.fromkeys(dic_docs_remove_per_q.keys(),0)
	query_moved=dic_docs_remove_per_q.fromkeys(dic_docs_remove_per_q.keys(),False)
	file_output=str()
	for line in file:
		queryNum=line.split()[0]
		docnum=line.split()[2]
		if docnum in dic_docs_remove_per_q[queryNum]:
			query_moved[queryNum]=True;
			query_counters[queryNum]+=1
			continue;
		if query_moved[queryNum]:
			templine=line.split()
			templine[3]=str(int(templine[3])-query_counters[queryNum])
			for word in templine:
				file_output+=word+' '
			file_output+=('\n')
		else:
			file_output+=line
	new_res_file=open(dest_file_path,'w+')
	new_res_file.write(file_output)
	new_res_file.close()

def create_residual_Qrels_file(source_file_path,dest_file_path,dic_docs_remove_per_q):
	file=open(source_file_path,'r')
	file_output=str()
	for line in file:
		queryNum=line.split( )[0]
		docnum=line.split( )[2]
		if docnum not in dic_docs_remove_per_q[queryNum]:
			file_output+=line
	new_res_file=open( dest_file_path, 'w+' )
	new_res_file.write( file_output )
	new_res_file.close( )
	return True



es.init_path(init_ret_path_input,qrels_path_input)
es.getting_rel_docs_from_res_file(max_K, flag_fixed ,es.init_ret_path, es.qrels_path)
es.create_eval_Folders( home_dir_input)
# #eval normal: 1. evaluate 2.cv_loo (different file)
normal_eval_rerank.trec_eval_normal_on_1000( es.n_rerank_res_normal, es.n_sub_eval_normal )
cvl.cv_loo( es.n_sub_eval_normal, es.n_sub_eval_normal.rstrip( '/' )+'_cvloo/' )
eval_residual_for_rerank.sum_data_dir( es.n_sub_eval_normal, 'map', True, True, es.eval_rerank_home_dir, str( k )+"_n_normal_eval" )
eval_residual_for_rerank.sum_data_dir( es.n_sub_eval_normal, '"P5 "', True, True, es.eval_rerank_home_dir, str( k )+"_n_normal_eval" )
for file in es.single_neg_home_dir:
	nrd_used=float(file.split('_')[-4])>0.0
	nrseg_used=float(file.split('_')[-2])>0.0
	fbdoc=int(file.split('_')[0])
	if nrd_used:
		files_used=es.All_used[fbdoc]


	else:
		files_used=es.Rel_used[fbdoc]
	##create qrels for The case of residual evaluation when only NRSeg is used::
	for k in range( 1, max_K+1 ):# #creating Qrels, needed for eval, QL_comparable baseline
		new_qrels_file_path=es.qrels_dir+str( k )+'_'+es.baseline_home_dir.rstrip( '/' ).split( '_' )[-1]+'_residual_collection.qrels_binary'
		create_residual_Qrels_file( es.qrels_path, new_qrels_file_path,es.Rel_used[k] )
		new_QL_file_path=es.ql_residual_res+str( k )+'_'+es.baseline_home_dir.rstrip( '/' ).split( '_' )[-1]+'_ql_residual_res'
		create_residual_res_file( es.QL_full_res_path, new_QL_file_path,es.Rel_used[k] )
	eval_residual_for_rerank.QL_residual_eval( )
	eval_residual_for_rerank.sum_data_dir( es.ql_residual_eval, "map", True, True, es.eval_rerank_home_dir, "ql_residual_eval" )
	eval_residual_for_rerank.sum_data_dir( es.ql_residual_eval, '"P5 "', True, True, es.eval_rerank_home_dir, "ql_residual_eval" )
	for Flag_positive in [False]:#True, was brought out. put back for evaluating positive as well

		##eval_residual
		eval_residual_for_rerank.eval_residual_chopped( es.n_rerank_res_residual, es.n_sub_eval_residual, es.qrels_dir )
		eval_residual_for_rerank.sum_data_dir( es.n_sub_eval_residual, "map", True, False, es.eval_rerank_home_dir, str( k )+"_n_residual_eval" )
		eval_residual_for_rerank.sum_data_dir( es.n_sub_eval_residual, '"P5 "', True, False, es.eval_rerank_home_dir,str( k )+ "_n_residual_eval" )
		cvl.cv_loo( es.n_sub_eval_residual, es.n_sub_eval_residual.rstrip('/')+'_cvloo/' )

