__author__='Elinor'
import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt
import pandas as pd
import numpy as np
import os
import sys
from scipy.stats import ttest_rel

alpha=0.05
if not os.path.exists( '/lv_local/home/elinor/negative_feedback/reports/' ):
	os.makedirs( '/lv_local/home/elinor/negative_feedback/reports/' )
if not os.path.exists( '/lv_local/home/elinor/negative_feedback/reports/data_comparable_plots/' ):
	os.makedirs( '/lv_local/home/elinor/negative_feedback/reports/data_comparable_plots/'  )
if not os.path.exists( '/lv_local/home/elinor/negative_feedback/reports/comparable_plots/' ):
		os.makedirs( '/lv_local/home/elinor/negative_feedback/reports/comparable_plots/' )
plotsdir='/lv_local/home/elinor/negative_feedback/reports/comparable_plots/'
csv_source="/lv_local/home/elinor/negative_feedback/reports/list_all_141115.csv"
#csv_source="/lv_local/home/elinor/negative_feedback/reports/list_all_SN.csv"
#csv_source="/lv_local/home/elinor/negative_feedback/reports/list_all_MN.csv"


lsamp=['Accumulative','Accumulative','Accumulative','Accumulative','Accumulative','Accumulative','Accumulative','Accumulative','Accumulative','Accumulative','Accumulative','Accumulative','Accumulative','Accumulative','Accumulative','Accumulative','Accumulative','Accumulative','Accumulative','Accumulative','Accumulative','Accumulative','Accumulative','Accumulative','Fixed','Fixed','Fixed','Fixed','Fixed','Fixed','Fixed','Fixed','Fixed','Fixed','Fixed','Fixed','Fixed','Fixed','Fixed','Fixed','Fixed','Fixed','Fixed','Fixed','Fixed','Fixed','Fixed','Fixed']
lmes=['map_normal','map_normal','map_normal','map_normal','map_normal','map_normal','p5_normal','p5_normal','p5_normal','p5_normal','p5_normal','p5_normal','map_residual','map_residual','map_residual','map_residual','map_residual','map_residual','p5_residual','p5_residual','p5_residual','p5_residual','p5_residual','p5_residual','map_normal','map_normal','map_normal','map_normal','map_normal','map_normal','p5_normal','p5_normal','p5_normal','p5_normal','p5_normal','p5_normal','map_residual','map_residual','map_residual','map_residual','map_residual','map_residual','p5_residual','p5_residual','p5_residual','p5_residual','p5_residual','p5_residual']
ldit=['all','indifferent','improved','significance','Pimproved','Psignif','all','indifferent','improved','significance','Pimproved','Psignif','all','indifferent','improved','significance','Pimproved','Psignif','all','indifferent','improved','significance','Pimproved','Psignif','all','indifferent','improved','significance','Pimproved','Psignif','all','indifferent','improved','significance','Pimproved','Psignif','all','indifferent','improved','significance','Pimproved','Psignif','all','indifferent','improved','significance','Pimproved','Psignif']
arrays=[lsamp,lmes,ldit]
tuples = list(zip(*arrays))
index = pd.MultiIndex.from_tuples(tuples, names=['first', 'second','third'])

table_rearank=pd.DataFrame(index=index)
table_distill=pd.DataFrame(index=index)
table_distill_rerank=pd.DataFrame(index=index)
table_DocSeg=pd.DataFrame(index=index)

def readNclean(csv_source):
	global table
	table=pd.DataFrame.from_csv( csv_source )
	table=table.drop(['sampling'],axis=0)#lines of text and not data
	table=table.reset_index( )
	table=table.dropna(1)
	table=table.drop_duplicates()
	#print table
	for mes in ['map','p5']:
		for ev in ['residual', 'normal']:
			table['list_'+mes+'_'+ev]=table[mes+'_'+ev].apply( lambda x: x.rstrip( ']' ).strip( '[' ).split( ',' ) ).apply(lambda x: [float(i) for i in x] )
			table[mes+'_'+ev]=table['list_'+mes+'_'+ev].apply( np.mean )
	#table.rename( columns={'$a': 'a', '$b': 'b'} ,inplace=True)
	table['list_ql_residual_p5']=table['QL_p5_residual'].apply( lambda x: x.rstrip( ']' ).strip( '[' ).split( ',' ) ).apply( lambda x: [float( i.strip( ).strip( "'" ).strip( ) ) for i in x] )
	table['QL_p5_residual']=table['list_ql_residual_p5'].apply( np.mean )
	table['list_ql_residual_map']=table['QL_map_residual'].apply( lambda x: x.rstrip( ']' ).strip( '[' ).split( ',' ) ).apply( lambda x: [float( i.strip( ).strip( "'" ).strip( ) ) for i in x] )
	table['QL_map_residual']=table['list_ql_residual_map'].apply( np.mean )
	table['QL_map_normal']=0.3684
	table['QL_p5_normal']=0.6117
	table['fb_docs']=table['fb_docs'].apply( lambda x: int(x))
	#print table
	table.to_csv('/lv_local/home/elinor/negative_feedback/reports/data_comparable_plots/datamean.csv',index=False)
	#for i in 'p5_normal,QLmap_residual,p5_residual,QLp5_residual,top_map_normal_setup,top_map_normal,top_p5_normal_setup,top_p5_normal,top_map_residual_setup,top_map_residual,top_p5_residual_setup,top_p5_residual,mapQLN,p5QLN'.split(','):
		#table=table.drop( [i], axis=1 )
	return 1;



readNclean(csv_source)
all_table=table


#print table
SMM=table[table['model']=='SMM']
SMM=SMM[['full_setup_name','rerank_setup','fb_docs','map_normal','p5_normal','map_residual','p5_residual','QL_map_residual','QL_p5_residual','QL_map_normal','QL_p5_normal']]
SMMP=table[table['model']=='SMMP']
SMMP=SMMP[['full_setup_name','rerank_setup','fb_docs','map_normal','p5_normal','map_residual','p5_residual','QL_map_residual','QL_p5_residual','QL_map_normal','QL_p5_normal']]
SMMPP=table[table['model']=='SMMPP']
SMMPP=SMMPP[['full_setup_name','rerank_setup','fb_docs','map_normal','p5_normal','map_residual','p5_residual','QL_map_residual','QL_p5_residual','QL_map_normal','QL_p5_normal']]
RM=table[table['model']=='RM']
RM=RM[['full_setup_name','rerank_setup','fb_docs','map_normal','p5_normal','map_residual','p5_residual','QL_map_residual','QL_p5_residual','QL_map_normal','QL_p5_normal']]

SMM_SMMP=table[(table['model']=='SMM')|(table['model']=='SMMP')]
SMM_SMMP=SMM_SMMP[['full_setup_name','model','rerank_setup','fb_docs','map_normal','p5_normal','map_residual','p5_residual','QL_map_residual','QL_p5_residual','QL_map_normal','QL_p5_normal']]
SMM_SMMP['distil']=SMM_SMMP['full_setup_name'].apply( lambda x: x.split( '_' )[2] if len( x.split( '_' ) ) > 2 else 'non' ) # if SMMP will hold distilation source. else will hold Non]

SMM_all=table[(table['model']=='SMM')|(table['model']=='SMMP')|(table['model']=='SMMPP')]
SMM_all=SMM_all[['full_setup_name','model','rerank_setup','fb_docs','map_normal','p5_normal','map_residual','p5_residual','QL_map_residual','QL_p5_residual','QL_map_normal','QL_p5_normal']]
#SMM_all['distil']=SMM_all['full_setup_name'].apply( lambda x: x.split( '_' )[2] if len( x.split( '_' ) ) ==3 else 'non' ) # if SMMP will hold distilation source. else will hold Non]
SMM_all['distil']=SMM_all['full_setup_name'].apply( lambda x: '_'.join(x.split( '_' )[2:]) if len( x.split( '_' ) ) >2 else 'non' ) # if SMMP will hold distilation source. else will hold Non]
#print SMM_all

"comparing everything with everything for a certain sampling model, evaluation method, and mesure"
def RelsegVSRelD(posx,posy):
	for sampling in ['Accumulative','Fixed']:
		for eval_method in ["normal", 'residual']:
			for mesure in ["map", "p5"]:
				#print '######################################################################################################################################################\n RelSeg better than RelD? #### \n\tdistillation: non\n\tmesure: '+mesure+'\n\tevaluation: '+eval_method+'\n\tsampling: '+sampling+'####################################################################'
				source_narrow=table[table['sampling'] == sampling][['model', 'full_setup_name', 'rerank_setup', 'fb_docs', mesure+'_'+eval_method, 'list_'+mesure+'_'+eval_method]]# ,'QL_'+mesure+'_'+eval_method]]
				source_narrow['pos']=source_narrow['full_setup_name'].apply( lambda x: str( x ).split( '_' )[1] )
				source_narrow['distil']=source_narrow['full_setup_name'].apply( lambda x: '_'.join( x.split( '_' )[2:] ) if len( x.split( '_' ) ) > 2 else 'non' )
				source_narrow['signeg']=source_narrow['rerank_setup'].apply( lambda x: (x.split( '-' )[0]) if len( x.split( '-' ) ) > 1 else 'non' )
				source_narrow['signeg'][source_narrow.signeg == 'multi_neg']='max'
				source_narrow['signeg'][source_narrow.signeg == 'multi_neg_avg']='avg'
				source_narrow['signeg'][source_narrow.signeg == 'single_neg']='sn'
				source_narrow['rerank_source']=source_narrow['rerank_setup'].apply( lambda x: (x.split( '-' )[1]) if len( x.split( '-' ) ) > 1 else 'non' ).apply( lambda x: (x.split( '_' )[0]) )
				source_narrow['uniq_key']=source_narrow['model']+'_'+source_narrow['full_setup_name']+'_'+source_narrow['rerank_setup']
				source_narrow['key']=1
				source_narrow.reset_index( )
				product=pd.merge( source_narrow, source_narrow, on='key' )
				product=product[product.fb_docs_x == product.fb_docs_y]
				product=product[product.uniq_key_x != product.uniq_key_y]
				# print new[[first, second]]
				product['significance']=product[['list_'+mesure+'_'+eval_method+'_x', 'list_'+mesure+'_'+eval_method+'_y']].apply( lambda x: 1 if (ttest_rel( x[0], x[1] )[1] < alpha) else 0, axis=1 )
				# product['improvement']=product[['list_'+mesure+'_'+eval_method+'_x', 'list_'+mesure+'_'+eval_method+'_y']].apply( lambda x: sum( [i-j for i, j in zip( x[0], x[1] )] )/len( x[0] ), axis=1 )
				product['improvement']=product[[mesure+'_'+eval_method+'_x', mesure+'_'+eval_method+'_y']].apply( lambda x: 1 if (x[0] > x[1]) else 0, axis=1 )
				product['indiff']=product[[mesure+'_'+eval_method+'_x', mesure+'_'+eval_method+'_y']].apply( lambda x: 1 if (x[0] == x[1]) else 0, axis=1 )
				#peint product[['significance','improvement','model_x','model_y','full_setup_name_x','full_setup_name_y','rerank_setup_x','rerank_setup_y','fb_docs_x','fb_docs_y']]

				# #relseg vs. reld - all cases (under eval method/samping/mesuare)
				all_cases=product[product.rerank_setup_x == product.rerank_setup_y][product.pos_x == posx][product.pos_y == posy][product.model_x == product.model_y][product.distil_x == product.distil_y]
				tempi=all_cases[all_cases.improvement == 1].shape[0]
				temps=all_cases[all_cases.improvement == 1][all_cases.significance == 1].shape[0]
				tempd=all_cases[all_cases.indiff == 1].shape[0]
				SegVSDocs=posx+"_vs_"+posy
				table_DocSeg.ix[[(sampling, mesure+'_'+eval_method, 'all')], SegVSDocs+"_all"]=all_cases.shape[0]
				table_DocSeg.ix[[(sampling, mesure+'_'+eval_method, 'indifferent')],SegVSDocs+"_all"]=tempd
				table_DocSeg.ix[[(sampling, mesure+'_'+eval_method, 'improved')], SegVSDocs+"_all"]=tempi
				table_DocSeg.ix[[(sampling, mesure+'_'+eval_method, 'significance')], SegVSDocs+"_all"]=temps
				table_DocSeg.ix[[(sampling, mesure+'_'+eval_method, 'Pimproved')], SegVSDocs+"_all"]=float(tempi)/float(all_cases.shape[0]-tempd)
				table_DocSeg.ix[[(sampling, mesure+'_'+eval_method, 'Psignif')], SegVSDocs+"_all"]=float(temps)/max(float(tempi),1.0)
				table_DocSeg.ix[[(sampling, mesure+'_'+eval_method, 'all')], SegVSDocs+"_all_nodistill"]=all_cases[all_cases.distil_x == 'non'].shape[0]
				table_DocSeg.ix[[(sampling, mesure+'_'+eval_method, 'indifferent')], SegVSDocs+"_all_nodistill"]=all_cases[all_cases.distil_x == 'non'][all_cases.indiff == 1].shape[0]
				table_DocSeg.ix[[(sampling, mesure+'_'+eval_method, 'improved')], SegVSDocs+"_all_nodistill"]=all_cases[all_cases.improvement == 1][all_cases.distil_x == 'non'].shape[0]
				table_DocSeg.ix[[(sampling, mesure+'_'+eval_method, 'significance')], SegVSDocs+"_all_nodistill"]=all_cases[all_cases.improvement == 1][all_cases.distil_x == 'non'][all_cases.significance == 1].shape[0]
				table_DocSeg.ix[[(sampling, mesure+'_'+eval_method, 'Pimproved')], SegVSDocs+"_all_nodistill"]=float(all_cases[all_cases.improvement == 1][all_cases.distil_x == 'non'].shape[0])/float(all_cases[all_cases.distil_x == 'non'][all_cases.indiff == 0].shape[0])
				table_DocSeg.ix[[(sampling, mesure+'_'+eval_method, 'Psignif')], SegVSDocs+"_all_nodistill"]=float(all_cases[all_cases.improvement == 1][all_cases.distil_x == 'non'][all_cases.significance == 1].shape[0])/max(float(all_cases[all_cases.improvement == 1][all_cases.distil_x == 'non'].shape[0]),1.0)
				for rerank in ['non', 'NRSeg', 'NRD', 'combined']:
					tempi=all_cases[all_cases.improvement == 1][all_cases.distil_x == 'non'][all_cases.rerank_source_x == rerank].shape[0]
					temps=all_cases[all_cases.improvement == 1][all_cases.distil_x == 'non'][all_cases.rerank_source_x == rerank][all_cases.significance == 1].shape[0]
					table_DocSeg.ix[[(sampling, mesure+'_'+eval_method, 'all')], SegVSDocs+"_all_nodistill_reranksource_"+rerank]=all_cases[all_cases.distil_x == 'non'][all_cases.rerank_source_x == rerank].shape[0]
					table_DocSeg.ix[[(sampling, mesure+'_'+eval_method, 'indifferent')], SegVSDocs+"_all_nodistill_reranksource_"+rerank]=all_cases[all_cases.distil_x == 'non'][all_cases.rerank_source_x == rerank][all_cases.indiff == 1].shape[0]
					table_DocSeg.ix[[(sampling, mesure+'_'+eval_method, 'improved')],SegVSDocs+"_all_nodistill_reranksource_"+rerank]=tempi
					table_DocSeg.ix[[(sampling, mesure+'_'+eval_method, 'significance')],SegVSDocs+"_all_nodistill_reranksource_"+rerank]=temps
					table_DocSeg.ix[[(sampling, mesure+'_'+eval_method, 'Pimproved')],SegVSDocs+"_all_nodistill_reranksource_"+rerank]=float( tempi )/float( all_cases[all_cases.distil_x == 'non'][all_cases.rerank_source_x == rerank][all_cases.indiff == 0].shape[0])
					table_DocSeg.ix[[(sampling, mesure+'_'+eval_method, 'Psignif')], SegVSDocs+"_all_nodistill_reranksource_"+rerank]=float( temps )/max( float( tempi ), 1.0 )



"comparing everything with everything for a certain sampling model, evaluation method, and mesure"
def RelsegVS_RelDwithrerank():
	for sampling in ['Accumulative','Fixed']:
		for eval_method in ["normal", 'residual']:
			for mesure in ["map", "p5"]:
				#print '######################################################################################################################################################\n RelSeg better than RelD? #### \n\tdistillation: non\n\tmesure: '+mesure+'\n\tevaluation: '+eval_method+'\n\tsampling: '+sampling+'####################################################################'
				source_narrow=table[table['sampling'] == sampling][['model', 'full_setup_name', 'rerank_setup', 'fb_docs', mesure+'_'+eval_method, 'list_'+mesure+'_'+eval_method]]# ,'QL_'+mesure+'_'+eval_method]]
				source_narrow['pos']=source_narrow['full_setup_name'].apply( lambda x: str( x ).split( '_' )[1] )
				source_narrow['distil']=source_narrow['full_setup_name'].apply( lambda x: '_'.join( x.split( '_' )[2:] ) if len( x.split( '_' ) ) > 2 else 'non' )
				source_narrow['signeg']=source_narrow['rerank_setup'].apply( lambda x: (x.split( '-' )[0]) if len( x.split( '-' ) ) > 1 else 'non' )
				source_narrow['signeg'][source_narrow.signeg == 'multi_neg']='max'
				source_narrow['signeg'][source_narrow.signeg == 'multi_neg_avg']='avg'
				source_narrow['signeg'][source_narrow.signeg == 'single_neg']='sn'
				source_narrow['rerank_source']=source_narrow['rerank_setup'].apply( lambda x: (x.split( '-' )[1]) if len( x.split( '-' ) ) > 1 else 'non' ).apply( lambda x: (x.split( '_' )[0]) )
				source_narrow['uniq_key']=source_narrow['model']+'_'+source_narrow['full_setup_name']+'_'+source_narrow['rerank_setup']
				source_narrow['key']=1
				source_narrow.reset_index( )
				product=pd.merge( source_narrow, source_narrow, on='key' )
				product=product[product.fb_docs_x == product.fb_docs_y]
				product=product[product.uniq_key_x != product.uniq_key_y]
				# print new[[first, second]]
				product['significance']=product[['list_'+mesure+'_'+eval_method+'_x', 'list_'+mesure+'_'+eval_method+'_y']].apply( lambda x: 1 if (ttest_rel( x[0], x[1] )[1] < alpha) else 0, axis=1 )
				# product['improvement']=product[['list_'+mesure+'_'+eval_method+'_x', 'list_'+mesure+'_'+eval_method+'_y']].apply( lambda x: sum( [i-j for i, j in zip( x[0], x[1] )] )/len( x[0] ), axis=1 )
				product['improvement']=product[[mesure+'_'+eval_method+'_x', mesure+'_'+eval_method+'_y']].apply( lambda x: 1 if (x[0] > x[1]) else 0, axis=1 )
				product['indiff']=product[[mesure+'_'+eval_method+'_x', mesure+'_'+eval_method+'_y']].apply( lambda x: 1 if (x[0] == x[1]) else 0, axis=1 )
				#peint product[['significance','improvement','model_x','model_y','full_setup_name_x','full_setup_name_y','rerank_setup_x','rerank_setup_y','fb_docs_x','fb_docs_y']]

				# #relseg vs. reld - all cases (under eval method/samping/mesuare)
				all_cases=product[product.rerank_source_x =='non' ][product.pos_x == 'RelSeg'][product.pos_y == 'RelD'][product.model_x == product.model_y][product.distil_x == product.distil_y]
				tempi=all_cases[all_cases.improvement == 1].shape[0]
				temps=all_cases[all_cases.improvement == 1][all_cases.significance == 1].shape[0]
				tempd=all_cases[all_cases.indiff == 1].shape[0]
				table_DocSeg.ix[[(sampling, mesure+'_'+eval_method, 'all')], "SegnonVSDocs_all"]=all_cases.shape[0]
				table_DocSeg.ix[[(sampling, mesure+'_'+eval_method, 'indifferent')], "SegnonVSDocs_all"]=tempd
				table_DocSeg.ix[[(sampling, mesure+'_'+eval_method, 'improved')], "SegnonVSDocs_all"]=tempi
				table_DocSeg.ix[[(sampling, mesure+'_'+eval_method, 'significance')], "SegnonVSDocs_all"]=temps
				table_DocSeg.ix[[(sampling, mesure+'_'+eval_method, 'Pimproved')], "SegnonVSDocs_all"]=float(tempi)/float(all_cases.shape[0]-tempd)
				table_DocSeg.ix[[(sampling, mesure+'_'+eval_method, 'Psignif')], "SegnonVSDocs_all"]=float(temps)/max(float(tempi),1.0)
				table_DocSeg.ix[[(sampling, mesure+'_'+eval_method, 'all')], "SegnonVSDocs_all"]=all_cases[all_cases.distil_x == 'non'].shape[0]
				table_DocSeg.ix[[(sampling, mesure+'_'+eval_method, 'indifferent')], "SegnonVSDocs_all"]=all_cases[all_cases.distil_x == 'non'][all_cases.indiff == 1].shape[0]
				table_DocSeg.ix[[(sampling, mesure+'_'+eval_method, 'improved')], "SegnonVSDocs_all"]=all_cases[all_cases.improvement == 1][all_cases.distil_x == 'non'].shape[0]
				table_DocSeg.ix[[(sampling, mesure+'_'+eval_method, 'significance')], "SegnonVSDocs_all"]=all_cases[all_cases.improvement == 1][all_cases.distil_x == 'non'][all_cases.significance == 1].shape[0]
				table_DocSeg.ix[[(sampling, mesure+'_'+eval_method, 'Pimproved')], "SegnonVSDocs_all"]=float(all_cases[all_cases.improvement == 1][all_cases.distil_x == 'non'].shape[0])/float(all_cases[all_cases.distil_x == 'non'][all_cases.indiff == 0].shape[0])
				table_DocSeg.ix[[(sampling, mesure+'_'+eval_method, 'Psignif')], "SegnonVSDocs_all"]=float(all_cases[all_cases.improvement == 1][all_cases.distil_x == 'non'][all_cases.significance == 1].shape[0])/max(float(all_cases[all_cases.improvement == 1][all_cases.distil_x == 'non'].shape[0]),1.0)
				for rerank in ['non', 'NRSeg', 'NRD', 'combined']:
					tempi=all_cases[all_cases.improvement == 1][all_cases.distil_x == 'non'][all_cases.rerank_source_y == rerank].shape[0]
					temps=all_cases[all_cases.improvement == 1][all_cases.distil_x == 'non'][all_cases.rerank_source_y == rerank][all_cases.significance == 1].shape[0]
					print all_cases[all_cases.improvement == 1][all_cases.distil_x == 'non'][all_cases.rerank_source_y == rerank]
					table_DocSeg.ix[[(sampling, mesure+'_'+eval_method, 'all')], "SegnonVSDocs_nodistill_reranksource_"+rerank]=all_cases[all_cases.distil_x == 'non'][all_cases.rerank_source_y == rerank].shape[0]
					table_DocSeg.ix[[(sampling, mesure+'_'+eval_method, 'indifferent')], "SegnonVSDocs_nodistill_reranksource_"+rerank]=all_cases[all_cases.distil_x == 'non'][all_cases.rerank_source_y == rerank][all_cases.indiff == 1].shape[0]
					table_DocSeg.ix[[(sampling, mesure+'_'+eval_method, 'improved')], "SegnonVSDocs_nodistill_reranksource_"+rerank]=tempi
					table_DocSeg.ix[[(sampling, mesure+'_'+eval_method, 'significance')],"SegnonVSDocs_nodistill_reranksource_"+rerank]=temps
					table_DocSeg.ix[[(sampling, mesure+'_'+eval_method, 'Pimproved')],"SegnonVSDocs_nodistill_reranksource_"+rerank]=float( tempi )/float( all_cases[all_cases.distil_x == 'non'][all_cases.rerank_source_y == rerank][all_cases.indiff == 0].shape[0])
					table_DocSeg.ix[[(sampling, mesure+'_'+eval_method, 'Psignif')], "SegnonVSDocs_nodistill_reranksource_"+rerank]=float( temps )/max( float( tempi ), 1.0 )


def general_table(outputcsv):
	for sampling in ['Accumulative', 'Fixed']:
		for eval_method in ["normal", 'residual']:
			for mesure in ["map", "p5"]:
				# print '######################################################################################################################################################\n RelSeg better than RelD? #### \n\tdistillation: non\n\tmesure: '+mesure+'\n\tevaluation: '+eval_method+'\n\tsampling: '+sampling+'####################################################################'
				source_narrow=table[table['sampling'] == sampling][['model', 'full_setup_name', 'rerank_setup', 'fb_docs', mesure+'_'+eval_method, 'list_'+mesure+'_'+eval_method]]# ,'QL_'+mesure+'_'+eval_method]]
				source_narrow['pos']=source_narrow['full_setup_name'].apply( lambda x: str( x ).split( '_' )[1] )
				source_narrow['distil']=source_narrow['full_setup_name'].apply( lambda x: '_'.join( x.split( '_' )[2:] ) if len( x.split( '_' ) ) > 2 else 'non' )
				source_narrow['signeg']=source_narrow['rerank_setup'].apply( lambda x: (x.split( '-' )[0]) if len( x.split( '-' ) ) > 1 else 'non' )
				source_narrow['signeg'][source_narrow.signeg == 'multi_neg']='max'
				source_narrow['signeg'][source_narrow.signeg == 'multi_neg_avg']='avg'
				source_narrow['signeg'][source_narrow.signeg == 'single_neg']='sn'
				source_narrow['rerank_source']=source_narrow['rerank_setup'].apply( lambda x: (x.split( '-' )[1]) if len( x.split( '-' ) ) > 1 else 'non' ).apply( lambda x: (x.split( '_' )[0]) )
				source_narrow['uniq_key']=source_narrow['model']+'_'+source_narrow['full_setup_name']+'_'+source_narrow['rerank_setup']
				source_narrow['key']=1
				source_narrow.reset_index( )
				product=pd.merge( source_narrow, source_narrow, on='key' )
				product=product[product.fb_docs_x == product.fb_docs_y]
				product=product[product.uniq_key_x != product.uniq_key_y]
				#print product
				product=pd.DataFrame(product).drop_duplicates(subset=['uniq_key_x', 'fb_docs_x','uniq_key_y', 'fb_docs_y'])
				#print product
				# print new[[first, second]]
				product['significance']=product[['list_'+mesure+'_'+eval_method+'_x', 'list_'+mesure+'_'+eval_method+'_y']].apply( lambda x: 1 if (ttest_rel( x[0], x[1] )[1] < alpha) else 0, axis=1 )
				product['improvement']=product[[mesure+'_'+eval_method+'_x', mesure+'_'+eval_method+'_y']].apply( lambda x: 1 if (x[0] > x[1]) else 0, axis=1 )
				product['indiff']=product[[mesure+'_'+eval_method+'_x', mesure+'_'+eval_method+'_y']].apply( lambda x: 1 if (x[0] == x[1]) else 0, axis=1 )
				product[['model_x','model_y','pos_x','pos_y','rerank_source_x','rerank_source_y','signeg_x','signeg_y','distil_x','distil_y','fb_docs_x' , mesure+'_'+eval_method+'_x', mesure+'_'+eval_method+'_y','significance','improvement','indiff']].to_csv(outputcsv.strip('.csv')+'_'+sampling+'_'+mesure+'_'+eval_method+'.csv')
				print "out: "+sampling+'_'+mesure+'_'+eval_method

"comparing everything with everything for a certain sampling model, evaluation method, and mesure"
def SingleNegTypes(sigbetter,sigless):
	for sampling in ['Accumulative','Fixed']:
		for eval_method in ["normal", 'residual']:
			for mesure in ["map", "p5"]:
				#print '######################################################################################################################################################\n '+sigbetter+' better than '+sigless+'? #### \n\tdistillation: non\n\tmesure: '+mesure+'\n\tevaluation: '+eval_method+'\n\tsampling: '+sampling+'####################################################################'
				#prep_nand_screen_to_sampling-eval-measure
				source_narrow=table[table['sampling'] == sampling][['model', 'full_setup_name', 'rerank_setup', 'fb_docs', mesure+'_'+eval_method, 'list_'+mesure+'_'+eval_method]]# ,'QL_'+mesure+'_'+eval_method]]
				source_narrow['pos']=source_narrow['full_setup_name'].apply( lambda x: str( x ).split( '_' )[1] )
				source_narrow['distil']=source_narrow['full_setup_name'].apply( lambda x: '_'.join( x.split( '_' )[2:] ) if len( x.split( '_' ) ) > 2 else 'non' )
				source_narrow['signeg']=source_narrow['rerank_setup'].apply( lambda x: (x.split( '-' )[0]) if len( x.split( '-' ) ) > 1 else 'non' )
				source_narrow['signeg'][source_narrow.signeg == 'multi_neg']='max'
				source_narrow['signeg'][source_narrow.signeg == 'multi_neg_avg']='avg'
				source_narrow['signeg'][source_narrow.signeg == 'single_neg']='sn'
				source_narrow['rerank_source']=source_narrow['rerank_setup'].apply( lambda x: (x.split( '-' )[1]) if len( x.split( '-' ) ) > 1 else 'non' ).apply( lambda x: (x.split( '_' )[0]) )
				source_narrow['uniq_key']=source_narrow['model']+'_'+source_narrow['full_setup_name']+'_'+source_narrow['rerank_setup']
				source_narrow['key']=1
				source_narrow.reset_index( )
				product=pd.merge( source_narrow, source_narrow, on='key' )
				product=product[product.fb_docs_x == product.fb_docs_y]
				product=product[product.uniq_key_x != product.uniq_key_y]
				#print product
				product=pd.DataFrame(product).drop_duplicates(subset=['uniq_key_x', 'fb_docs_x','uniq_key_y', 'fb_docs_y'])
				#print product
				# print new[[first, second]]
				product['significance']=product[['list_'+mesure+'_'+eval_method+'_x', 'list_'+mesure+'_'+eval_method+'_y']].apply( lambda x: 1 if (ttest_rel( x[0], x[1] )[1] < alpha) else 0, axis=1 )
				# product['improvement']=product[['list_'+mesure+'_'+eval_method+'_x', 'list_'+mesure+'_'+eval_method+'_y']].apply( lambda x: sum( [i-j for i, j in zip( x[0], x[1] )] )/len( x[0] ), axis=1 )
				product['improvement']=product[[mesure+'_'+eval_method+'_x', mesure+'_'+eval_method+'_y']].apply( lambda x: 1 if (x[0] > x[1]) else 0, axis=1 )
				product['indiff']=product[[mesure+'_'+eval_method+'_x', mesure+'_'+eval_method+'_y']].apply( lambda x: 1 if (x[0] == x[1]) else 0, axis=1 )
				#peint product[['significance','improvement','model_x','model_y','full_setup_name_x','full_setup_name_y','rerank_setup_x','rerank_setup_y','fb_docs_x','fb_docs_y']]

				# #Multyneg max better than singleNeg? - all cases (under eval method/samping/mesuare)(all equal apart for singleneg param)
				all_cases=product[product.rerank_source_x == product.rerank_source_y][product.pos_x == product.pos_y][product.model_x == product.model_y][product.distil_x == 'non'][product.distil_y == 'non'][product.signeg_x == sigbetter][product.signeg_y == sigless]
				print "all cases ",all_cases.shape[0]##all cases
				tempi=all_cases[all_cases.improvement == 1].shape[0]
				temps=all_cases[all_cases.improvement == 1][all_cases.significance == 1].shape[0]
				tempd=all_cases[all_cases.indiff == 1].shape[0]
				table_rearank.ix[[(sampling, mesure+'_'+eval_method, 'all')], sigbetter+" vs "+sigless+"-no distillation"]=all_cases.shape[0]
				table_rearank.ix[[(sampling, mesure+'_'+eval_method, 'indifferent')], sigbetter+" vs "+sigless+"-no distillation"]=all_cases[all_cases.indiff == 1].shape[0]
				table_rearank.ix[[(sampling, mesure+'_'+eval_method, 'improved')], sigbetter+" vs "+sigless+"-no distillation"]=tempi
				table_rearank.ix[[(sampling, mesure+'_'+eval_method, 'significance')], sigbetter+" vs "+sigless+"-no distillation"]=temps
				table_rearank.ix[[(sampling, mesure+'_'+eval_method, 'Pimproved')], sigbetter+" vs "+sigless+"-no distillation"]=float(tempi)/float(all_cases.shape[0]-tempd)
				table_rearank.ix[[(sampling, mesure+'_'+eval_method, 'Psignif')], sigbetter+" vs "+sigless+"-no distillation"]=float(temps)/max(float(tempi),1.0)
				for model in ['RM', 'SMM']:
					tempall=all_cases[all_cases.model_x == model].shape[0]
					tempi=all_cases[all_cases.improvement == 1][all_cases.model_x == model].shape[0]
					temps=all_cases[all_cases.improvement == 1][all_cases.model_x == model][all_cases.significance == 1].shape[0]
					tempd=all_cases[all_cases.model_x == model][all_cases.indiff == 1].shape[0]
					table_rearank.ix[[(sampling, mesure+'_'+eval_method, 'all')], sigbetter+" vs "+sigless+"-no distillation_"+model]=tempall
					table_rearank.ix[[(sampling, mesure+'_'+eval_method, 'indifferent')], sigbetter+" vs "+sigless+"-no distillation_"+model]=tempd
					table_rearank.ix[[(sampling, mesure+'_'+eval_method, 'improved')], sigbetter+" vs "+sigless+"-no distillation_"+model]=tempi
					table_rearank.ix[[(sampling, mesure+'_'+eval_method, 'significance')], sigbetter+" vs "+sigless+"-no distillation_"+model]=temps
					table_rearank.ix[[(sampling, mesure+'_'+eval_method, 'Pimproved')], sigbetter+" vs "+sigless+"-no distillation_"+model]=float( tempi )/float( tempall-tempd )
					table_rearank.ix[[(sampling, mesure+'_'+eval_method, 'Psignif')], sigbetter+" vs "+sigless+"-no distillation_"+model]=float( temps )/max( float( tempi ), 1.0 )

				#all_cases[all_cases.improvement == 1].to_csv('/lv_local/home/elinor/negative_feedback/reports/'+sigbetter+"_vs_"+sigless+'_'+mesure+'_'+eval_method+'_'+sampling+'.csv',mode='a')


"comparing everything with everything for a certain sampling model, evaluation method, and mesure. chwchink for all reranking  models (avg,sn,mn)"
def RerankUndernoDisatill(reranktype1,reranktype2):
	for sampling in ['Accumulative','Fixed']:
		for eval_method in ["normal", 'residual']:
			for mesure in ["map", "p5"]:
				#print '######################################################################################################################################################\n '+reranktype1+' better than '+reranktype2+'? #### \n\tdistillation: non\n\tmesure: '+mesure+'\n\tevaluation: '+eval_method+'\n\tsampling: '+sampling+'####################################################################'
				#prep_nand_screen_to_sampling-eval-measure
				source_narrow=table[table['sampling'] == sampling][['model', 'full_setup_name', 'rerank_setup', 'fb_docs', mesure+'_'+eval_method, 'list_'+mesure+'_'+eval_method]]# ,'QL_'+mesure+'_'+eval_method]]
				source_narrow['pos']=source_narrow['full_setup_name'].apply( lambda x: str( x ).split( '_' )[1] )
				source_narrow['distil']=source_narrow['full_setup_name'].apply( lambda x: '_'.join( x.split( '_' )[2:] ) if len( x.split( '_' ) ) > 2 else 'non' )
				source_narrow['signeg']=source_narrow['rerank_setup'].apply( lambda x: (x.split( '-' )[0]) if len( x.split( '-' ) ) > 1 else 'non' )
				source_narrow['signeg'][source_narrow.signeg == 'multi_neg']='max'
				source_narrow['signeg'][source_narrow.signeg == 'multi_neg_avg']='avg'
				source_narrow['signeg'][source_narrow.signeg == 'single_neg']='sn'
				source_narrow['rerank_source']=source_narrow['rerank_setup'].apply( lambda x: (x.split( '-' )[1]) if len( x.split( '-' ) ) > 1 else 'non' ).apply( lambda x: (x.split( '_' )[0]) )
				source_narrow['uniq_key']=source_narrow['model']+'_'+source_narrow['full_setup_name']+'_'+source_narrow['rerank_setup']
				source_narrow['key']=1
				source_narrow.reset_index( )
				product=pd.merge( source_narrow, source_narrow, on='key' )
				product=product[product.fb_docs_x == product.fb_docs_y]
				product=product[product.uniq_key_x != product.uniq_key_y]
				#print product
				#product=pd.DataFrame(product).drop_duplicates(subset=['uniq_key_x', 'fb_docs_x','uniq_key_y', 'fb_docs_y'])
				#print product
				# print new[[first, second]]
				product['significance']=product[['list_'+mesure+'_'+eval_method+'_x', 'list_'+mesure+'_'+eval_method+'_y']].apply( lambda x: 1 if (ttest_rel( x[0], x[1] )[1] < alpha) else 0, axis=1 )
				# product['improvement']=product[['list_'+mesure+'_'+eval_method+'_x', 'list_'+mesure+'_'+eval_method+'_y']].apply( lambda x: sum( [i-j for i, j in zip( x[0], x[1] )] )/len( x[0] ), axis=1 )
				product['improvement']=product[[mesure+'_'+eval_method+'_x', mesure+'_'+eval_method+'_y']].apply( lambda x: 1 if (x[0] > x[1]) else 0, axis=1 )
				product['indiff']=product[[mesure+'_'+eval_method+'_x', mesure+'_'+eval_method+'_y']].apply( lambda x: 1 if (x[0] == x[1]) else 0, axis=1 )
				#peint product[['significance','improvement','model_x','model_y','full_setup_name_x','full_setup_name_y','rerank_setup_x','rerank_setup_y','fb_docs_x','fb_docs_y']]
				# #Multyneg max better than singleNeg? - all cases (under eval method/samping/mesuare)(all equal apart for singleneg param)
				#print product
				if (reranktype2=='non') or (reranktype1=='non'):
					all_cases=product[product.rerank_source_x==reranktype1][product.rerank_source_y==reranktype2][product.pos_x == product.pos_y][product.model_x == product.model_y][product.distil_x == 'non'][product.distil_y == 'non']
				else:
					all_cases=product[product.rerank_source_x==reranktype1][product.rerank_source_y==reranktype2][product.pos_x == product.pos_y][product.model_x == product.model_y][product.distil_x == 'non'][product.distil_y == 'non'][product.signeg_x == product.signeg_y]
				#print all_cases
				#table_rearank[reranktype1+' vs '+reranktype2,sampling+'_'+eval_method+'_'+mesure]=
				#print "all cases ",all_cases.shape[0]##all cases
				tempi=all_cases[all_cases.improvement == 1].shape[0]
				temps=all_cases[all_cases.improvement == 1][all_cases.significance == 1].shape[0]
				tempd=all_cases[all_cases.indiff == 1].shape[0]
				table_rearank.ix[[(sampling, mesure+'_'+eval_method, 'all')],reranktype1+" vs "+reranktype2+"-no distillation"]=all_cases.shape[0]
				table_rearank.ix[[(sampling, mesure+'_'+eval_method, 'indifferent')],reranktype1+" vs "+reranktype2+"-no distillation"]=tempd
				table_rearank.ix[[(sampling, mesure+'_'+eval_method, 'improved')], reranktype1+" vs "+reranktype2+"-no distillation"]=tempi
				table_rearank.ix[[(sampling, mesure+'_'+eval_method, 'significance')], reranktype1+" vs "+reranktype2+"-no distillation"]=temps
				table_rearank.ix[[(sampling, mesure+'_'+eval_method, 'Pimproved')], reranktype1+" vs "+reranktype2+"-no distillation"]=float(tempi)/float(all_cases.shape[0]-tempd)
				table_rearank.ix[[(sampling, mesure+'_'+eval_method, 'Psignif')], reranktype1+" vs "+reranktype2+"-no distillation"]=float(temps)/max(float(tempi),1.0)
				for model in ['RM', 'SMM']:
					tempall=all_cases[all_cases.model_x == model].shape[0]
					tempi=all_cases[all_cases.improvement == 1][all_cases.model_x == model].shape[0]
					temps=all_cases[all_cases.improvement == 1][all_cases.model_x == model][all_cases.significance == 1].shape[0]
					tempd=all_cases[all_cases.model_x == model][all_cases.indiff == 1].shape[0]
					table_rearank.ix[[(sampling, mesure+'_'+eval_method, 'all')], reranktype1+" vs "+reranktype2+"-no distillation_"+model]=tempall
					table_rearank.ix[[(sampling, mesure+'_'+eval_method, 'indifferent')], reranktype1+" vs "+reranktype2+"-no distillation_"+model]=tempd
					table_rearank.ix[[(sampling, mesure+'_'+eval_method, 'improved')], reranktype1+" vs "+reranktype2+"-no distillation_"+model]=tempi
					table_rearank.ix[[(sampling, mesure+'_'+eval_method, 'significance')], reranktype1+" vs "+reranktype2+"-no distillation_"+model]=temps
					table_rearank.ix[[(sampling, mesure+'_'+eval_method, 'Pimproved')], reranktype1+" vs "+reranktype2+"-no distillation_"+model]=float( tempi )/float( tempall-tempd )
					table_rearank.ix[[(sampling, mesure+'_'+eval_method, 'Psignif')], reranktype1+" vs "+reranktype2+"-no distillation_"+model]=float( temps )/max( float( tempi ), 1.0 )


#['non','NRSeg','NRD','combined']
#RerankUndernoDisatill('NRD','NRSeg')


###distilation no rerank.
"comparing everything with everything for a certain sampling model, evaluation method, and mesure. chwchink for all reranking  models (avg,sn,mn)"
def DistillationNoRerank(distil_type1,distil_type2):
	for sampling in ['Accumulative','Fixed']:
		for eval_method in ["normal", 'residual']:
			for mesure in ["map", "p5"]:
				#print
				#sampling='Fixed'
				#eval_method='normal'
				#mesure='p5'
				# '######################################################################################################################################################\n '+distil_type1+' better than '+distil_type2+'? #### \n\trerank: non\n\tmesure: '+mesure+'\n\tevaluation: '+eval_method+'\n\tsampling: '+sampling+'####################################################################'
				#prep_nand_screen_to_sampling-eval-measure
				source_narrow=table[table['sampling'] == sampling][['model', 'full_setup_name', 'rerank_setup', 'fb_docs', mesure+'_'+eval_method, 'list_'+mesure+'_'+eval_method]]# ,'QL_'+mesure+'_'+eval_method]]
				source_narrow['pos']=source_narrow['full_setup_name'].apply( lambda x: str( x ).split( '_' )[1] )
				source_narrow['distil']=source_narrow['full_setup_name'].apply( lambda x: '_'.join( x.split( '_' )[2:] ) if len( x.split( '_' ) ) > 2 else 'non' )
				source_narrow['signeg']=source_narrow['rerank_setup'].apply( lambda x: (x.split( '-' )[0]) if len( x.split( '-' ) ) > 1 else 'non' )
				source_narrow['signeg'][source_narrow.signeg == 'multi_neg']='max'
				source_narrow['signeg'][source_narrow.signeg == 'multi_neg_avg']='avg'
				source_narrow['signeg'][source_narrow.signeg == 'single_neg']='sn'
				source_narrow['rerank_source']=source_narrow['rerank_setup'].apply( lambda x: (x.split( '-' )[1]) if len( x.split( '-' ) ) > 1 else 'non' ).apply( lambda x: (x.split( '_' )[0]) )
				source_narrow['uniq_key']=source_narrow['model']+'_'+source_narrow['full_setup_name']+'_'+source_narrow['rerank_setup']
				source_narrow['key']=1
				source_narrow.reset_index( )
				product=pd.merge( source_narrow, source_narrow, on='key' )
				product=product[product.fb_docs_x == product.fb_docs_y]
				product=product[product.uniq_key_x != product.uniq_key_y]
				product['significance']=product[['list_'+mesure+'_'+eval_method+'_x', 'list_'+mesure+'_'+eval_method+'_y']].apply( lambda x: 1 if (ttest_rel( x[0], x[1] )[1] < alpha) else 0, axis=1 )
				product['improvement']=product[[mesure+'_'+eval_method+'_x', mesure+'_'+eval_method+'_y']].apply( lambda x: 1 if (x[0] > x[1]) else 0, axis=1 )
				product['indiff']=product[[mesure+'_'+eval_method+'_x', mesure+'_'+eval_method+'_y']].apply( lambda x: 1 if (x[0] == x[1]) else 0, axis=1 )

				# #Specific for function
				all_cases=product[product.rerank_source_x=='non'][product.rerank_source_y=='non'][product.pos_x == product.pos_y][product.distil_x == distil_type1][product.distil_y == distil_type2][product.model_x!='RM'][product.model_y!='RM']
				#print "all cases ",all_cases.shape[0]##all cases
				tempi=all_cases[all_cases.improvement == 1].shape[0]
				temps=all_cases[all_cases.improvement == 1][all_cases.significance == 1].shape[0]
				tempd=all_cases[all_cases.indiff==1].shape[0]
				table_distill.ix[[(sampling, mesure+'_'+eval_method, 'all')], "distill_"+distil_type1+"_VS_"+distil_type2]=all_cases.shape[0]
				table_distill.ix[[(sampling, mesure+'_'+eval_method, 'indifferent')], "distill_"+distil_type1+"_VS_"+distil_type2]=tempd
				table_distill.ix[[(sampling, mesure+'_'+eval_method, 'improved')], "distill_"+distil_type1+"_VS_"+distil_type2]=tempi
				table_distill.ix[[(sampling, mesure+'_'+eval_method, 'significance')], "distill_"+distil_type1+"_VS_"+distil_type2]=temps
				table_distill.ix[[(sampling, mesure+'_'+eval_method, 'Pimproved')],  "distill_"+distil_type1+"_VS_"+distil_type2]=float(tempi)/float(all_cases.shape[0]-tempd)
				table_distill.ix[[(sampling, mesure+'_'+eval_method, 'Psignif')],  "distill_"+distil_type1+"_VS_"+distil_type2]=float(temps)/max(float(tempi),1.0)
				for pos in ['RelSeg', 'RelD']:
					tempi=all_cases[all_cases.improvement == 1][all_cases.pos_x == pos].shape[0]
					temps=all_cases[all_cases.improvement == 1][all_cases.pos_x == pos][all_cases.significance == 1].shape[0]
					tempd=all_cases[all_cases.indiff == 1][all_cases.pos_x == pos].shape[0]
					table_distill.ix[[(sampling, mesure+'_'+eval_method, 'all')], "distill_"+distil_type1+"_VS_"+distil_type2+"_givenPOS_"+pos]=all_cases[all_cases.pos_x == pos].shape[0]
					table_distill.ix[[(sampling, mesure+'_'+eval_method, 'indifferent')], "distill_"+distil_type1+"_VS_"+distil_type2+"_givenPOS_"+pos]=all_cases[all_cases.pos_x == pos][all_cases.indiff==1].shape[0]
					table_distill.ix[[(sampling, mesure+'_'+eval_method, 'improved')], "distill_"+distil_type1+"_VS_"+distil_type2+"_givenPOS_"+pos]=tempi
					table_distill.ix[[(sampling, mesure+'_'+eval_method, 'significance')], "distill_"+distil_type1+"_VS_"+distil_type2+"_givenPOS_"+pos]=temps
					table_distill.ix[[(sampling, mesure+'_'+eval_method, 'Pimproved')], "distill_"+distil_type1+"_VS_"+distil_type2+"_givenPOS_"+pos]=float( tempi )/max( float( all_cases[all_cases.pos_x == pos][all_cases.indiff==0].shape[0] ),1.0)
					table_distill.ix[[(sampling, mesure+'_'+eval_method, 'Psignif')],  "distill_"+distil_type1+"_VS_"+distil_type2+"_givenPOS_"+pos]=float( temps )/max( float( tempi ), 1.0 )

#DistillationNoRerank('NRD_NRSeg','NRD')

"comparing everything with everything for a certain sampling model, evaluation method, and mesure"
def RelsegVSRelD_in_distill():
	for sampling in ['Accumulative','Fixed']:
		for eval_method in ["normal", 'residual']:
			for mesure in ["map", "p5"]:
				#print '######################################################################################################################################################\n RelSeg better than RelD? #### \n\tdistillation: non\n\tmesure: '+mesure+'\n\tevaluation: '+eval_method+'\n\tsampling: '+sampling+'####################################################################'
				source_narrow=table[table['sampling'] == sampling][['model', 'full_setup_name', 'rerank_setup', 'fb_docs', mesure+'_'+eval_method, 'list_'+mesure+'_'+eval_method]]# ,'QL_'+mesure+'_'+eval_method]]
				source_narrow['pos']=source_narrow['full_setup_name'].apply( lambda x: str( x ).split( '_' )[1] )
				source_narrow['distil']=source_narrow['full_setup_name'].apply( lambda x: '_'.join( x.split( '_' )[2:] ) if len( x.split( '_' ) ) > 2 else 'non' )
				source_narrow['signeg']=source_narrow['rerank_setup'].apply( lambda x: (x.split( '-' )[0]) if len( x.split( '-' ) ) > 1 else 'non' )
				source_narrow['signeg'][source_narrow.signeg == 'multi_neg']='max'
				source_narrow['signeg'][source_narrow.signeg == 'multi_neg_avg']='avg'
				source_narrow['signeg'][source_narrow.signeg == 'single_neg']='sn'
				source_narrow['rerank_source']=source_narrow['rerank_setup'].apply( lambda x: (x.split( '-' )[1]) if len( x.split( '-' ) ) > 1 else 'non' ).apply( lambda x: (x.split( '_' )[0]) )
				source_narrow['uniq_key']=source_narrow['model']+'_'+source_narrow['full_setup_name']+'_'+source_narrow['rerank_setup']
				source_narrow['key']=1
				source_narrow.reset_index( )
				product=pd.merge( source_narrow, source_narrow, on='key' )
				product=product[product.fb_docs_x == product.fb_docs_y]
				product=product[product.uniq_key_x != product.uniq_key_y]
				# print new[[first, second]]
				product['significance']=product[['list_'+mesure+'_'+eval_method+'_x', 'list_'+mesure+'_'+eval_method+'_y']].apply( lambda x: 1 if (ttest_rel( x[0], x[1] )[1] < alpha) else 0, axis=1 )
				# product['improvement']=product[['list_'+mesure+'_'+eval_method+'_x', 'list_'+mesure+'_'+eval_method+'_y']].apply( lambda x: sum( [i-j for i, j in zip( x[0], x[1] )] )/len( x[0] ), axis=1 )
				product['improvement']=product[[mesure+'_'+eval_method+'_x', mesure+'_'+eval_method+'_y']].apply( lambda x: 1 if (x[0] > x[1]) else 0, axis=1 )
				product['indiff']=product[[mesure+'_'+eval_method+'_x', mesure+'_'+eval_method+'_y']].apply( lambda x: 1 if (x[0] == x[1]) else 0, axis=1 )
				#peint product[['significance','improvement','model_x','model_y','full_setup_name_x','full_setup_name_y','rerank_setup_x','rerank_setup_y','fb_docs_x','fb_docs_y']]

				# #relseg vs. reld - all cases (under eval method/samping/mesuare)
				all_cases=product[product.distil_x == product.distil_y][product.pos_x == 'RelSeg'][product.pos_y == 'RelD'][product.model_x == product.model_y][product.distil_x == product.distil_y][product.rerank_source_x=='non'][product.rerank_source_y=='non']
				#print "all cases ",all_cases.shape[0]##all cases
				tempi=all_cases[all_cases.improvement == 1].shape[0]
				temps=all_cases[all_cases.improvement == 1][all_cases.significance == 1].shape[0]
				tempd=all_cases[all_cases.indiff == 1].shape[0]
				table_DocSeg.ix[[(sampling, mesure+'_'+eval_method, 'all')], "RelsegVSRelD"]=all_cases.shape[0]
				table_DocSeg.ix[[(sampling, mesure+'_'+eval_method, 'indifferent')], "RelsegVSRelD"]=all_cases[all_cases.indiff == 1].shape[0]
				table_DocSeg.ix[[(sampling, mesure+'_'+eval_method, 'improved')], "RelsegVSRelD"]=tempi
				table_DocSeg.ix[[(sampling, mesure+'_'+eval_method, 'significance')], "RelsegVSRelD"]=temps
				table_DocSeg.ix[[(sampling, mesure+'_'+eval_method, 'Pimproved')], "RelsegVSRelD"]=float(tempi)/float(all_cases.shape[0]-tempd)
				table_DocSeg.ix[[(sampling, mesure+'_'+eval_method, 'Psignif')], "RelsegVSRelD"]=float(temps)/max(float(tempi),1.0)
				for distill in ['non','NRSeg','NRD','NRD_NRSeg']:
					tempi=all_cases[all_cases.improvement == 1][product.distil_x == distill].shape[0]
					temps=all_cases[all_cases.improvement == 1][product.distil_x == distill][all_cases.significance == 1].shape[0]
					table_DocSeg.ix[[(sampling, mesure+'_'+eval_method, 'all')], "RelsegVSRelD-distil-"+distill]=all_cases[product.distil_x == distill].shape[0]
					table_DocSeg.ix[[(sampling, mesure+'_'+eval_method, 'indifferent')], "RelsegVSRelD-distil-"+distill]=all_cases[product.distil_x == distill][all_cases.indiff == 1].shape[0]
					table_DocSeg.ix[[(sampling, mesure+'_'+eval_method, 'improved')], "RelsegVSRelD-distil-"+distill]=tempi
					table_DocSeg.ix[[(sampling, mesure+'_'+eval_method, 'significance')], "RelsegVSRelD-distil-"+distill]=temps
					table_DocSeg.ix[[(sampling, mesure+'_'+eval_method, 'Pimproved')],"RelsegVSRelD-distil-"+distill]=float( tempi )/float( all_cases[all_cases.improvement == 1][all_cases.distil_x == distill][all_cases.indiff == 0].shape[0] )
					table_DocSeg.ix[[(sampling, mesure+'_'+eval_method, 'Psignif')], "RelsegVSRelD-distil-"+distill]=float( temps )/max( float( tempi ), 1.0 )



"non is not included cause than might compare rm and smm"
def distill_interaction_rerank(reranktype1,reranktype2,distill):
	for sampling in ['Accumulative','Fixed']:
		for eval_method in ["normal", 'residual']:
			for mesure in ["map", "p5"]:
				#print '######################################################################################################################################################\n '+reranktype1+' better than '+reranktype2+'? #### \n\tdistillation: '+distill+' \n\tmesure: '+mesure+'\n\tevaluation: '+eval_method+'\n\tsampling: '+sampling+'####################################################################'
				#prep_nand_screen_to_sampling-eval-measure
				source_narrow=table[table['sampling'] == sampling][['model', 'full_setup_name', 'rerank_setup', 'fb_docs', mesure+'_'+eval_method, 'list_'+mesure+'_'+eval_method]]# ,'QL_'+mesure+'_'+eval_method]]
				source_narrow['pos']=source_narrow['full_setup_name'].apply( lambda x: str( x ).split( '_' )[1] )
				source_narrow['distil']=source_narrow['full_setup_name'].apply( lambda x: '_'.join( x.split( '_' )[2:] ) if len( x.split( '_' ) ) > 2 else 'non' )
				source_narrow['signeg']=source_narrow['rerank_setup'].apply( lambda x: (x.split( '-' )[0]) if len( x.split( '-' ) ) > 1 else 'non' )
				source_narrow['signeg'][source_narrow.signeg == 'multi_neg']='max'
				source_narrow['signeg'][source_narrow.signeg == 'multi_neg_avg']='avg'
				source_narrow['signeg'][source_narrow.signeg == 'single_neg']='sn'
				source_narrow['rerank_source']=source_narrow['rerank_setup'].apply( lambda x: (x.split( '-' )[1]) if len( x.split( '-' ) ) > 1 else 'non' ).apply( lambda x: (x.split( '_' )[0]) )
				source_narrow['rerank_source'][source_narrow.rerank_source=='combined']='NRD_NRSeg'
				source_narrow['uniq_key']=source_narrow['model']+'_'+source_narrow['full_setup_name']+'_'+source_narrow['rerank_setup']
				source_narrow['key']=1
				source_narrow.reset_index( )
				product=pd.merge( source_narrow, source_narrow, on='key' )
				product=product[product.fb_docs_x == product.fb_docs_y]
				product=product[product.uniq_key_x != product.uniq_key_y]
				#print product
				#product=pd.DataFrame(product).drop_duplicates(subset=['uniq_key_x', 'fb_docs_x','uniq_key_y', 'fb_docs_y'])
				#print product
				# print new[[first, second]]
				product['significance']=product[['list_'+mesure+'_'+eval_method+'_x', 'list_'+mesure+'_'+eval_method+'_y']].apply( lambda x: 1 if (ttest_rel( x[0], x[1] )[1] < alpha) else 0, axis=1 )
				# product['improvement']=product[['list_'+mesure+'_'+eval_method+'_x', 'list_'+mesure+'_'+eval_method+'_y']].apply( lambda x: sum( [i-j for i, j in zip( x[0], x[1] )] )/len( x[0] ), axis=1 )
				product['improvement']=product[[mesure+'_'+eval_method+'_x', mesure+'_'+eval_method+'_y']].apply( lambda x: 1 if (x[0] > x[1]) else 0, axis=1 )
				product['indiff']=product[[mesure+'_'+eval_method+'_x', mesure+'_'+eval_method+'_y']].apply( lambda x: 1 if (x[0] == x[1]) else 0, axis=1 )
				#peint product[['significance','improvement','model_x','model_y','full_setup_name_x','full_setup_name_y','rerank_setup_x','rerank_setup_y','fb_docs_x','fb_docs_y']]
				# #Multyneg max better than singleNeg? - all cases (under eval method/samping/mesuare)(all equal apart for singleneg param)
				#print product
				if reranktype2=='non':
					all_cases=product[product.rerank_source_x == reranktype1][product.rerank_source_y == reranktype2][product.pos_x == product.pos_y][product.distil_x == distill][product.distil_y == distill][product.model_x == product.model_y]
				else:
					all_cases=product[product.rerank_source_x==reranktype1][product.rerank_source_y==reranktype2][product.pos_x == product.pos_y][product.distil_x == distill][product.distil_y == distill][product.signeg_x == product.signeg_y]
				#print all_cases

				#print "all cases ",all_cases.shape[0]##all cases
				tempi=all_cases[all_cases.improvement == 1].shape[0]
				temps=all_cases[all_cases.improvement == 1][all_cases.significance == 1].shape[0]
				tempd=all_cases[all_cases.indiff == 1].shape[0]
				table_distill_rerank.ix[[(sampling, mesure+'_'+eval_method, 'all')], reranktype1+"vs"+reranktype2+"-"+distill+" distillation"]=all_cases.shape[0]
				table_distill_rerank.ix[[(sampling, mesure+'_'+eval_method, 'indifferent')], reranktype1+"vs"+reranktype2+"-"+distill+" distillation"]=all_cases[all_cases.indiff == 1].shape[0]
				table_distill_rerank.ix[[(sampling, mesure+'_'+eval_method, 'improved')], reranktype1+"vs"+reranktype2+"-"+distill+" distillation"]=tempi
				table_distill_rerank.ix[[(sampling, mesure+'_'+eval_method, 'significance')], reranktype1+"vs"+reranktype2+"-"+distill+" distillation"]=temps
				table_distill_rerank.ix[[(sampling, mesure+'_'+eval_method, 'Pimproved')], reranktype1+"vs"+reranktype2+"-"+distill+" distillation"]=float(tempi)/float(all_cases.shape[0]-tempd)
				table_distill_rerank.ix[[(sampling, mesure+'_'+eval_method, 'Psignif')], reranktype1+"vs"+reranktype2+"-"+distill+" distillation"]=float(temps)/max(float(tempi),1.0)




"non is not included cause than might compare rm and smm"
def distill_vs_rerank(reranktype1,distill,pos):
	for sampling in ['Accumulative','Fixed']:
		for eval_method in ["normal", 'residual']:
			for mesure in ["map", "p5"]:
				#print '######################################################################################################################################################\n '+reranktype1+' better than '+reranktype2+'? #### \n\tdistillation: '+distill+' \n\tmesure: '+mesure+'\n\tevaluation: '+eval_method+'\n\tsampling: '+sampling+'####################################################################'
				#prep_nand_screen_to_sampling-eval-measure
				source_narrow=table[table['sampling'] == sampling][['model', 'full_setup_name', 'rerank_setup', 'fb_docs', mesure+'_'+eval_method, 'list_'+mesure+'_'+eval_method]]# ,'QL_'+mesure+'_'+eval_method]]
				source_narrow['pos']=source_narrow['full_setup_name'].apply( lambda x: str( x ).split( '_' )[1] )
				source_narrow['distil']=source_narrow['full_setup_name'].apply( lambda x: '_'.join( x.split( '_' )[2:] ) if len( x.split( '_' ) ) > 2 else 'non' )
				source_narrow['signeg']=source_narrow['rerank_setup'].apply( lambda x: (x.split( '-' )[0]) if len( x.split( '-' ) ) > 1 else 'non' )
				source_narrow['signeg'][source_narrow.signeg == 'multi_neg']='max'
				source_narrow['signeg'][source_narrow.signeg == 'multi_neg_avg']='avg'
				source_narrow['signeg'][source_narrow.signeg == 'single_neg']='sn'
				source_narrow['rerank_source']=source_narrow['rerank_setup'].apply( lambda x: (x.split( '-' )[1]) if len( x.split( '-' ) ) > 1 else 'non' ).apply( lambda x: (x.split( '_' )[0]) )
				source_narrow['rerank_source'][source_narrow.rerank_source=='combined']='NRD_NRSeg'
				source_narrow['uniq_key']=source_narrow['model']+'_'+source_narrow['full_setup_name']+'_'+source_narrow['rerank_setup']
				source_narrow['key']=1
				source_narrow.reset_index( )
				product=pd.merge( source_narrow, source_narrow, on='key' )
				product=product[product.fb_docs_x == product.fb_docs_y]
				product=product[product.uniq_key_x != product.uniq_key_y]
				#print product
				#product=pd.DataFrame(product).drop_duplicates(subset=['uniq_key_x', 'fb_docs_x','uniq_key_y', 'fb_docs_y'])
				#print product
				# print new[[first, second]]
				product['significance']=product[['list_'+mesure+'_'+eval_method+'_x', 'list_'+mesure+'_'+eval_method+'_y']].apply( lambda x: 1 if (ttest_rel( x[0], x[1] )[1] < alpha) else 0, axis=1 )
				# product['improvement']=product[['list_'+mesure+'_'+eval_method+'_x', 'list_'+mesure+'_'+eval_method+'_y']].apply( lambda x: sum( [i-j for i, j in zip( x[0], x[1] )] )/len( x[0] ), axis=1 )
				product['improvement']=product[[mesure+'_'+eval_method+'_x', mesure+'_'+eval_method+'_y']].apply( lambda x: 1 if (x[0] > x[1]) else 0, axis=1 )
				product['indiff']=product[[mesure+'_'+eval_method+'_x', mesure+'_'+eval_method+'_y']].apply( lambda x: 1 if (x[0] == x[1]) else 0, axis=1 )
				#peint product[['significance','improvement','model_x','model_y','full_setup_name_x','full_setup_name_y','rerank_setup_x','rerank_setup_y','fb_docs_x','fb_docs_y']]
				# #Multyneg max better than singleNeg? - all cases (under eval method/samping/mesuare)(all equal apart for singleneg param)
				#print product
				if pos=='all':
					all_cases=product[product.rerank_source_x == reranktype1][product.rerank_source_y == 'non'][product.pos_x == product.pos_y][product.distil_x == 'non'][product.distil_y == distill][product.model_x == 'SMM']
				else:
					all_cases=product[product.rerank_source_x == reranktype1][product.rerank_source_y == 'non'][product.pos_x == product.pos_y][product.pos_x == pos][product.distil_x == 'non'][product.distil_y == distill][product.model_x == 'SMM']
				#print all_cases
				#print all_cases
				#print "all cases ",all_cases.shape[0]##all cases
				tempi=all_cases[all_cases.improvement == 1].shape[0]
				temps=all_cases[all_cases.improvement == 1][all_cases.significance == 1].shape[0]
				tempd=all_cases[all_cases.indiff == 1].shape[0]
				table_distill_rerank.ix[[(sampling, mesure+'_'+eval_method, 'all')], reranktype1+"_rerank_vs"+"_distillation_whenPOS_"+pos]=all_cases.shape[0]
				table_distill_rerank.ix[[(sampling, mesure+'_'+eval_method, 'indifferent')], reranktype1+"_rerank_vs"+"_distillation_whenPOS_"+pos]=all_cases[all_cases.indiff == 1].shape[0]
				table_distill_rerank.ix[[(sampling, mesure+'_'+eval_method, 'improved')], reranktype1+"_rerank_vs"+"_distillation_whenPOS_"+pos]=tempi
				table_distill_rerank.ix[[(sampling, mesure+'_'+eval_method, 'significance')],  reranktype1+"_rerank_vs"+"_distillation_whenPOS_"+pos]=temps
				table_distill_rerank.ix[[(sampling, mesure+'_'+eval_method, 'Pimproved')], reranktype1+"_rerank_vs"+"_distillation_whenPOS_"+pos]=float(tempi)/float(all_cases.shape[0]-tempd)
				table_distill_rerank.ix[[(sampling, mesure+'_'+eval_method, 'Psignif')], reranktype1+"_rerank_vs"+"_distillation_whenPOS_"+pos]=float(temps)/max(float(tempi),1.0)

				"non is not included cause than might compare rm and smm"


def SMMvsRM(pos):
	for sampling in ['Accumulative', 'Fixed']:
		for eval_method in ["normal", 'residual']:
			for mesure in ["map", "p5"]:
				# print '######################################################################################################################################################\n '+reranktype1+' better than '+reranktype2+'? #### \n\tdistillation: '+distill+' \n\tmesure: '+mesure+'\n\tevaluation: '+eval_method+'\n\tsampling: '+sampling+'####################################################################'
				#prep_nand_screen_to_sampling-eval-measure
				source_narrow=table[table['sampling'] == sampling][['model', 'full_setup_name', 'rerank_setup', 'fb_docs', mesure+'_'+eval_method, 'list_'+mesure+'_'+eval_method]]# ,'QL_'+mesure+'_'+eval_method]]
				source_narrow['pos']=source_narrow['full_setup_name'].apply( lambda x: str( x ).split( '_' )[1] )
				source_narrow['distil']=source_narrow['full_setup_name'].apply( lambda x: '_'.join( x.split( '_' )[2:] ) if len( x.split( '_' ) ) > 2 else 'non' )
				source_narrow['signeg']=source_narrow['rerank_setup'].apply( lambda x: (x.split( '-' )[0]) if len( x.split( '-' ) ) > 1 else 'non' )
				source_narrow['signeg'][source_narrow.signeg == 'multi_neg']='max'
				source_narrow['signeg'][source_narrow.signeg == 'multi_neg_avg']='avg'
				source_narrow['signeg'][source_narrow.signeg == 'single_neg']='sn'
				source_narrow['rerank_source']=source_narrow['rerank_setup'].apply( lambda x: (x.split( '-' )[1]) if len( x.split( '-' ) ) > 1 else 'non' ).apply( lambda x: (x.split( '_' )[0]) )
				source_narrow['rerank_source'][source_narrow.rerank_source == 'combined']='NRD_NRSeg'
				source_narrow['uniq_key']=source_narrow['model']+'_'+source_narrow['full_setup_name']+'_'+source_narrow['rerank_setup']
				source_narrow['key']=1
				source_narrow.reset_index( )
				product=pd.merge( source_narrow, source_narrow, on='key' )
				product=product[product.fb_docs_x == product.fb_docs_y]
				product=product[product.uniq_key_x != product.uniq_key_y]
				#print product
				#product=pd.DataFrame(product).drop_duplicates(subset=['uniq_key_x', 'fb_docs_x','uniq_key_y', 'fb_docs_y'])
				#print product
				# print new[[first, second]]
				product['significance']=product[['list_'+mesure+'_'+eval_method+'_x', 'list_'+mesure+'_'+eval_method+'_y']].apply( lambda x: 1 if (ttest_rel( x[0], x[1] )[1] < alpha) else 0, axis=1 )
				# product['improvement']=product[['list_'+mesure+'_'+eval_method+'_x', 'list_'+mesure+'_'+eval_method+'_y']].apply( lambda x: sum( [i-j for i, j in zip( x[0], x[1] )] )/len( x[0] ), axis=1 )
				product['improvement']=product[[mesure+'_'+eval_method+'_x', mesure+'_'+eval_method+'_y']].apply( lambda x: 1 if (x[0] > x[1]) else 0, axis=1 )
				product['indiff']=product[[mesure+'_'+eval_method+'_x', mesure+'_'+eval_method+'_y']].apply( lambda x: 1 if (x[0] == x[1]) else 0, axis=1 )
				#peint product[['significance','improvement','model_x','model_y','full_setup_name_x','full_setup_name_y','rerank_setup_x','rerank_setup_y','fb_docs_x','fb_docs_y']]
				# #Multyneg max better than singleNeg? - all cases (under eval method/samping/mesuare)(all equal apart for singleneg param)
				#print product
				if pos=='all':
					all_cases=product[product.rerank_source_x == product.rerank_source_y ][product.pos_x == product.pos_y][product.distil_x == 'non'][product.distil_y == 'non'][product.model_x == 'SMM'][product.model_y == 'RM']
				else:
					all_cases=product[product.rerank_source_x == product.rerank_source_y ][product.pos_x == product.pos_y][product.distil_x == 'non'][product.distil_y == 'non'][product.pos_x == pos][product.model_x == 'SMM'][product.model_y == 'RM']
				#print all_cases
				val_name='SMMvsRM_'+pos
				#print "all cases ",all_cases.shape[0]##all cases
				tempi=all_cases[all_cases.improvement == 1].shape[0]
				temps=all_cases[all_cases.improvement == 1][all_cases.significance == 1].shape[0]
				tempd=all_cases[all_cases.indiff == 1].shape[0]
				table_distill_rerank.ix[[(sampling, mesure+'_'+eval_method, 'all')],val_name]=all_cases.shape[0]
				table_distill_rerank.ix[[(sampling, mesure+'_'+eval_method, 'indifferent')], val_name]=all_cases[all_cases.indiff == 1].shape[0]
				table_distill_rerank.ix[[(sampling, mesure+'_'+eval_method, 'improved')],val_name]=tempi
				table_distill_rerank.ix[[(sampling, mesure+'_'+eval_method, 'significance')], val_name]=temps
				table_distill_rerank.ix[[(sampling, mesure+'_'+eval_method, 'Pimproved')], val_name]=float( tempi )/float( all_cases.shape[0]-tempd )
				table_distill_rerank.ix[[(sampling, mesure+'_'+eval_method, 'Psignif')], val_name]=float( temps )/max( float( tempi ), 1.0 )


#general_table('/lv_local/home/elinor/negative_feedback/reports/table_compare_all_extra.csv')
#print "a full comparison table has been written to dir"
#DistillationNoRerank('NRD','NRSeg')
RelsegVS_RelDwithrerank()
RelsegVSRelD('RelSeg','RelD')
RelsegVSRelD('RelSeg','RelDP')
RelsegVSRelD('RelDP','RelD')
RelsegVSRelD_in_distill()

table_DocSeg.T.to_csv('/lv_local/home/elinor/negative_feedback/reports/table_DocSeg.csv')

print "done table_DocSeg"


SingleNegTypes('max','sn')
SingleNegTypes('max','avg')
SingleNegTypes('sn','avg')
SingleNegTypes('sn','max')
SingleNegTypes('avg','sn')

RerankUndernoDisatill('non','NRSeg')
RerankUndernoDisatill('NRD','non')
RerankUndernoDisatill('NRSeg','non')
RerankUndernoDisatill('NRD','NRSeg')
RerankUndernoDisatill('combined','NRD')
RerankUndernoDisatill('combined','NRSeg')


print(table_rearank)
table_rearank.T.to_csv('/lv_local/home/elinor/negative_feedback/reports/table_rearank.csv')
print "done table_rearank"

DistillationNoRerank('NRD','non')
DistillationNoRerank('NRSeg','non')
DistillationNoRerank('NRD','NRSeg')
DistillationNoRerank('NRSeg','NRD')
DistillationNoRerank('NRD_NRSeg','NRD')
DistillationNoRerank('NRD_NRSeg','NRSeg')

print(table_distill)
table_distill.T.to_csv('/lv_local/home/elinor/negative_feedback/reports/table_distill.csv')
print "done table_distill"


distill_vs_rerank('NRD_NRSeg','NRD_NRSeg','RelD')
distill_vs_rerank('NRD_NRSeg','NRD_NRSeg','all')
distill_vs_rerank('NRD_NRSeg','NRD_NRSeg','RelSeg')
distill_vs_rerank('NRD','NRD','all')
distill_vs_rerank('NRD','NRD','RelD')
distill_vs_rerank('NRD','NRD','RelSeg')

distill_interaction_rerank('NRD','non','NRD')
distill_interaction_rerank('NRSeg','non','NRD')
distill_interaction_rerank('NRSeg','non','NRSeg')
distill_interaction_rerank('NRD','non','NRSeg')
distill_interaction_rerank('NRD_NRSeg','non','NRD_NRSeg')
distill_interaction_rerank('NRD','non','NRD_NRSeg')
distill_interaction_rerank('NRSeg','non','NRD_NRSeg')

SMMvsRM('all')
SMMvsRM('RelD')
SMMvsRM('RelSeg')
table_distill_rerank.T.to_csv('/lv_local/home/elinor/negative_feedback/reports/table_distill_rerank.csv')
print "done table_distill_rerank"

