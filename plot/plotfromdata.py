__author__='Elinor'
import matplotlib
import matplotlib.mathtext
matplotlib.mathtext.SHRINK_FACTOR=0.9
matplotlib.mathtext.GROW_FACTOR=1/0.9
matplotlib.use('Agg')
matplotlib.rcParams['ps.useafm'] = True
matplotlib.rcParams['pdf.use14corefonts'] = True
matplotlib.rcParams['text.usetex'] = False
import matplotlib.pyplot as plt
import pandas as pd
import numpy as np
import os
import creat_plot as cp
import sys
from matplotlib import rc
#rc('font',**{'family':'sans-serif','sans-serif':['Helvetica']})
## for Palatino and other serif fonts use:
#rc('font',**{'family':'serif','serif':['Palatino']})
#rc('text', usetex=True)

def readNclean(csv_source,datasdir,clean):
	global table
	table=pd.DataFrame.from_csv( csv_source )
	if clean:
		table=table.drop(['sampling'],axis=0)#lines of text and not data
		table=table.reset_index( )
		table=table.dropna(1)
		table=table.drop_duplicates()
		#print table
		for mes in ['map','p5']:
			for ev in ['residual', 'normal']:
				table['list_'+mes+'_'+ev]=table[mes+'_'+ev].apply( lambda x: x.rstrip( ']' ).strip( '[' ).split( ',' ) ).apply(lambda x: [float(i) for i in x] )
				table[mes+'_'+ev]=table['list_'+mes+'_'+ev].apply( np.mean )
		#table.rename( columns={'$a': 'a', '$b': 'b'} ,inplace=True)
		table['list_ql_residual_p5']=table['QL_p5_residual'].apply( lambda x: x.rstrip( ']' ).strip( '[' ).split( ',' ) ).apply( lambda x: [float( i.strip( ).strip( "'" ).strip( ) ) for i in x] )
		table['QL_p5_residual']=table['list_ql_residual_p5'].apply( np.mean )
		table['list_ql_residual_map']=table['QL_map_residual'].apply( lambda x: x.rstrip( ']' ).strip( '[' ).split( ',' ) ).apply( lambda x: [float( i.strip( ).strip( "'" ).strip( ) ) for i in x] )
		table['QL_map_residual']=table['list_ql_residual_map'].apply( np.mean )
		table['QL_map_normal']=0.3684
		table['QL_p5_normal']=0.6117
		table['fb_docs']=table['fb_docs'].apply( lambda x: int(x))
		#print table
		table.to_csv(datasdir+'datamean.csv',index=False)
		#for i in 'p5_normal,QLmap_residual,p5_residual,QLp5_residual,top_map_normal_setup,top_map_normal,top_p5_normal_setup,top_p5_normal,top_map_residual_setup,top_map_residual,top_p5_residual_setup,top_p5_residual,mapQLN,p5QLN'.split(','):
			#table=table.drop( [i], axis=1 )
	return 1;

#matplotlib.pyplot.autoscale(enable=True, axis='both', tight=None)
def simple_plot(headline,dir,outputname,dflist,ylable,legend):#df_index_fbdocs,coulms-lines to compare.
	mt=["o","d","v",'s','>','<','*',"x",'p']
	fillstyles=(u'full', u'left', u'right', u'bottom', u'top', u'none')
#colors=('#1F5C99','#CC7A00','#136B66','Green','#663399','DeepPink','LightCoral','SteelBlue','Tomato')
	colorsb=('#0000FF','#003366','SteelBlue','#0066FF','#3083FF','#08DADD','#1F7092')
	colorsr=('#FF6600','#F88017','#C47451','#FF5C00','#FF9900','#DE180B','#DD0885')
	colors_grey=('0.0','0.7','0.3','1')
	from matplotlib.gridspec import GridSpec
	matplotlib.style.use( 'grayscale' )
	#plt.suptitle(headline)
	gs1 = GridSpec(1, 1)
	gs1.update( hspace=1)
	ax1 = plt.subplot(gs1[0, :])
	f=plt.figure( 1 )
	i=0
	ic=0
	for df in dflist:
		QLflags=1
		QLflagd=1
		for line in df.index:
			minitable=df.loc[line].astype( 'float64' )

			#print i,mt[i]
			fillstyle=fillstyles[(i)%len( fillstyles )]
			"""
			if 'QL' in line and 'NRSeg' in line and QLflags:
				QLflags=0
				minitable.plot( ax=ax1, marker=mt[i%len(mt)], markersize=6, fillstyle=fillstyle )# plots the uniform part, markerfacecolor=colors[(2+i)%len(colors)]
			elif 'QL' in line and 'NRd' in line and QLflagd:
				QLflagd=0
				minitable.plot( ax=ax1, marker=mt[i%len( mt )], markersize=6, fillstyle=fillstyle )# plots the uniform part, markerfacecolor=colors[(2+i)%len(colors)]
			elif 'QL' in line:
				continue
			elif ("MN" in line or "multi_neg" in line) and 'QL' not in line:#if '4CSMM' in line:
				color=colorsb[(ic)%len( colorsb )]
				ic+=1
				minitable.plot( ax=ax1, marker=mt[i%len(mt)], markersize=6,fillstyle=fillstyle,markeredgecolor= color,markerfacecolor=color,c=color )#plots the uniform part, markerfacecolor=colors[(2+i)%len(colors)]
			elif ("SN" in line or "single_neg" in line) and ('QL' not in line) and ('QL' not in line):# if '4CSMM' in line:
				color=colorsr[(ic)%len( colorsr )]
				ic+=1
				minitable.plot( ax=ax1, marker=mt[i%len(mt)], markersize=6, fillstyle=fillstyle, markeredgecolor=color, markerfacecolor=color, c=color )#plots the uniform part, markerfacecolor=colors[(2+i)%len(colors)]
			else:
			   """
			minitable.plot( ax=ax1, marker=mt[i%len(mt)], markersize=6,fillstyle=fillstyle )#plots the uniform part, markerfacecolor=colors[(2+i)%len(colors)]
			i+=1
			if i>12: i=0
	axtitle='$k$'
	ax1.xaxis.set_major_locator((plt.MaxNLocator(integer=True)))
	ax1.set_xlabel(axtitle,fontsize=14)
	ax1.set_ylabel(ylable,fontsize=14)
	ax1.tick_params(axis='y', which='major', labelsize=11)
	ax1.tick_params(axis='x', which='major', labelsize=11)
	#ax1.set_ylim(auto=True)
	#ax1.set_ymargin( 0 )
	#ax.grid() ##plots the grid in background (suppose to)
	legend = "out"
	if legend == 'top':
		ax1.legend( bbox_to_anchor=(0., 1.02, 1., .102), prop={'size': 13}, loc=1, ncol=3, mode="expand", borderaxespad=0. )# #loc='lower left'
		f.subplots_adjust( right=0.9 )
	elif legend == 0:
		ax1.legend( loc='upper left', bbox_to_anchor=(1, 1.02), prop={'size': 12} )# #loc='lower left'
		f.subplots_adjust( right=0.8 )
	elif legend == "out":

		ax1.legend( bbox_to_anchor=(1., 0.02, 0.4, 1.0), prop={'size': 13}, numpoints=1, loc=1, ncol=1, mode="expand" )# #loc='lower left'
		# ax1.legend( bbox_to_anchor=(1., 1.0), prop={'size': 13}, loc=1, ncol=1, mode="expand" )# #loc='lower left'
		f.subplots_adjust( right=0.8 )
	else:
		# ax1.legend( loc='upper left', bbox_to_anchor=(1, 1.02), prop={'size': 12} )
		ax1.legend( loc='best', prop={'size': 12} )
		f.subplots_adjust( right=0.9 )
	ax1.margins( 0.005 )
	#ax1.axis( 'tight' )
	#plt.savefig( dir+outputname+'.eps', format='eps', bbox_inches='tight', dpi=1000 )# change to 1000 after all
	#f.clear()
	plt.savefig( dir+outputname+'.eps', format='eps', bbox_inches='tight',pad_inches=0.7, dpi=1000 )# change to 1000 after all
	#plt.savefig( dir+outputname+'.pdf', format='pdf', bbox_inches='tight',pad_inches=0.7, dpi=1000 )# change to 1000 after all
	f.clear()


def paper_plot_easy(headline,dir,outputname,dflist,ylable,legend,style,ytight=0):#df_index_fbdocs,coulms-lines to compare.
	ql_counter=0
	mt=["o","d","v",'s','>','*',"x",'p']
	fillstyles=(u'full', u'left', u'right', u'bottom', u'top', u'none')
	colors_grey=('0.0','0.7','0.3','1')
	linestyles=['-', '--', ':']
	from matplotlib.gridspec import GridSpec
	#matplotlib.style.use( 'grayscale' )
	#plt.suptitle(headline)
	gs1 = GridSpec(1, 1)
	gs1.update( hspace=1)
	ax1 = plt.subplot(gs1[0, :])
	f=plt.figure( 1 )
	i=-1
	for dfnum,df in enumerate(dflist):
		for line in df.index:
			i+=1
			minitable=df.loc[line].astype( 'float64' )
			size=8
			linesall='-'
			if 'MM' in line or 'RM' in line:
				m=mt[i%len(mt)]
				fill=u'full'# u'left'
				mcolor='0.5'
				linesall='-'
			else:
				if "(R_{p}" in line:
					continue;
				else:
					size=10
					m=mt[i%len( mt )]
					mcolor="1.0"
					fill='none'
					linesall=linestyles[i%len(linestyles)]
			#
			minitable.plot( ax=ax1, marker=m, markersize=size, c='0.0', mew=1,mec='0.0', mfc=mcolor,lw=1, linestyle=linesall)# plots the uniform part, markerfacecolor=colors[(2+i)%len(colors)] linestyle=lines,


			#else:
			#	minitable.plot( ax=ax1, marker=mt[i%len(mt)], markersize=6,fillstyle=fillstyle )#plots the uniform part, markerfacecolor=colors[(2+i)%len(colors)]
	xmin, xmax=ax1.get_xaxis().get_data_interval()
	print xmin,xmax
	axtitle='$k$'
	ax1.xaxis.set_major_locator((plt.FixedLocator(xrange(int(xmin),int(xmax+1),1) )))
	mYticks=ax1.get_yticks()#
	#print ax1.get_data_interval()
	ymin, ymax=ax1.get_yaxis().get_data_interval()
	mYticks[-1]=ymax-(ymax%0.001)#
	mYticks[0]=ymin+0.001-(ymin%0.001)#
	ax1.get_yaxis().set_view_interval(ymin-0.005,ymax+0.005)##to make the legend not cut the curve in distill
	#else:

	#print mYticks
	#print ax1.get_yaxis().get_data_interval()
	#ax1.set_ylim( )
	ax1.set_yticks(mYticks)#
	ax1.set_xlabel(axtitle,fontsize=14)
	ax1.set_ylabel(ylable,fontsize=14)
	ax1.tick_params(axis='y', which='major', labelsize=11)
	ax1.tick_params(axis='x', which='major', labelsize=11)
	#ax.grid() ##plots the grid in background (suppose to)
	ax1.grid(b=False)
	legend='ottt'
	#legend='buttomright'
	#legend='buttomright+2'
	if legend == 'top':
		ax1.legend( bbox_to_anchor=(0., 1.02, 1., .102), prop={'size': 13}, loc=1, ncol=3, mode="expand" )# #loc='lower left'
		f.subplots_adjust( right=0.9 )
	elif legend == 1:
		ax1.legend( loc='upper left', bbox_to_anchor=(1, 1.02), prop={'size': 11} )# #loc='lower left'
		f.subplots_adjust( right=0.8 )
	elif legend=='match':
		ax1.legend( bbox_to_anchor=(0., 0.0), prop={'size': 14.5} ,handlelength =1.7,loc=3, ncol=2,numpoints =1,columnspacing =0.3,handletextpad =0.2, borderaxespad =0.05,borderpad=0.2)# #loc='lower left' mode="expand", markerscale=0.7
		#f.subplots_adjust( right=0.9 )
	elif legend == 'buttomright':
		# ax1.legend( loc='upper left', bbox_to_anchor=(1, 1.02), prop={'size': 12} )
		ax1.legend( loc=4, prop={'size': 14.5},numpoints=1,handlelength =1.5,columnspacing =0.3,handletextpad =0.2, borderaxespad =0.25,borderpad=0.2)
		f.subplots_adjust( right=0.9 )
	elif legend == 'buttomright+2':
		# ax1.legend( loc='upper left', bbox_to_anchor=(1, 1.02), prop={'size': 12} )
		ax1.legend( loc=4, prop={'size': 14.5},numpoints=1, ncol=2,handlelength =1.7,columnspacing =0.3,handletextpad =0.2, borderaxespad =0.25,borderpad=0.2)
		f.subplots_adjust( right=0.9 )
	elif legend=="out":
		ax1.legend( bbox_to_anchor=(1., 0.02, 0.4, 1.0), prop={'size': 13}, numpoints=1, loc=1, ncol=1, mode="expand")#  #loc='lower left'
		#ax1.legend( bbox_to_anchor=(1., 1.0), prop={'size': 13}, loc=1, ncol=1, mode="expand" )# #loc='lower left'
		f.subplots_adjust( right=0.7 )
	else:
		# ax1.legend( loc='upper left', bbox_to_anchor=(1, 1.02), prop={'size': 12} )
		ax1.legend( loc='best', prop={'size': 14.5},numpoints=1,ncol=2,handlelength =1.7,columnspacing =0.3,handletextpad =0.2, borderaxespad =0.25,borderpad=0.2)
		f.subplots_adjust( right=0.9 )
	#ax1.margins( 0.05 )
	#ax1.margins( 0.005 ) when want everything to be right on the frame. use with tight
	#ax1.axis( 'tight' )
	plt.savefig( dir+outputname+'.eps', format='eps', bbox_inches='tight',pad_inches=0.7, dpi=1000 )# change to 1000 after all
	#plt.savefig( dir+outputname+'.pdf', format='pdf', bbox_inches='tight',pad_inches=0.7, dpi=1000 )# change to 1000 after all
	f.clear()

def paper_plot(headline,dir,outputname,dflist,ylable,legend,style,ytight=0):#df_index_fbdocs,coulms-lines to compare.
	ql_counter=0
	mt=["o","d","v",'s','>','<','*',"x",'p']
	fillstyles=(u'full', u'left', u'right', u'bottom', u'top', u'none')
	colors_grey=('0.0','0.7','0.3','1')
	from matplotlib.gridspec import GridSpec
	matplotlib.style.use( 'grayscale' )
	#plt.suptitle(headline)
	gs1 = GridSpec(1, 1)
	gs1.update( hspace=1)
	ax1 = plt.subplot(gs1[0, :])
	f=plt.figure( 1 )
	i=0
	for dfnum,df in enumerate(dflist):
		for line in df.index:
			if 'QL' in line  or "D_{init" in line:
				ql_counter+=1
				if ql_counter>1:
					continue;
			minitable=df.loc[line].astype( 'float64' )
			fillstyle=fillstyles[(i)%len( fillstyles )]
			if style=='snMn_together':
				if "RM" in line:
					lines='--'
					color='0.5'
				elif "SMM" in line:
					lines='-'
					color='0.0'
				if "SN" in line :
					m='o'
				elif "QL" in line:
					m='*'
				elif "avg" in line :
					m='v'
				elif "max" in line :
					m='d'
				else:
					m='s'
				# linestyles=['-.', '-', '--', ':']
				minitable.plot( ax=ax1, marker=m, markersize=6, linestyle=lines, c=color, mec=color, mfc=color )# plots the uniform part,fillstyle=u'bottom'
			elif style == 'rVSd':
				color='0.0'
				lines='-'
				if "Distil" in line:
					m=r'$\bowtie$'
					color='0.5'
				elif "SF" in line:
					m='s'
				if "residual" in line:
					lines='--'
				# linestyles=['-.', '-', '--', ':']
				minitable.plot( ax=ax1, marker=m, markersize=6, linestyle=lines, c=color, mec=color, mfc=color )# plots the uniform part,fillstyle=u'bottom'
			elif style=='snMn':
				color='0.0'
				if 'RM' in line:
					color='0.55'
				if "SN" in line :
					m='o'
					lines='-'
				elif "QL" in line:
					m='*'
					lines='-'
				elif "avg" in line :
					m='v'
					lines='--'
				elif "max" in line :
					m='d'
					lines='-.'
				else:
					m='s'
					lines=':'
				# linestyles=['-.', '-', '--', ':']
				minitable.plot( ax=ax1, marker=m, markersize=6, linestyle=lines, c=color, mec='0.0',lw=2,mfc=color )# plots the uniform part,fillstyle=u'bottom'
			else:
				size=7
				linesall='-'
				if 'D_{r' in line or '$R_{d' in line:
					m=''
					fill=u'full'# u'left'
					mcolor='0.5'
					lines='--'
					linesall=':'
				if 'Seg_{r' in line or '$R_{p' in line:
					color='0.5'
					m=''
					fill=u'full'#u'left'
					mcolor='1.0'
				if style=="distil":
					if "D_{nr" in line or "NRD" in line or "NR_{d" in line:
						lines='--'
						m='d'
						size=10
						if "Seg_{nr}" in line or "NRSeg" in line or "NR_{p" in line:
							lines=':'
							m='*'
					elif "Seg_{nr}" in line or "NRSeg" in line or "NR_{p" in line:
						lines='-.'
						m='s'
						size=10
					elif "QL" in line or "D_{init" in line:
						m='x'
						size=9
						lines='-'
						color='0.0'
						mcolor=color
					else:
						m='o'
						size=7
						lines='-'
					#linestyles=['_', '-', '--', ':']
					#(u'full', u'left', u'right', u'bottom', u'top', u'none'
					minitable.plot(ax=ax1, marker=m, markersize=size, c='0.0', mew=1,mec='0.0', mfc=mcolor,fillstyle=fill ,lw=2, linestyle=linesall)# plots the uniform part, markerfacecolor=colors[(2+i)%len(colors)],,
				elif style == "distilPlus":
					lines='-'
					print line
					if "RDistill" in line:
						lines='--'
					if "D_{nr" in line or "NRD" in line or "NR_{d" in line:
						size=10
						m='d'
						if "Seg_{nr}" in line or "NRSeg" in line or "NR_{p" in line:
								m='*'
					elif "Seg_{nr}" in line or "NRSeg" in line or "NR_{p" in line:
						m='s'
						size=10
					elif "QL" in line or "D_{init" in line:
						m='x'
						size=9
						color='0.0'
						mcolor=color
					else:
						m='o'
						size=7
						lines=":"
					# linestyles=['_', '-', '--', ':']
					#(u'full', u'left', u'right', u'bottom', u'top', u'none'
					minitable.plot( ax=ax1, marker=m, markersize=size, c='0.0', mew=1, mec='0.0', mfc=mcolor, fillstyle=fill, lw=2, linestyle=lines )# plots the uniform part, markerfacecolor=colors[(2+i)%len(colors)],,
				elif style=="rerank_basic":
					size=7
					mec_v='0.0'
					if "D_{nr" in line or "NRD" in line or "NR_{d" in line:
						#lines='--'
						m='d'
						size=10
						if mcolor == '0.0':
							size=10
							mec_v='1.0'
						if "Seg_{nr}" in line or "NRSeg" in line or "NR_{p" in line:
							lines=':'
							m='*'
					elif "Seg_{nr}" in line or "NRSeg" in line or "NR_{p" in line:
						#lines='-.'
						#m=r'$\bowtie$'
						m='s'
						size=10
						if mcolor=='0.0':
							size=10
							mec_v='1.0'
					elif "QL" in line or "$D_{init" in line:
						#print line
						m='x'
						lines='-'
						color='0.0'
						linesall='-'
						mcolor=color
						size=9
					else:
						m='o'
						size=7
						lines='-'
						color='0.0'


				# linestyles=['_', '-', '--', ':']
					minitable.plot( ax=ax1, marker=m, markersize=size, c='0.0', mew=1,mec='0.0', mfc=mcolor,fillstyle=fill,lw=2, linestyle=linesall)# plots the uniform part, markerfacecolor=colors[(2+i)%len(colors)] linestyle=lines,

				elif style == "RM":
					size=7
					fillcol=mec_v='0.0'
					m='o'
					lines='--'
					if "RM3C" in line:
						fillcol=mec_v='0.5'
						lines='-'
					if "Comb" in line:
						m='s'
						fill= u'full'
						fillcol='1.0'
						size=10
					if 'NRD' in line:
						fill=u'full'
						m='d'
					"""
						m='d'
						size=10
						if mcolor == '0.0':
							size=10
							mec_v='1.0'
						if "Seg_{nr}" in line or "NRSeg" in line or "NR_{p" in line:
							lines=':'
							m='*'
					elif "Seg_{nr}" in line or "NRSeg" in line or "NR_{p" in line:
						# lines='-.'
						#m=r'$\bowtie$'
						m='s'
						size=10
						if mcolor == '0.0':
							size=10
							mec_v='1.0'
					elif "QL" in line or "$D_{init" in line:
						# print line
						m='x'
						lines='-'
						color='0.0'
						linesall='-'
						mcolor=color
						size=9
					else:
						m='o'
						size=7
						lines='-'
		"""
					# linestyles=['_', '-', '--', ':']
					minitable.plot( ax=ax1, marker=m, markersize=size,  mew=1, mec='0.0', fillstyle=fill,c=mec_v, mfc=fillcol, lw=2, linestyle=lines )# plots the uniform part, markerfacecolor=colors[(2+i)%len(colors)] linestyle=lines,




			#else:
			#	minitable.plot( ax=ax1, marker=mt[i%len(mt)], markersize=6,fillstyle=fillstyle )#plots the uniform part, markerfacecolor=colors[(2+i)%len(colors)]
	xmin, xmax=ax1.get_xaxis().get_data_interval()
	print xmin,xmax
	axtitle='$k$'
	ax1.xaxis.set_major_locator((plt.FixedLocator(xrange(int(xmin),int(xmax+1),1) )))
	mYticks=ax1.get_yticks()#
	#print ax1.get_data_interval()
	ymin, ymax=ax1.get_yaxis().get_data_interval()
	mYticks[-1]=ymax-(ymax%0.001)#
	mYticks[0]=ymin+0.001-(ymin%0.001)#
	ax1.get_yaxis().set_view_interval(ymin-0.005,ymax+0.005)##to make the legend not cut the curve in distill
	#else:

	#print mYticks
	#print ax1.get_yaxis().get_data_interval()
	#ax1.set_ylim( )
	ax1.set_yticks(mYticks)#
	ax1.set_xlabel(axtitle,fontsize=14)
	ax1.set_ylabel(ylable,fontsize=14)
	ax1.tick_params(axis='y', which='major', labelsize=11)
	ax1.tick_params(axis='x', which='major', labelsize=11)
	#ax.grid() ##plots the grid in background (suppose to)
	ax1.grid(b=False)
	legend='outtt'
	#legend='buttomright'
	legend='buttomright+2'
	if legend == 'top':
		ax1.legend( bbox_to_anchor=(0., 1.02, 1., .102), prop={'size': 13}, loc=1, ncol=3, mode="expand" )# #loc='lower left'
		f.subplots_adjust( right=0.9 )
	elif legend == 1:
		ax1.legend( loc='upper left', bbox_to_anchor=(1, 1.02), prop={'size': 11} )# #loc='lower left'
		f.subplots_adjust( right=0.8 )
	elif legend=='match':
		ax1.legend( bbox_to_anchor=(0., 0.0), prop={'size': 14.5} ,handlelength =1.7,loc=3, ncol=2,numpoints =1,columnspacing =0.3,handletextpad =0.2, borderaxespad =0.05,borderpad=0.2)# #loc='lower left' mode="expand", markerscale=0.7
		#f.subplots_adjust( right=0.9 )
	elif legend == 'buttomright':
		# ax1.legend( loc='upper left', bbox_to_anchor=(1, 1.02), prop={'size': 12} )
		ax1.legend( loc=4, prop={'size': 14.5},numpoints=1,handlelength =1.5,columnspacing =0.3,handletextpad =0.2, borderaxespad =0.25,borderpad=0.2)
		f.subplots_adjust( right=0.9 )
	elif legend == 'buttomright+2':
		# ax1.legend( loc='upper left', bbox_to_anchor=(1, 1.02), prop={'size': 12} )
		ax1.legend( loc=4, prop={'size': 14.5},numpoints=1, ncol=2,handlelength =1.7,columnspacing =0.3,handletextpad =0.2, borderaxespad =0.25,borderpad=0.2)
		f.subplots_adjust( right=0.9 )
	elif legend=="out":
		ax1.legend( bbox_to_anchor=(1., 0.02, 0.4, 1.0), prop={'size': 13}, numpoints=1, loc=1, ncol=1, mode="expand")#  #loc='lower left'
		#ax1.legend( bbox_to_anchor=(1., 1.0), prop={'size': 13}, loc=1, ncol=1, mode="expand" )# #loc='lower left'
		f.subplots_adjust( right=0.7 )
	else:
		# ax1.legend( loc='upper left', bbox_to_anchor=(1, 1.02), prop={'size': 12} )
		ax1.legend( loc='best', prop={'size': 14.5},numpoints=1,ncol=2,handlelength =1.7,columnspacing =0.3,handletextpad =0.2, borderaxespad =0.25,borderpad=0.2)
		f.subplots_adjust( right=0.9 )
	#ax1.margins( 0.05 )
	#ax1.margins( 0.005 ) when want everything to be right on the frame. use with tight
	#ax1.axis( 'tight' )
	plt.savefig( dir+outputname+'.eps', format='eps', bbox_inches='tight',pad_inches=0.7, dpi=1000 )# change to 1000 after all
	#plt.savefig( dir+outputname+'.pdf', format='pdf', bbox_inches='tight',pad_inches=0.7, dpi=1000 )# change to 1000 after all
	f.clear()

def compare_Rel_docs_segs(plotsdir,SMM,RM):## for SMM, Drel or Segrel? no rerank
	this_plot_homedir=plotsdir+'/RelSeg-RelD/'
	if not os.path.exists( this_plot_homedir ):
		os.makedirs( this_plot_homedir )
	for sampling in ['Fixed', 'Accumulative']:
		this_plot_homedir_samp=this_plot_homedir+sampling+'/'
		if not os.path.exists( this_plot_homedir_samp ):
			os.makedirs( this_plot_homedir_samp )
		this_plot_homedir_model=this_plot_homedir_samp+'/SMM/'
		if not os.path.exists( this_plot_homedir_model ):
			os.makedirs( this_plot_homedir_model )
		for evalmethod in ['residual', 'normal']:
			for measure in ['map', 'p5']:
				plotsdir_pos=this_plot_homedir_model# +measure+'/'
				if not os.path.exists( plotsdir_pos ):
					os.makedirs( plotsdir_pos )
				if measure == 'map':
					ylable='MAP'
				else:
					ylable='P5'
				# SMM_only=SMM_only[['full_setup_name','fb_docs',measure+'_'+evalmethod,'QL_'+measure+'_'+evalmethod]]##not needed
				a=SMM[(SMM['rerank_setup'] == 'non')]
				mask=SMM.apply( lambda x: sampling in str( x ), axis=1 )
				a=a[mask]
				a['full_setup_name']=a['full_setup_name'].apply( lambda x: str( x ).split( '_' )[1] )
				a['full_setup_name'][(a['full_setup_name'] == 'RelD')]='$D_{r}$'
				a['full_setup_name'][(a['full_setup_name'] == 'RelSeg')]='$Seg_{r}$'
				#print a
				#measures_to_check=[measure+'_'+evalmethod, 'QL_'+measure+'_'+evalmethod]
				a2=pd.DataFrame(a[['fb_docs', 'full_setup_name', 'QL_'+measure+'_'+evalmethod]])
				#if evalmethod=='normal':#here also in residual no need in different query-likelihood
				a2['full_setup_name']='$QL$'
				a2=a2.set_index(['full_setup_name','fb_docs'],drop=False).drop_duplicates()
					#print a2
				#else:
					#a2['full_setup_name']=a2['full_setup_name'].apply( lambda x: '$QL$' )
				a1=pd.DataFrame( a[['fb_docs', 'full_setup_name', measure+'_'+evalmethod]] )
				a1['full_setup_name']=a1['full_setup_name'].apply( lambda x: '$SMM - '+str( x ).lstrip( '$' ) )
				#print a1
				#print a2
				a1=a1.pivot( columns='fb_docs', index='full_setup_name', values=measure+'_'+evalmethod )
				a2=a2.pivot( columns='fb_docs', index='full_setup_name', values='QL_'+measure+'_'+evalmethod )
				plotname='SMM-'+measure+'-'+sampling+'-'+evalmethod
				plot_headline='$D_{r}$ over $Seg_r$'
				simple_plot( plot_headline, plotsdir_pos, plotname, [a1, a2], ylable,"top" )
	for sampling in ['Fixed', 'Accumulative']:
		this_plot_homedir_samp=this_plot_homedir+sampling+'/'
		if not os.path.exists( this_plot_homedir_samp ):
			os.makedirs( this_plot_homedir_samp )
		this_plot_homedir_model=this_plot_homedir_samp+'/RM/'
		if not os.path.exists( this_plot_homedir_model ):
			os.makedirs( this_plot_homedir_model )
		for evalmethod in ['residual', 'normal']:
			for measure in ['map', 'p5']:
				plotsdir_pos=this_plot_homedir_model#+measure+'/'
				if not os.path.exists( plotsdir_pos ):
					os.makedirs( plotsdir_pos )
				#SMM_only=SMM_only[['full_setup_name','fb_docs',measure+'_'+evalmethod,'QL_'+measure+'_'+evalmethod]]##not needed
				if measure == 'map':
					ylable='MAP'
				else:
					ylable='P5'
				a=RM[(RM['rerank_setup'] == 'non')]
				mask=RM.apply( lambda x: sampling in str( x ), axis=1 )
				a=a[mask]
				a['full_setup_name']=a['full_setup_name'].apply( lambda x: str( x ).split( '_' )[1] )
				a['full_setup_name'][(a['full_setup_name'] == 'RelD')]='$D_{r}^{uniform}$'
				a['full_setup_name'][(a['full_setup_name'] == 'RelDP')]='$D_{r}^{percentage}$'
				a['full_setup_name'][(a['full_setup_name'] == 'RelSeg')]='$Seg_{r}$'
				#print a
				measures_to_check=[measure+'_'+evalmethod, 'QL_'+measure+'_'+evalmethod]
				a1=pd.DataFrame( a[['fb_docs', 'full_setup_name', measure+'_'+evalmethod]] )
				a1['full_setup_name']=a1['full_setup_name'].apply( lambda x: '$RM - '+str( x ).lstrip( '$' ) )
				a1=a1.pivot( columns='fb_docs', index='full_setup_name', values=measure+'_'+evalmethod )
				a2=pd.DataFrame(a[['fb_docs', 'full_setup_name', 'QL_'+measure+'_'+evalmethod]])
				if evalmethod=='normal':
					a2['full_setup_name']='$QL$'
					a2=a2.set_index( ['full_setup_name', 'fb_docs'], drop=False ).drop_duplicates( )
				else:
					a2['full_setup_name']=a2['full_setup_name'].apply( lambda x: '$QL - '+str( x ).lstrip( '$' ) )
				a2=a2.pivot( columns='fb_docs', index='full_setup_name', values='QL_'+measure+'_'+evalmethod )
				#print a1
				#print a2
				plotname='RM-'+measure+'-'+sampling+'-'+evalmethod
				plot_headline='$D_{r}$ over $Seg_r$'
				simple_plot( plot_headline, plotsdir_pos, plotname, [a1, a2], ylable,"top" )
	return




def creat_plot_seperate_rerank_all(plotsdir,RM,SMM,SMMP):#for SMM, RM+P,  SMMP+NRSEG, SMMP+NRD, rerank effect over RelD,RelSeg {non,NRD,NRSeg}
	this_plot_homedir=plotsdir+'/rerank-over-all/separate/'
	if not os.path.exists( this_plot_homedir ):
		os.makedirs( this_plot_homedir )
	for sampling in ['Fixed','Accumulative']:
		for model in ['SMM','RM', 'SMMP']:
			for pos in ["RelD",'RelSeg']:
				this_plot_homedir_samp=this_plot_homedir+sampling+'/'
				if not os.path.exists( this_plot_homedir_samp ):
					os.makedirs( this_plot_homedir_samp )
				this_plot_homedir_model=this_plot_homedir_samp+'/'+model+'/'
				if not os.path.exists( this_plot_homedir_model ):
					os.makedirs( this_plot_homedir_model )
				plotsdir_pos=this_plot_homedir_model+pos+'/'
				if not os.path.exists( plotsdir_pos ):
					os.makedirs( plotsdir_pos )
				for evalmethod in ['normal','residual']:
					for measure in ['map','p5']:
						if measure == 'map':
							ylable='MAP'
						else:
							ylable='P5'
						# #now the setup is set. for each model should creat the table and plot
						add='$'
						if model == 'SMMP':
							a=SMMP
							add='$'
							modelname='3CSMM_a'
							a['distil']=a['full_setup_name'].apply( lambda x: x.split( '_' )[2] if len( x.split( '_' ) ) > 2 else 'non' ) # if SMMP will hold distilation source. else will hold Non]
							a['distil'][(a['distil'] == 'NRD')]='$D_{nr}$'
							a['distil'][(a['distil'] == 'NRSeg')]='$Seg_{nr}$'
						elif model == 'RM':
							a=RM
							modelname='RM3'
							add='^{uni}$)'
						elif model == 'SMM':
							a=SMM
							modelname='SMM_a'
						mask=a.apply( lambda x:  sampling in str( x ), axis=1 )
						a=a[mask]
						a['full_setup_name']=a['full_setup_name'].apply( lambda x: str( x ).split( '_' )[1] )
						a=a[a['full_setup_name']==pos]
						a['full_setup_name'][(a['full_setup_name'] == 'RelD')]='$q_{pos}$($D_{r}'+add
						a=a[(a['full_setup_name'] != 'RelDP')]
						#a['full_setup_name'][(a['full_setup_name'] == 'RelDP')]='$D_{r}^{percentage}$'
						#print a
						a['full_setup_name'][(a['full_setup_name'] == 'RelSeg')]='$q_{pos}$($Seg_{r}$'
						if model=='SMMP':
							a['full_setup_name']=a['full_setup_name']+','+a['distil']+')'
						else:
							a['full_setup_name']=a['full_setup_name']+')'
						a['rerank_setup'][a['rerank_setup'] == 'non']=''
						a['rerank_setup'][a['rerank_setup'] == 'single_neg']='$SN$'
						a['rerank_setup'][a['rerank_setup'] == 'single_neg-combined_fuse_res']='$SN$'
						a['rerank_setup'][a['rerank_setup'] == 'single_neg-NRD_fus_res']='$SN-NRD$'
						a['rerank_setup'][a['rerank_setup'] == 'single_neg-NRSeg_fus_res']='$SN-NRSeg$'
						a['rerank_setup'][a['rerank_setup'] == 'multi_neg-combined_fuse_res']='$MN$'
						a['rerank_setup'][a['rerank_setup'] == 'multi_neg-NRD_fus_res']='$MN-NRD$'
						a['rerank_setup'][a['rerank_setup'] == 'multi_neg-NRSeg_fus_res']='$MN-NRSeg$'
						a['unique_key']=a['rerank_setup']+' '+a['full_setup_name']
						if evalmethod=='normal':
							measures_to_check=[measure+'_'+evalmethod]
						else:
							measures_to_check=[measure+'_'+evalmethod, 'QL_'+measure+'_'+evalmethod]
						list_of_df_to_plot=[]
						for m in measures_to_check:
							#print a.columns
							#print m
							ai=pd.DataFrame( a[['unique_key', 'fb_docs', m]] )
							if 'QL' in m:
								ai['unique_key']=ai['unique_key'].apply( lambda x:  '$QL$ - '+ str( x ) )
							else:
								ai['unique_key']=ai['unique_key'].apply( lambda x: '$'+modelname+'$  '+str( x ) )
							ai=ai.pivot(columns='fb_docs', index='unique_key', values=m)
							#print ai.index
							list_of_df_to_plot+=[ai]
						plotname=model+'-'+measure+'-'+sampling+'-'+evalmethod
						plot_headline=modelname+'-'+measure+'-'+sampling+'-'+evalmethod+'-rerank-over-all'
						simple_plot(plot_headline,plotsdir_pos,plotname,list_of_df_to_plot,ylable,1)


def creat_plot_seperate_rerank_all_SN_OR_MN(plotsdir,SMM,RM, SMMP,SMMPP):#for SMM, RM+P,  SMMP+NRSEG, SMMP+NRD, rerank effect over RelD,RelSeg {non,NRD,NRSeg}
	for method in ["multi_neg","multi_neg_avg","single_neg"]:
		this_plot_homedir=plotsdir+'rerank-over-all/'+method+'/'
		if not os.path.exists( this_plot_homedir ):
			os.makedirs( this_plot_homedir )
		for sampling in ['Fixed','Accumulative']:
			for model in ['SMM','RM', 'SMMP','SMMPP']:
				for pos in ["RelD",'RelSeg']:
					this_plot_homedir_samp=this_plot_homedir+sampling+'/'
					if not os.path.exists( this_plot_homedir_samp ):
						os.makedirs( this_plot_homedir_samp )
					this_plot_homedir_model=this_plot_homedir_samp+'/'+model+'/'
					if not os.path.exists( this_plot_homedir_model ):
						os.makedirs( this_plot_homedir_model )
					plotsdir_pos=this_plot_homedir_model+pos+'/'
					if not os.path.exists( plotsdir_pos ):
						os.makedirs( plotsdir_pos )
					for evalmethod in ['normal','residual']:
						for measure in ['map','p5']:
							if measure == 'map':
								ylable='MAP'
							else:
								ylable='P5'
							# #now the setup is set. for each model should creat the table and plot
							add='$'
							if model == 'SMMP':
								a=SMMP
								add='$'
								modelname='3CSMM_a'
								a['distil']=a['full_setup_name'].apply( lambda x: x.split( '_' )[2] if len( x.split( '_' ) ) > 2 else 'non' ) # if SMMP will hold distilation source. else will hold Non]
								a['distil'][(a['distil'] == 'NRD')]='$D_{nr}$'
								a['distil'][(a['distil'] == 'NRSeg')]='$Seg_{nr}$'
							elif model == 'SMMPP':
								a=SMMPP
								add='$'
								modelname='4CSMM_a'
								#a['distil']=a['full_setup_name'].apply( lambda x: x.split( '_' )[2] if len( x.split( '_' ) ) > 2 else 'non' ) # if SMMP will hold distilation source. else will hold Non]
								#a['distil'][(a['distil'] == 'NRD')]='$D_{nr}$'
								#a['distil'][(a['distil'] == 'NRSeg')]='$Seg_{nr}$'
							elif model == 'RM':
								a=RM
								modelname='RM3'
								add='^{uni}$)'
							elif model == 'SMM':
								a=SMM
								modelname='SMM_a'
							mask=a.apply( lambda x:  sampling in str( x ), axis=1 )
							a=a[mask]
							mask1=a['rerank_setup'].apply( lambda x: (method == str( x.split('-')[0] )) or (x=='non') )
							a=a[mask1]
							a['full_setup_name']=a['full_setup_name'].apply( lambda x: str( x ).split( '_' )[1] )
							a=a[a['full_setup_name']==pos]
							a['full_setup_name'][(a['full_setup_name'] == 'RelD')]='$q_{pos}$($D_{r}'+add
							a=a[(a['full_setup_name'] != 'RelDP')]
							#a['full_setup_name'][(a['full_setup_name'] == 'RelDP')]='$D_{r}^{percentage}$'
							#print a
							a['full_setup_name'][(a['full_setup_name'] == 'RelSeg')]='$q_{pos}$($Seg_{r}$'
							if model=='SMMP':
								a['full_setup_name']=a['full_setup_name']+','+a['distil']+')'
							else:
								a['full_setup_name']=a['full_setup_name']+')'
							a['rerank_setup'][a['rerank_setup'] == 'non']=''
							a['rerank_setup'][a['rerank_setup'] == 'single_neg']='$SN$'
							a['rerank_setup'][a['rerank_setup'] == 'single_neg-combined_fuse_res']='$SN$'
							a['rerank_setup'][a['rerank_setup'] == 'single_neg-NRD_fus_res']='$SN-NRD$'
							a['rerank_setup'][a['rerank_setup'] == 'single_neg-NRSeg_fus_res']='$SN-NRSeg$'
							a['rerank_setup'][a['rerank_setup'] == 'multi_neg-combined_fuse_res']='$MN_{max}$'
							a['rerank_setup'][a['rerank_setup'] == 'multi_neg_avg--combined_fuse_res']='$MN_{avg}$'
							a['rerank_setup'][a['rerank_setup'] == 'multi_neg-NRD_fus_res']='$MN_{max}-NRD$'
							a['rerank_setup'][a['rerank_setup'] == 'multi_neg-NRSeg_fus_res']='$MN_{max}-NRSeg$'
							a['rerank_setup'][a['rerank_setup'] == 'multi_neg_avg--NRD_fus_res']='$MN_{avg}-NRD$'
							a['rerank_setup'][a['rerank_setup'] == 'multi_neg_avg--NRSeg_fus_res']='$MN_{avg}-NRSeg$'
							a['unique_key']=a['rerank_setup']+' '+a['full_setup_name']
							if evalmethod=='normal':
								measures_to_check=[measure+'_'+evalmethod]
							else:
								measures_to_check=[measure+'_'+evalmethod, 'QL_'+measure+'_'+evalmethod]
							list_of_df_to_plot=[]
							for m in measures_to_check:
								#print a.columns
								#print m
								ai=pd.DataFrame( a[['unique_key', 'fb_docs', m]] )
								if 'QL' in m:
									ai['unique_key']=ai['unique_key'].apply( lambda x:  '$QL$ - '+ str( x ) )
								else:
									ai['unique_key']=ai['unique_key'].apply( lambda x: '$'+modelname+'$  '+str( x ) )
								ai=ai.pivot(columns='fb_docs', index='unique_key', values=m)
								#print ai.index
								list_of_df_to_plot+=[ai]
							plotname=model+'-'+measure+'-'+sampling+'-'+evalmethod
							plot_headline=modelname+'-'+measure+'-'+sampling+'-'+evalmethod+'-rerank-over-all'
							simple_plot(plot_headline,plotsdir_pos,plotname,list_of_df_to_plot,ylable,1)


def creat_plot_rerank_all(plotsdir,RM,SMM,SMMP,SMMPP):#for SMM, RM+P,  SMMP+NRSEG, SMMP+NRD, rerank effect over RelD,RelSeg {non,NRD,NRSeg}
	this_plot_homedir=plotsdir+'/rerank-over-all/'
	if not os.path.exists( this_plot_homedir ):
		os.makedirs( this_plot_homedir )
	for sampling in ['Fixed','Accumulative']:
		for model in ['SMM','RM', 'SMMP','SMMPP']:
			this_plot_homedir_samp=this_plot_homedir+sampling+'/'
			if not os.path.exists( this_plot_homedir_samp ):
				os.makedirs( this_plot_homedir_samp )
			this_plot_homedir_model=this_plot_homedir_samp+'/'+model+'/'
			if not os.path.exists( this_plot_homedir_model ):
				os.makedirs( this_plot_homedir_model )
			plotsdir_pos=this_plot_homedir_model+'/'
			if not os.path.exists( plotsdir_pos ):
				os.makedirs( plotsdir_pos )
			for evalmethod in ['normal','residual']:
				for measure in ['map','p5']:
					if measure == 'map':
						ylable='MAP'
					else:
						ylable='P5'
					# #now the setup is set. for each model should creat the table and plot
					add='$'
					if model == 'SMMP':
						a=SMMP
						add='$'
						modelname='3CSMM_a'
						a['distil']=a['full_setup_name'].apply( lambda x: x.split( '_' )[2] if len( x.split( '_' ) ) > 2 else 'non' ) # if SMMP will hold distilation source. else will hold Non]
						a['distil'][(a['distil'] == 'NRD')]='$D_{nr}$'
						a['distil'][(a['distil'] == 'NRSeg')]='$Seg_{nr}$'
					elif model == 'SMMPP':
						a=SMMPP
						add='$'
						modelname='4CSMM_a'
						#a['distil']=a['full_setup_name'].apply( lambda x: x.split( '_' )[2] if len( x.split( '_' ) ) > 2 else 'non' ) # if SMMP will hold distilation source. else will hold Non]
						#a['distil'][(a['distil'] == 'NRD')]='$D_{nr}$'
						#a['distil'][(a['distil'] == 'NRSeg')]='$Seg_{nr}$'
					elif model == 'RM':
						a=RM
						modelname='RM3'
						add='^{uni}$)'
					elif model == 'SMM':
						a=SMM
						modelname='SMM_a'
					mask=a.apply( lambda x:  sampling in str( x ), axis=1 )
					a=a[mask]
					if a.shape[0]==0:
						continue
					a['full_setup_name']=a['full_setup_name'].apply( lambda x: str( x ).split( '_' )[1] )
					#a=a[a['full_setup_name']==pos]
					print a['full_setup_name']
					a['full_setup_name'][(a['full_setup_name'] == 'RelD')]='$q_{pos}$($D_{r}'+add
					a=a[(a['full_setup_name'] != 'RelDP')]
					#a['full_setup_name'][(a['full_setup_name'] == 'RelDP')]='$D_{r}^{percentage}$'
					#print a
					a['full_setup_name'][(a['full_setup_name'] == 'RelSeg')]='$q_{pos}$($Seg_{r}$'
					if model=='SMMP' or model=='SMMPP':
						if model=="SMMP":
							a['full_setup_name']=a['full_setup_name']+','+a['distil']+')'
						a=a[a['rerank_setup'].apply( lambda x:  '_fus_res' not in str( x  ))]
					else:
						a['full_setup_name']=a['full_setup_name']+')'
					a['rerank_setup'][a['rerank_setup'] == 'non']=''
					a['rerank_setup'][a['rerank_setup'] == 'single_neg-combined_fuse_res']='$SN$'
					a['rerank_setup'][a['rerank_setup'] == 'single_neg-NRD_fus_res']='$SN-NRD$'
					a['rerank_setup'][a['rerank_setup'] == 'single_neg-NRSeg_fus_res']='$SN-NRSeg$'
					a['rerank_setup'][a['rerank_setup'] == 'multi_neg-combined_fuse_res']='$MN_{max}$'
					a['rerank_setup'][a['rerank_setup'] == 'multi_neg_avg-combined_fuse_res']='$MN_{avg}$'
					a['rerank_setup'][a['rerank_setup'] == 'multi_neg-NRD_fus_res']='$MN_{max}-NRD$'
					a['rerank_setup'][a['rerank_setup'] == 'multi_neg-NRSeg_fus_res']='$MN_{max}-NRSeg$'
					a['rerank_setup'][a['rerank_setup'] == 'multi_neg_avg-NRD_fus_res']='$MN_{avg}-NRD$'
					a['rerank_setup'][a['rerank_setup'] == 'multi_neg_avg-NRSeg_fus_res']='$MN_{avg}-NRSeg$'
					a['unique_key']=a['rerank_setup']+' '+a['full_setup_name']
					if evalmethod=='normal':
						measures_to_check=[measure+'_'+evalmethod]
					else:
						measures_to_check=[measure+'_'+evalmethod, 'QL_'+measure+'_'+evalmethod]
					list_of_df_to_plot=[]
					for m in measures_to_check:
						#print a.columns
						#print m
						ai=pd.DataFrame( a[['unique_key', 'fb_docs', m]] )
						if 'QL' in m:
							ai['unique_key']=ai['unique_key'].apply( lambda x:  '$QL$ - '+ str( x ) )
						else:
							ai['unique_key']=ai['unique_key'].apply( lambda x: '$'+modelname+'$  '+str( x ) )
						ai=ai.pivot(columns='fb_docs', index='unique_key', values=m)
						#print ai.index
						list_of_df_to_plot+=[ai]
					plotname=model+'-'+measure+'-'+sampling+'-'+evalmethod
					plot_headline=modelname+'-'+measure+'-'+sampling+'-'+evalmethod+'-rerank-over-all'
					simple_plot(plot_headline,plotsdir_pos,plotname,list_of_df_to_plot,ylable,1)

def creat_plot_SMM_SMMP_norerank_all(plotsdir,df_all):#for SMM,distillation effect over RelD,RelSeg

	this_plot_homedir=plotsdir+'/Distil-effect/no-rerank/'
	if not os.path.exists( this_plot_homedir ):
		os.makedirs( this_plot_homedir )
	for sampling in ['Fixed','Accumulative']:
		this_plot_homedir_samp=this_plot_homedir+sampling+'/'
		if not os.path.exists( this_plot_homedir_samp ):
			os.makedirs( this_plot_homedir_samp )
		for evalmethod in ['normal','residual']:
			for measure in ['map','p5']:
				if measure == 'map':
					ylable='MAP'
				else:
					ylable='P5'
				# #now the setup is set. for each model should creat the table and plot
				a=df_all
				mask=a.apply( lambda x:  sampling in str( x ), axis=1 )
				a=a[mask]
				a=a[(a['rerank_setup'] == 'non')]
				a['positive']='$D_{r}$'
				a['full_setup_name']=a['full_setup_name'].apply( lambda x: str( x ).split( '_' )[1] )
				a['positive'][(a['full_setup_name'] == 'RelSeg')]='$Seg_{r}$'
				a['distil'][(a['distil'] == 'NRD')]='$D_{nr}$'
				a['distil'][(a['distil'] == 'NRSeg')]='$Seg_{nr}$'
				a['distil'][(a['distil'] == 'NRD_NRSeg')]='$D_{nr},Seg_{nr}$'
				a['model'][a['model'] == 'SMMPP']='$4CSMM_a$'+' '+'$q_{pos}$('+a['positive']+','+a['distil']+')'
				a['model'][a['model'] == 'SMMP']='$3CSMM_a$'+' '+'$q_{pos}$('+a['positive']+','+a['distil']+')'
				a['model'][a['model'] == 'SMM']='$SMM_a$'+' '+'$q_{pos}$('+a['positive']+')'
				a['unique_key']=a['model']#+' '+a['positive']
				flag_baseline_plotted=1
				if evalmethod=='normal' or flag_baseline_plotted==1:
					measures_to_check=[measure+'_'+evalmethod]
				else:
					measures_to_check=[measure+'_'+evalmethod, 'QL_'+measure+'_'+evalmethod]
					flag_baseline_plotted=1
				list_of_df_to_plot=[]
				for m in measures_to_check:
					#print a[['unique_key', 'fb_docs', m]]
					ai=pd.DataFrame( a[['unique_key', 'fb_docs', m]] )
					if 'QL' in m:
						ai['unique_key']=ai['unique_key'].apply( lambda x:  '$QL - '+ str( x ).lstrip('$') )
						#print ai.shape, sampling, evalmethod, measure,m
						#ai['unique_key']='$comparable QL baseline$'+" "+ai['unique_key'].apply( lambda x:  '$QL - '+ str( x ).split( )[0] )
						#print ai.shape, sampling, evalmethod, measure
						#ai=ai.drop_duplicates()
						#print ai.shape, sampling, evalmethod, measure
						#print ai
					ai=ai.pivot(columns='fb_docs', index='unique_key', values=m)
					list_of_df_to_plot+=[ai]
				plotname='Distillation-effect-no-rerank'+'-'+measure+'-'+sampling+'-'+evalmethod
				plot_headline='3C-SMM'+'-'+measure+'-'+sampling+'-'+evalmethod+'-Distillation-effect-no-rerank'
				simple_plot(plot_headline,this_plot_homedir_samp,plotname,list_of_df_to_plot,ylable,1)

def creat_plot_SMM_SMMP_signeg_all(plotsdir,df_all):#for SMM,distillation effect over RelD,RelSeg
	this_plot_homedir=plotsdir+'/Distil-effect/rerank/'
	if not os.path.exists( this_plot_homedir ):
		os.makedirs( this_plot_homedir )
	for sampling in ['Fixed', 'Accumulative']:
		this_plot_homedir_samp=this_plot_homedir+sampling+'/'
		if not os.path.exists( this_plot_homedir_samp ):
			os.makedirs( this_plot_homedir_samp )
		for pos in ['RelD', 'RelSeg']:
			plotsdir_pos=this_plot_homedir_samp+pos+'/'
			if not os.path.exists( plotsdir_pos ):
				os.makedirs( plotsdir_pos )
			for neg in ['NRSeg', 'NRD']:
				plotsdir_distil=this_plot_homedir_samp+pos+'/'+'Distill-'+neg+'/'
				if not os.path.exists( plotsdir_distil ):
					os.makedirs( plotsdir_distil )
				for evalmethod in ['normal', 'residual']:
					for measure in ['map', 'p5']:
						if measure == 'map':
							ylable='MAP'
						else:
							ylable='P5'
						# #now the setup is set. for each model should creat the table and plot
						a=df_all
						mask=a.apply( lambda x: sampling in str( x ), axis=1 )
						a=a[mask]
						mask2=a['rerank_setup'].apply( lambda x: 'NRSeg_fus_res' not in str(x))
						a=a[mask2]
						mask3=a['rerank_setup'].apply( lambda x:  str( x ).split('-')[0]!='multi_neg_avg' )
						a=a[mask3]
						a['rerank_setup'][a['rerank_setup'] == 'non']=''
						a['rerank_setup'][a['rerank_setup'] == 'single_neg-combined_fuse_res']='$SN$'
						a['rerank_setup'][a['rerank_setup'] == 'single_neg-NRD_fus_res']='$SN-NRD$'
						a['rerank_setup'][a['rerank_setup'] == 'single_neg-NRSeg_fus_res']='$SN-NRSeg$'
						a['rerank_setup'][a['rerank_setup'] == 'multi_neg-combined_fuse_res']='$MN_{max}$'
						a['rerank_setup'][a['rerank_setup'] == 'multi_neg_avg-combined_fuse_res']='$MN_{avg}$'
						a['rerank_setup'][a['rerank_setup'] == 'multi_neg-NRD_fus_res']='$MN_{max}-NRD$'
						a['rerank_setup'][a['rerank_setup'] == 'multi_neg-NRSeg_fus_res']='$MN_{max}-NRSeg$'
						a['rerank_setup'][a['rerank_setup'] == 'multi_neg_avg-NRD_fus_res']='$MN_{avg}-NRD$'
						a['rerank_setup'][a['rerank_setup'] == 'multi_neg_avg-NRSeg_fus_res']='$MN_{avg}-NRSeg$'
						a['positive']='$D_{r}$'
						a['full_setup_name']=a['full_setup_name'].apply( lambda x: str( x ).split( '_' )[1] )
						a=a[(a['full_setup_name'] == pos )]
						a['positive'][(a['full_setup_name'] == 'RelSeg')]='$Seg_{r}$'
						a=a[(a['distil'] == neg) | (a['distil'] == 'NRD_NRSeg') | (a['distil'] == 'non')]
						a['distil'][(a['distil'] == 'NRD')]='$D_{nr}$'
						a['distil'][(a['distil'] == 'NRSeg')]='$Seg_{nr}$'
						a['distil'][(a['distil'] == 'NRD_NRSeg')]='$D_{nr},Seg_{nr}$'
						a['model'][a['model'] == 'SMMPP']='$4CSMM_a$'+' '+'$q_{pos}$('+a['positive']+','+a['distil']+')'
						a['model'][a['model'] == 'SMMP']='$3CSMM_a$'+' '+'$q_{pos}$('+a['positive']+','+a['distil']+')'
						a['model'][a['model'] == 'SMM']='$SMM_a$'+' '+'$q_{pos}$('+a['positive']+')'
						a['unique_key']=a['rerank_setup']+' '+a['model']
						if evalmethod == 'normal':
							measures_to_check=[measure+'_'+evalmethod]
						else:
							measures_to_check=[measure+'_'+evalmethod]#, 'QL_'+measure+'_'+evalmethod]
						list_of_df_to_plot=[]
						for m in measures_to_check:
							ai=pd.DataFrame( a[['unique_key', 'fb_docs', m]] )
							if 'QL' in m:
								ai['unique_key']=ai['unique_key'].apply( lambda x: '$QL - '+str( x ).lstrip( '$' ) )
							#else:
							#ai['unique_key']=ai['unique_key'].apply( lambda x: '$ '+modelname+' - '+str( x ).lstrip( '$' ) )
							ai=ai.pivot( columns='fb_docs', index='unique_key', values=m )
							#print ai.index
							list_of_df_to_plot+=[ai]
						plotname='Distillation-effect-rerank'+'-'+pos+'-'+measure+'-'+sampling+'-'+evalmethod
						plot_headline='3C-SMM'+'-'+pos+'-'+measure+'-'+sampling+'-'+evalmethod+'-Distillation-effect-no-rerank'
						simple_plot( plot_headline, plotsdir_distil, plotname, list_of_df_to_plot, ylable, 1 )



def creat_plot_SMM_SMMP_rerank_all():#for SMM,distillation effect over RelD,RelSeg. when rerank is different. no signeg.
	global plotsdir
	global SMM_all
	this_plot_homedir=plotsdir+'/Distil-effect/rerank/'
	if not os.path.exists( this_plot_homedir ):
		os.makedirs( this_plot_homedir )
	for sampling in ['Fixed','Accumulative']:
		this_plot_homedir_samp=this_plot_homedir+sampling+'/'
		if not os.path.exists( this_plot_homedir_samp ):
			os.makedirs( this_plot_homedir_samp )
		for pos in ['RelD', 'RelSeg']:
			plotsdir_pos=this_plot_homedir_samp +pos+'/'
			if not os.path.exists( plotsdir_pos ):
				os.makedirs( plotsdir_pos )
			for neg in ['NRSeg', 'NRD']:
				plotsdir_distil=this_plot_homedir_samp+pos+'/'+'Distill-'+neg+'/'
				if not os.path.exists( plotsdir_distil ):
					os.makedirs( plotsdir_distil )
				for evalmethod in ['normal','residual']:
					for measure in ['map','p5']:
						if measure == 'map':
							ylable='MAP'
						else:
							ylable='P5'
						# #now the setup is set. for each model should creat the table and plot
						a=SMM_all
						mask=a.apply( lambda x:  sampling in str( x ), axis=1 )
						a=a[mask]
						a['rerank_setup'][a['rerank_setup'] == 'non']=''
						a['rerank_setup'][a['rerank_setup'] == 'NRSeg']='$q_{neg}(Seg_{nr})$'
						a['rerank_setup'][a['rerank_setup'] == 'NRD']='$q_{neg}(D_{nr})$'
						a['positive']='$D_{r}$'
						a['full_setup_name']=a['full_setup_name'].apply( lambda x: str( x ).split( '_' )[1] )
						a=a[(a['full_setup_name'] == pos )]
						a['positive'][(a['full_setup_name'] == 'RelSeg')]='$Seg_{r}$'
						a=a[(a['distil']==neg)|(a['distil']=='non')]
						a['distil'][(a['distil']=='NRD')]='$D_{nr}$'
						a['distil'][(a['distil']=='NRSeg')]='$Seg_{nr}$'
						a['model']='SMM'
						a['model'][a['distil'] != 'non']='$3CSMM_a$'+' '+'$q_{pos}$('+a['positive']+','+a['distil']+')'
						a['model'][a['model'] == 'SMM']='$SMM_a$'+' '+'$q_{pos}$('+a['positive']+')'
						a['unique_key']=a['model']+' '+a['rerank_setup']
						if evalmethod=='normal':
							measures_to_check=[measure+'_'+evalmethod]
						else:
							measures_to_check=[measure+'_'+evalmethod]#, 'QL_'+measure+'_'+evalmethod]
						list_of_df_to_plot=[]
						for m in measures_to_check:
							ai=pd.DataFrame( a[['unique_key', 'fb_docs', m]] )
							if 'QL' in m:
								ai['unique_key']=ai['unique_key'].apply( lambda x:  '$QL - '+ str( x ).lstrip('$') )
							#else:
								#ai['unique_key']=ai['unique_key'].apply( lambda x: '$ '+modelname+' - '+str( x ).lstrip( '$' ) )
							ai=ai.pivot(columns='fb_docs', index='unique_key', values=m)
							#print ai.index
							list_of_df_to_plot+=[ai]
						plotname='Distillation-effect-rerank'+'-'+pos+'-'+measure+'-'+sampling+'-'+evalmethod
						plot_headline='3C-SMM'+'-'+pos+'-'+measure+'-'+sampling+'-'+evalmethod+'-Distillation-effect-no-rerank'
						simple_plot(plot_headline,plotsdir_distil,plotname,list_of_df_to_plot,ylable,1)

def compare_RM_SMM_best(plotsdir,all_table):#for SMM, RM+P,  SMMP+NRSEG, SMMP+NRD, rerank effect over RelD,RelSeg {non,NRD,NRSeg}
	this_plot_homedir=plotsdir+'/signegRMSMM/'
	if not os.path.exists( this_plot_homedir ):
		os.makedirs( this_plot_homedir )
	for sampling in ['Fixed','Accumulative']:
		this_plot_homedir_samp=this_plot_homedir+sampling+'/'
		if not os.path.exists( this_plot_homedir_samp ):
			os.makedirs( this_plot_homedir_samp )
		this_plot_homedir_model=this_plot_homedir_samp+'/'
		if not os.path.exists( this_plot_homedir_model ):
			os.makedirs( this_plot_homedir_model )
		for evalmethod in ['normal','residual']:
			for measure in ['map','p5']:
				if measure == 'map':
					ylable='MAP'
				else:
					ylable='P5'
				plotsdir_pos=this_plot_homedir_model#+measure+'/'
				if not os.path.exists( plotsdir_pos ):
					os.makedirs( plotsdir_pos )
				# #now the setup is set. for each model should creat the table and plot
				add='$'
				a=all_table[['full_setup_name','model','rerank_setup','fb_docs','map_normal','p5_normal','map_residual','p5_residual','QL_map_residual','QL_p5_residual','QL_map_normal','QL_p5_normal']]
				a= a[(a['model']=='SMM')|(a['model']=='RM')]
				a['model'][a['model'] == 'RM']='$RM3^{uni}$'
				a['model'][a['model'] == 'SMM']='$SMM_a$'
				mask=a.apply( lambda x:  sampling in str( x ), axis=1 )
				a=a[mask]
				a['full_setup_name']=a['full_setup_name'].apply( lambda x: str( x ).split( '_' )[1] )
				#a['full_setup_name'][(a['full_setup_name'] == 'RelD')]='$D_{r}'+add
				a=a[(a['full_setup_name'] == 'RelSeg')]
				#a['full_setup_name'][(a['full_setup_name'] == 'RelDP')]='$D_{r}^{percentage}$'
				#print a
				a['full_setup_name'][(a['full_setup_name'] == 'RelSeg')]='$Seg_{r}$'
				#print a
				#a=a[(a['rerank_setup'] == 'single_neg')]
				a['rerank_setup'][a['rerank_setup'] == 'non']=''
				a['rerank_setup'][a['rerank_setup'] == 'single_neg']='$SN$'
				a['rerank_setup'][a['rerank_setup'] == 'single_neg-combined_fuse_res']='$SN$'
				a['rerank_setup'][a['rerank_setup'] == 'single_neg-NRD_fus_res']='$SN-NRD$'
				a['rerank_setup'][a['rerank_setup'] == 'single_neg-NRSeg_fus_res']='$SN-NRSeg$'
				a['rerank_setup'][a['rerank_setup'] == 'multi_neg-combined_fuse_res']='$MN_{max}$'
				a['rerank_setup'][a['rerank_setup'] == 'multi_neg_avg--combined_fuse_res']='$MN_{avg}$'
				a['rerank_setup'][a['rerank_setup'] == 'multi_neg-NRD_fus_res']='$MN_{max}-NRD$'
				a['rerank_setup'][a['rerank_setup'] == 'multi_neg-NRSeg_fus_res']='$MN_{max}-NRSeg$'
				a['rerank_setup'][a['rerank_setup'] == 'multi_neg_avg--NRD_fus_res']='$MN_{avg}-NRD$'
				a['rerank_setup'][a['rerank_setup'] == 'multi_neg_avg--NRSeg_fus_res']='$MN_{avg}-NRSeg$'
				#print a
				a['unique_key']=a['model']+' '+a['rerank_setup']+" "+a['full_setup_name']
				if evalmethod=='normal':
					measures_to_check=[measure+'_'+evalmethod]
				else:
					measures_to_check=[measure+'_'+evalmethod, 'QL_'+measure+'_'+evalmethod]
				list_of_df_to_plot=[]
				for m in measures_to_check:
					#print a.columns
					#print m
					ai=pd.DataFrame( a[['unique_key', 'fb_docs', m]] )
					if 'QL' in m:
						ai['unique_key']=ai['unique_key'].apply( lambda x:  '$QL - '+ str( x ).lstrip('$') )
					#else:
						#ai['unique_key']=ai['unique_key'].apply( lambda x: '$ '+modelname+' - '+str( x ).lstrip( '$' ) )
					ai=ai.pivot(columns='fb_docs', index='unique_key', values=m)
					#print ai.index
					list_of_df_to_plot+=[ai]
				plotname='Rseg'+'-'+measure+'-'+sampling+'-'+evalmethod
				plot_headline=measure+'-'+sampling+'-'+evalmethod+'-rerank-over-allSMM-RM-Compare'
				simple_plot(plot_headline,plotsdir_pos,plotname,list_of_df_to_plot,ylable,1)


#########################################################################################
#########################################################################################
def read_and_proccess(plotsdirout,datasdirout,csv_source,clean):
	global plotsdir
	plotsdir=plotsdirout
	global table
	if not os.path.exists( '/lv_local/home/elinor/negative_feedback/reports/' ):
		os.makedirs( '/lv_local/home/elinor/negative_feedback/reports/' )
	if not os.path.exists( plotsdir ):
		os.makedirs(plotsdir)
	if not os.path.exists( datasdirout ):
		os.makedirs( datasdirout )

	readNclean( csv_source,datasdirout ,clean)
	all_table=table

	#print table
	SMM=table[table['model'] == 'SMM']
	SMM=SMM[['full_setup_name', 'rerank_setup', 'fb_docs', 'map_normal', 'p5_normal', 'map_residual', 'p5_residual', 'QL_map_residual', 'QL_p5_residual', 'QL_map_normal', 'QL_p5_normal']]
	SMMP=table[table['model'] == 'SMMP']
	SMMP=SMMP[['full_setup_name', 'rerank_setup', 'fb_docs', 'map_normal', 'p5_normal', 'map_residual', 'p5_residual', 'QL_map_residual', 'QL_p5_residual', 'QL_map_normal', 'QL_p5_normal']]
	SMMPP=table[table['model'] == 'SMMPP']
	SMMPP=SMMPP[['full_setup_name', 'rerank_setup', 'fb_docs', 'map_normal', 'p5_normal', 'map_residual', 'p5_residual', 'QL_map_residual', 'QL_p5_residual', 'QL_map_normal', 'QL_p5_normal']]
	RM=table[table['model'] == 'RM']
	RM=RM[['full_setup_name', 'rerank_setup', 'fb_docs', 'map_normal', 'p5_normal', 'map_residual', 'p5_residual', 'QL_map_residual', 'QL_p5_residual', 'QL_map_normal', 'QL_p5_normal']]

	SMM_SMMP=table[(table['model'] == 'SMM') | (table['model'] == 'SMMP')]
	SMM_SMMP=SMM_SMMP[['full_setup_name', 'model', 'rerank_setup', 'fb_docs', 'map_normal', 'p5_normal', 'map_residual', 'p5_residual', 'QL_map_residual', 'QL_p5_residual', 'QL_map_normal', 'QL_p5_normal']]
	SMM_SMMP['distil']=SMM_SMMP['full_setup_name'].apply( lambda x: x.split( '_' )[2] if len( x.split( '_' ) ) > 2 else 'non' ) # if SMMP will hold distilation source. else will hold Non]

	SMM_all=table[(table['model'] == 'SMM') | (table['model'] == 'SMMP') | (table['model'] == 'SMMPP')]
	SMM_all=SMM_all[['full_setup_name', 'model', 'rerank_setup', 'fb_docs', 'map_normal', 'p5_normal', 'map_residual', 'p5_residual', 'QL_map_residual', 'QL_p5_residual', 'QL_map_normal', 'QL_p5_normal']]
	#SMM_all['distil']=SMM_all['full_setup_name'].apply( lambda x: x.split( '_' )[2] if len( x.split( '_' ) ) ==3 else 'non' ) # if SMMP will hold distilation source. else will hold Non]
	SMM_all['distil']=SMM_all['full_setup_name'].apply( lambda x: '_'.join( x.split( '_' )[2:] ) if len( x.split( '_' ) ) > 2 else 'non' ) # if SMMP will hold distilation source. else will hold Non]
	#print SMM_all
	return SMM,SMM_all,SMMP,SMMPP,RM,all_table

	"""
	for measure in ['p5','map']:
		for type in ['residual','normal']:
			main_title=measure+' '+type+' over different $K$'
			cp.graph_fb_measure( type, measure, main_title, '/lv_local/home/elinor/negative_feedback/reports/data_comparable_plots/RM.csv', flagRM=1 )
			cp.graph_fb_measure( type, measure, main_title, '/lv_local/home/elinor/negative_feedback/reports/data_comparable_plots/SMM.csv', 0 )

	for measure in ['p5','map']:
		for type in ['residual','normal']:
			cp.graph_fb_measure_SMM_P_compare(type,measure,'/lv_local/home/elinor/negative_feedback/reports/data_comparable_plots/SMM.csv','/lv_local/home/elinor/negative_feedback/reports/data_comparable_plots/SMMP.csv')
	"""
def main():
	SMM, SMM_all, SMMP, SMMPP, RM, all_table=read_and_proccess( '/lv_local/home/elinor/negative_feedback/reports/comparable_plots_test/', '/lv_local/home/elinor/negative_feedback/reports/data_comparable_plots_test/',"/lv_local/home/elinor/negative_feedback/reports/res-26-10-15list.csv",1 )
	##creat_plot_SMM_SMMP_signeg_all(SMM_SMMP)
	creat_plot_SMM_SMMP_signeg_all(plotsdir,SMM_all)
	#compares SMM and SMMP with signeg. - each plot have only one positive info and one kind of rerank
	#-does not compare different methods, but views the effect and interaction of rerank and distill
	#########################################################################################
	#########################################################################################
	creat_plot_seperate_rerank_all_SN_OR_MN(plotsdir,SMM,RM, SMMP,SMMPP)
	##creat_plot_SMM_SMMP_norerank_all(SMM_SMMP)
	creat_plot_SMM_SMMP_norerank_all(plotsdir,SMM_all)
	#compares all positive queries of SMM and SMMP (relD vs. RelSeg vs. all kind of distilaation) vs. QL. (if change the flag)
	#########################################################################################
	#########################################################################################



	#########################################################################################
	#########################################################################################

	compare_Rel_docs_segs(plotsdir,SMM,RM)

	#This function compare for each basic model (RM,SMM) performances using different q_pos info. comparing to query likelihood
	#########################################################################################
	#########################################################################################


	compare_RM_SMM_best(plotsdir,all_table)

	#for each rm SMM, compares relseg and reld with and without single neg
	#########################################################################################
	#########################################################################################


	creat_plot_rerank_all( plotsdir,RM,SMM,SMMP,SMMPP )

	#for model compares non and singleneg for RelD with  non and singleneg for RelSeg. ql in residual only. model:(RM,SMM,SMMP?)
	#!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!not working well now, present for qpos=relseg only
	#########################################################################################
	#########################################################################################


	#creat_plot_seperate_rerank_all(plotsdir,RM,SMM,SMMP) -- not using now




if __name__ == '__main__':
	main()