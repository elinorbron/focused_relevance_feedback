__author__='Elinor'

import os
import pandas as pd
import sys
scriptpath="/lv_local/home/elinor/pycharm/nf/"
sys.path.append(os.path.dirname(scriptpath))
from plot import plotfromdata as plt


plotsdirout='/lv_local/home/elinor/negative_feedback/reports/comparable_plots_residual_all/'
datasdirout='/lv_local/home/elinor/negative_feedback/reports/data_comparable_plots_residual_all/'
#csv_source='/lv_local/home/elinor/negative_feedback/reports/191115extra_list_all_clean.csv'#this is from before caching the reranking cvloo to simple aggregation
csv_source='/lv_local/home/elinor/negative_feedback/reports/221215DataForShortPaper/data_residualall_normal2212.csv'

SMM, SMM_all, SMMP, SMMPP, RM, all_table=plt.read_and_proccess( plotsdirout,datasdirout,csv_source,0 )

def creat_plot_seperate_rerank_effect_basic(plotsdir,SMM,RM, SMMP,SMMPP,no_Reldp_in_graph,dropnrseg,qlpercent=0,residual_with_ql=0):#for SMM, RM+P,  SMMP+NRSEG, SMMP+NRD, rerank effect over RelD,RelSeg {non,NRD,NRSeg}
	if not no_Reldp_in_graph:
		this_plot_homedir=plotsdir+'/rerank_basic_withP/'
	else:
		this_plot_homedir=plotsdir+'/rerank_basic/'
	if not os.path.exists( this_plot_homedir ):
			os.makedirs( this_plot_homedir )
	for method in ["multi_neg"]:
		for sampling in ['Fixed','Accumulative']:
			for model in ['SMM']:#],'RM']:
				for evalmethod in ['normal','residual']:
					for measure in ['map','p5']:
						if measure == 'map':
							ylable='MAP'
						else:
							ylable='P5'
						# #now the setup is set. for each model should creat the table and plot
						add='$'
						if model == 'SMMP':
							a=SMMP
							add='$'
							modelname='3CSMM_a'
							a['distil']=a['full_setup_name'].apply( lambda x: x.split( '_' )[2] if len( x.split( '_' ) ) > 2 else 'non' ) # if SMMP will hold distilation source. else will hold Non]
							a['distil'][(a['distil'] == 'NRD')]='$D_{nr}$'
							a['distil'][(a['distil'] == 'NRSeg')]='$Seg_{nr}$'
						elif model == 'SMMPP':
							a=SMMPP
							add='$'
							modelname='4CSMM_a'
							#a['distil']=a['full_setup_name'].apply( lambda x: x.split( '_' )[2] if len( x.split( '_' ) ) > 2 else 'non' ) # if SMMP will hold distilation source. else will hold Non]
							#a['distil'][(a['distil'] == 'NRD')]='$D_{nr}$'
							#a['distil'][(a['distil'] == 'NRSeg')]='$Seg_{nr}$'
						elif model == 'RM':
							a=RM
							modelname='RM3'
							add='^{uni}$'
						elif model == 'SMM':
							a=SMM
							modelname='SF'
						mask=a.apply( lambda x:  sampling in str( x ), axis=1 )
						a=a[mask]
						mask1=a['rerank_setup'].apply( lambda x: (method == str( x.split('-')[0] )) or (x=='non') )
						a=a[mask1]
						a['full_setup_name']=a['full_setup_name'].apply( lambda x: str( x ).split( '_' )[1] )
						#a=a[a['full_setup_name']==pos]
						a['full_setup_name'][(a['full_setup_name'] == 'RelD')]='($R_{d}'+add
						if no_Reldp_in_graph:
							a=a[(a['full_setup_name'] != 'RelDP')]
						else:
							a['full_setup_name'][(a['full_setup_name'] == 'RelDP')]='($D_{r}^{percentage}$'
						#print a
						a['full_setup_name'][(a['full_setup_name'] == 'RelSeg')]='($R_{p}$'
						if model=='SMMP':
							a['full_setup_name']=a['full_setup_name']+','+a['distil']+')'
						else:
							a['full_setup_name']=a['full_setup_name']+''
						a=a[a['rerank_setup'] != 'single_neg-combined_fuse_res']
						a=a[a['rerank_setup'] != 'multi_neg-combined_fuse_res']
						a=a[a['rerank_setup'] != 'multi_neg_avg-combined_fuse_res']
						if dropnrseg:
							a=a[a['rerank_setup'] != 'multi_neg-NRSeg_fus_res']
							a=a[a['rerank_setup'] != 'multi_neg_avg-NRSeg_fus_res']
							a=a[a['rerank_setup'] != 'single_neg-NRSeg_fus_res']
						#a['rerank_setup'][a['rerank_setup'] == 'non']=''
						a['rerank_setup'][a['rerank_setup'] == 'single_neg']='$SN$'
						a['rerank_setup'][a['rerank_setup'] == 'single_neg-NRD_fus_res']='$SN-NRD$'
						a['rerank_setup'][a['rerank_setup'] == 'single_neg-NRSeg_fus_res']='$SN-NRSeg$'
						a['rerank_setup'][a['rerank_setup'] == 'multi_neg-combined_fuse_res']='$$'
						a['rerank_setup'][a['rerank_setup'] == 'multi_neg-NRD_fus_res']='$NR_{d}$'
						a['rerank_setup'][a['rerank_setup'] == 'multi_neg-NRSeg_fus_res']='$NR_{p}$'
						a['rerank_setup'][a['rerank_setup'] == 'multi_neg_avg-NRD_fus_res']='$MN_{avg}-NRD$'
						a['rerank_setup'][a['rerank_setup'] == 'multi_neg_avg-NRSeg_fus_res']='$MN_{avg}-NRSeg$'
						#a['unique_key']='MM'+a['full_setup_name']+')'
						a['unique_key']='SF'+a['full_setup_name']+','+a['rerank_setup']+')'
						#a['unique_key'][a['rerank_setup'] != '']='SF'+a['full_setup_name']+','+a['rerank_setup']+')'
						a['unique_key'][a['rerank_setup'] == 'non']='MM'+a['full_setup_name'][a['rerank_setup'] == 'non']+')'
						#a['unique_key']='SF'+a['full_setup_name']+','+a['rerank_setup']+')'

						if residual_with_ql:#evalmethod == 'residual' and
							measures_to_check=[measure+'_'+evalmethod, 'QL_'+measure+'_'+evalmethod]
						elif evalmethod == 'residual' and qlpercent:
							a['QL_improvement_percentage'+measure+'_'+evalmethod]=(a[measure+'_'+evalmethod]-a['QL_'+measure+'_'+evalmethod])/a[measure+'_'+evalmethod]
							# print a['QL_improvement_percentage'+measure+'_'+evalmethod]
							measures_to_check=['improvement_percentage'+measure+'_'+evalmethod]
							if measure == 'map':
								ylable="% MAP improvement wrt QL"
							else:
								ylable="% P5 improvement wrt QL"
						else:
							measures_to_check=[measure+'_'+evalmethod]
						list_of_df_to_plot=[]
						for m in measures_to_check:
							# print a.columns
							# print m
							ai=pd.DataFrame( a[['unique_key', 'fb_docs', m]] )
							if 'QL' in m:
								ai['unique_key']=ai['unique_key'].apply( lambda x: '$D_{init}$' )
								ai=ai.drop_duplicates( )
							ai=ai.pivot( columns='fb_docs', index='unique_key', values=m )
							if 'QL' not in m:
								ai=ai.reindex([ u'SF($R_{d}$,$NR_{d}$)', u'SF($R_{d}$,$NR_{p}$)', u'SF($R_{p}$,$NR_{d}$)', u'SF($R_{p}$,$NR_{p}$)',u'MM($R_{d}$)', u'MM($R_{p}$)'])
							list_of_df_to_plot+=[ai]
						plotname=model+'-'+measure+'-'+sampling+'-'+evalmethod+'_'+['with_nrseg','drop_nrseg'][dropnrseg]+["",'-w-QL'][residual_with_ql]
						plot_headline=modelname+'-'+measure+'-'+sampling+'-'+evalmethod+'-rerank-over-all'
						plt.paper_plot(plot_headline,this_plot_homedir,plotname,list_of_df_to_plot,ylable,0,"rerank_basic")

def creat_plot_SMM_SMMP_norerank_all(plotsdir,df_all,residual_with_ql=0,with_segmentsSMMPP=0,with_SMMPP=0):#for SMM,distillation effect over RelD,RelSeg
	this_plot_homedir=plotsdir+'/Distil-effect-all/'
	if not os.path.exists( this_plot_homedir ):
		os.makedirs( this_plot_homedir )
	for sampling in ['Fixed','Accumulative']:
		this_plot_homedir_samp=this_plot_homedir+'/'
		if not os.path.exists( this_plot_homedir_samp ):
			os.makedirs( this_plot_homedir_samp )
		for evalmethod in ['normal','residual']:
			for measure in ['map','p5']:
				if measure == 'map':
					ylable='MAP'
				else:
					ylable='P5'
				# #now the setup is set. for each model should creat the table and plot
				a=df_all
				mask=a.apply( lambda x:  sampling in str( x ), axis=1 )
				a=a[mask]
				a=a[(a['rerank_setup'] == 'non')]
				a['positive']='$R_{d}$'
				a['full_setup_name']=a['full_setup_name'].apply( lambda x: str( x ).split( '_' )[1] )
				a['positive'][(a['full_setup_name'] == 'RelSeg')]='$R_{p}$'
				a['distil'][(a['distil'] == 'NRD')]='$NR_{d}$'
				a['distil'][(a['distil'] == 'NRSeg')]='$NR_{p}$'
				a['distil'][(a['distil'] == 'NRD_NRSeg')]='$NR_{p},NR_{d}$'
				if not with_segmentsSMMPP:
					mask=a.apply( lambda x: not( 'SMMPP' in str( x ) and 'RelSeg' in str(x) ), axis=1 )
					a=a[mask]
				if not with_SMMPP:
					mask=a.apply( lambda x: not ( 'SMMPP' in str( x )  ), axis=1 )
					a=a[mask]
				a['model'][a['model'] == 'SMMPP']='$4CSMM_a$'+' '+'$q_{pos}$('+a['positive']+','+a['distil']+')'
				a['model'][a['model'] == 'SMMPP']='$4CSMM_a$'+' '+'$q_{pos}$('+a['positive']+','+a['distil']+')'
				a['model'][a['model'] == 'SMMP']='Distill'+'('+a['positive']+','+a['distil']+')'
				a['model'][a['model'] == 'SMM']='MM'+''+'('+a['positive']+')'
				a['unique_key']=a['model']#+' '+a['positive']
				if residual_with_ql:#evalmethod == 'residual' and
					measures_to_check=[measure+'_'+evalmethod, 'QL_'+measure+'_'+evalmethod]
				else:
					measures_to_check=[measure+'_'+evalmethod]
				list_of_df_to_plot=[]
				for m in measures_to_check:
					# print a.columns
					#print m
					ai=pd.DataFrame( a[['unique_key', 'fb_docs', m]] )
					if 'QL' in m:
						ai['unique_key']=ai['unique_key'].apply( lambda x: '$D_{init}$' )
						ai=ai.drop_duplicates( )
						#print ai
						ai=ai.pivot( columns='fb_docs', index='unique_key', values=m )
						#ai=ai.reindex(['Distill($R_{doc}$,$N_{doc}$)','Distill($R_{doc}$,$N_{psg}$)','Distill($R_{psg}$,$N_{doc}$)','Distill($R_{psg}$,$N_{psg}$)',"MM($R_{doc}$)","MM($R_{psg}$)",'$D_{init}$'])I once throw this away because it didn't work with (and worked without)
						#ai=ai.reindex(["MM($R_{doc}$)","MM($R_{por}$)",'$\\theta^q_{rel}$($R_{doc}$,$N_{doc}$)','$\\theta^q_{rel}$($R_{doc}$,$N_{por}$)','$\\theta^q_{rel}$($R_{por}$,$N_{doc}$)','$\\theta^q_{rel}$($R_{por}$,$N_{por}$)']) I once throw this away because it didn't work with (and worked without)
					else:
						ai=ai.pivot( columns='fb_docs', index='unique_key', values=m )
						ai=ai.reindex(['Distill($R_{d}$,$NR_{d}$)','Distill($R_{d}$,$NR_{p}$)','Distill($R_{p}$,$NR_{d}$)','Distill($R_{p}$,$NR_{p}$)',"MM($R_{d}$)","MM($R_{p}$)"])
					#print ai.index
					#print ai
					list_of_df_to_plot+=[ai]
				plotname='Distillation-effect-no-rerank'+'-'+measure+'-'+sampling+'-'+evalmethod+["",'-w-QL'][residual_with_ql]
				plot_headline='3C-SMM'+'-'+measure+'-'+sampling+'-'+evalmethod+'-Distillation-effect-no-rerank'
				plt.paper_plot(plot_headline,this_plot_homedir_samp,plotname,list_of_df_to_plot,ylable,0,"distil")


def creat_plot_SMM_SMMP_SMMPP(plotsdir,df_all):#for SMM,distillation effect over RelD,RelSeg
	this_plot_homedir=plotsdir+'/Distil-effect-Doc/'#onDoc/'
	if not os.path.exists( this_plot_homedir ):
		os.makedirs( this_plot_homedir )
	for sampling in ['Accumulative','Fixed']:
		this_plot_homedir_samp=this_plot_homedir+'/'
		if not os.path.exists( this_plot_homedir_samp ):
			os.makedirs( this_plot_homedir_samp )
		for evalmethod in ['normal','residual']:
			for measure in ['map']:
				if measure == 'map':
					ylable='MAP'
				else:
					ylable='P5'
				# #now the setup is set. for each model should creat the table and plot
				a=df_all
				mask=a.apply( lambda x:  sampling in str( x ), axis=1 )
				a=a[mask]
				a=a[(a['rerank_setup'] == 'non')]
				a['positive']='$D_{r}$'
				a['full_setup_name']=a['full_setup_name'].apply( lambda x: str( x ).split( '_' )[1] )
				a=a[(a['full_setup_name'] != 'RelSeg')]
				a['distil'][(a['distil'] == 'NRD')]='$D_{nr}$'
				a['distil'][(a['distil'] == 'NRSeg')]='$Seg_{nr}$'
				a['distil'][(a['distil'] == 'NRD_NRSeg')]='$D_{nr},Seg_{nr}$'
				a['model'][a['model'] == 'SMMPP']='$4CSMM_a$'+' '+'$q_{pos}$('+a['positive']+','+a['distil']+')'
				a['model'][a['model'] == 'SMMP']='$3CSMM_a$'+' '+'$q_{pos}$('+a['positive']+','+a['distil']+')'
				a['model'][a['model'] == 'SMM']='$SMM_a$'+' '+'$q_{pos}$('+a['positive']+')'
				a['unique_key']=a['model']#+' '+a['positive']
				measures_to_check=[measure+'_'+evalmethod]
				list_of_df_to_plot=[]
				for m in measures_to_check:
					#print a[['unique_key', 'fb_docs', m]]
					ai=pd.DataFrame( a[['unique_key', 'fb_docs', m]] )
					ai=ai.pivot(columns='fb_docs', index='unique_key', values=m)
					list_of_df_to_plot+=[ai]
				plotname='Doc-Distillation-effect-no-rerank'+'-'+measure+'-'+sampling+'-'+evalmethod
				plot_headline='3C-SMM'+'-'+measure+'-'+sampling+'-'+evalmethod+'-Distillation-effect-onDocs'
				plt.paper_plot(plot_headline,this_plot_homedir_samp,plotname,list_of_df_to_plot,ylable,0,'distil')



def creat_plot_rerank_all(plotsdir, RM, SMM):# for SMM, RM+P,  SMMP+NRSEG, SMMP+NRD, rerank effect over RelD,RelSeg {non,NRD,NRSeg}
	this_plot_homedir=plotsdir+'/single_multi_compare/'
	if not os.path.exists( this_plot_homedir ):
		os.makedirs( this_plot_homedir )
	for sampling in ['Accumulative']:
		this_plot_homedir_samp=this_plot_homedir+'/'
		if not os.path.exists( this_plot_homedir_samp ):
			os.makedirs( this_plot_homedir_samp )
		this_plot_homedir_model=this_plot_homedir_samp
		if not os.path.exists( this_plot_homedir_model ):
			os.makedirs( this_plot_homedir_model )
		plotsdir_pos=this_plot_homedir_model+'/'
		if not os.path.exists( plotsdir_pos ):
			os.makedirs( plotsdir_pos )
		for evalmethod in ['normal', 'residual']:
			for measure in ['map']:
				list_of_df_to_plot=[]
				for model in ['SMM', 'RM']:
					if measure == 'map':
						ylable='MAP'
					else:
						ylable='P5'
					# #now the setup is set. for each model should creat the table and plot
					add='$'
					if model == 'RM':
						a=RM
						modelname='RM3'
						add='^{uni}$)'
					elif model == 'SMM':
						a=SMM
						modelname='SMM_a'
					mask=a.apply( lambda x: sampling in str( x ), axis=1 )
					a=a[mask]
					if a.shape[0] == 0:
						continue
					a['full_setup_name']=a['full_setup_name'].apply( lambda x: str( x ).split( '_' )[1] )
					#a=a[a['full_setup_name']==pos]
					a=a[(a['full_setup_name'] != 'RelDP')]
					#a['full_setup_name'][(a['full_setup_name'] == 'RelDP')]='$D_{r}^{percentage}$'
					#print a
					a=a[(a['full_setup_name'] == 'RelSeg')]
					a['full_setup_name'][(a['full_setup_name'] == 'RelSeg')]='$q_{pos}$($Seg_{r}$'
					a['full_setup_name']=a['full_setup_name']+')'
					a=a[a['rerank_setup'] != 'non']
					a['rerank_setup'][a['rerank_setup'] == 'single_neg-combined_fuse_res']='$SN$'
					a=a[a['rerank_setup']!= 'single_neg-NRD_fus_res']
					a=a[a['rerank_setup'] != 'single_neg-NRSeg_fus_res']
					a=a[a['rerank_setup']!= 'multi_neg-NRD_fus_res']
					a=a[a['rerank_setup'] != 'multi_neg-NRSeg_fus_res']
					a=a[a['rerank_setup']!= 'multi_neg_avg-NRD_fus_res']
					a=a[a['rerank_setup'] != 'multi_neg_avg-NRSeg_fus_res']
					a['rerank_setup'][a['rerank_setup'] == 'multi_neg-combined_fuse_res']='$MN_{max}$'
					a['rerank_setup'][a['rerank_setup'] == 'multi_neg_avg-combined_fuse_res']='$MN_{avg}$'
					a['unique_key']=a['rerank_setup']+' '+a['full_setup_name']
					measures_to_check=[measure+'_'+evalmethod]
					for m in measures_to_check:
						#print a.columns
						#print m
						ai=pd.DataFrame( a[['unique_key', 'fb_docs', m]] )
						ai['unique_key']=ai['unique_key'].apply( lambda x: '$'+modelname+'$  '+str( x ) )
						ai=ai.pivot( columns='fb_docs', index='unique_key', values=m )
						#print ai.index
						list_of_df_to_plot+=[ai]
				plotname='single-multi-compare'+'-'+measure+'-'+sampling+'-'+evalmethod
				plot_headline='single-multi-compare'+'-'+measure+'-'+sampling+'-'+evalmethod+'-rerank-over-all'
				plt.paper_plot( plot_headline, plotsdir_pos, plotname, list_of_df_to_plot, ylable, 0,'snMn' )

def creat_plot_rerank_vs_distill(plotsdir,RM,SMM,SMMP,SMMPP,withql=0):#for SMM, RM+P,  SMMP+NRSEG, SMMP+NRD, rerank effect over RelD,RelSeg {non,NRD,NRSeg}
	this_plot_homedir=plotsdir+'/rerank-vs-distill/'
	if not os.path.exists( this_plot_homedir ):
		os.makedirs( this_plot_homedir )
	for sampling in ['Accumulative','Fixed']:
		for evalmethod in ['normal','residual']:
			for measure in ['map','p5']:
				for mname,model_set in [('RM',['RM']),('SMM',['SMM', 'SMMP'])]:
					list_of_df_to_plot=[]
					for model in model_set:
						if measure == 'map':
							ylable='MAP'
						else:
							ylable='P5'
						# #now the setup is set. for each model should creat the table and plot
						add='$'
						if model == 'SMMP':
							a=SMMP
							add='$'
							modelname='3CSMM_a'
							a['distil']=a['full_setup_name'].apply( lambda x: x.split( '_' )[2] if len( x.split( '_' ) ) > 2 else 'non' ) # if SMMP will hold distilation source. else will hold Non]
							a['distil'][(a['distil'] == 'NRD')]='$D_{nr}$'
							#a['distil'][(a['distil'] == 'NRSeg')]='$Seg_{nr}$'
							a=a[(a['distil'] != 'NRSeg')]
						elif model == 'SMMPP':
							a=SMMPP
							add='$'
							modelname='4CSMM_a'
						#a['distil']=a['full_setup_name'].apply( lambda x: x.split( '_' )[2] if len( x.split( '_' ) ) > 2 else 'non' ) # if SMMP will hold distilation source. else will hold Non]
						#a['distil'][(a['distil'] == 'NRD')]='$D_{nr}$'
						#a['distil'][(a['distil'] == 'NRSeg')]='$Seg_{nr}$'
						elif model == 'RM':
							a=RM
							modelname='RM3'
							add='^{uni}$)'
						elif model == 'SMM':
							a=SMM
							modelname='SMM_a'
						mask=a.apply( lambda x:  sampling in str( x ), axis=1 )
						a=a[mask]
						if a.shape[0]==0:
							continue
						a['full_setup_name']=a['full_setup_name'].apply( lambda x: str( x ).split( '_' )[1] )
						# a=a[a['full_setup_name']==pos]
						a['full_setup_name'][(a['full_setup_name'] == 'RelD')]='$q_{pos}$($D_{r}'+add
						a=a[(a['full_setup_name'] != 'RelDP')]
						#a['full_setup_name'][(a['full_setup_name'] == 'RelDP')]='$D_{r}^{percentage}$'
						#print a
						a=a[(a['full_setup_name'] == 'RelSeg')]
						a['full_setup_name'][(a['full_setup_name'] == 'RelSeg')]='$q_{pos}$($Seg_{r}$'
						a['full_setup_name']=a['full_setup_name']+')'
						if model!='SMMP':
							a=a[a['rerank_setup'] != 'non']
							a['rerank_setup'][a['rerank_setup'] == 'single_neg-NRD_fus_res']='$SN$'
							a=a[a['rerank_setup'] != 'single_neg-combined_fuse_res']
							a=a[a['rerank_setup'] != 'single_neg-NRSeg_fus_res']
							a=a[a['rerank_setup'] != 'multi_neg-combined_fuse_res']
							a=a[a['rerank_setup'] != 'multi_neg-NRSeg_fus_res']
							a=a[a['rerank_setup'] != 'multi_neg_avg-combined_fuse_res']
							a=a[a['rerank_setup'] != 'multi_neg_avg-NRSeg_fus_res']
							a['rerank_setup'][a['rerank_setup'] == 'multi_neg-NRD_fus_res']='$MN_{max}$'
							a['rerank_setup'][a['rerank_setup'] == 'multi_neg_avg-NRD_fus_res']='$MN_{avg}$'
						else:
							a=a[a['rerank_setup'] == 'non']
							a['rerank_setup'][a['rerank_setup'] == 'non']=''
						a['unique_key']=a['rerank_setup']+' '+a['full_setup_name']
						if evalmethod=='residual' and not withql:
							measures_to_check=[measure+'_'+evalmethod, 'QL_'+measure+'_'+evalmethod]
						else:
							measures_to_check=[measure+'_'+evalmethod]
						for m in measures_to_check:
							#print a.columns
							#print m
							ai=pd.DataFrame( a[['unique_key', 'fb_docs', m]] )
							if 'QL' in m:
								ai['unique_key']=ai['unique_key'].apply( lambda x:  '$QL$' )
							else:
								ai['unique_key']=ai['unique_key'].apply( lambda x: '$'+modelname+'$  '+str( x ) )
							ai=ai.drop_duplicates()
							ai=ai.pivot(columns='fb_docs', index='unique_key', values=m)
							#print ai.index
							list_of_df_to_plot+=[ai]
					plotname='rerank-vs-distill'+'-'+measure+'-'+sampling+'-'+evalmethod+'-'+mname
					plot_headline='rerank-vs-distill'+'-'+measure+'-'+sampling+'-'+evalmethod+'-'+mname
					plt.paper_plot(plot_headline,this_plot_homedir,plotname,list_of_df_to_plot,ylable,0,'snMn')



def creat_plot_rerank_vs_distill_shortpaper(plotsdir, SMM, SMMP, SMMPP):# for SMM, RM+P,  SMMP+NRSEG, SMMP+NRD, rerank effect over RelD,RelSeg {non,NRD,NRSeg}
	this_plot_homedir=plotsdir+'/rerank-vs-distill/'
	if not os.path.exists( this_plot_homedir ):
		os.makedirs( this_plot_homedir )
	for sampling in ['Accumulative', 'Fixed']:
		for measure in ['map', 'p5']:
			for mname, model_set in [('SMM', ['SMM', 'SMMP'])]:
				list_of_df_to_plot=[]
				for model in model_set:
					if measure == 'map':
						ylable='MAP'
					else:
						ylable='P5'
					# #now the setup is set. for each model should creat the table and plot
					add='$'
					if model == 'SMMP':
						a=SMMP
						add='$'
						modelname='Distil('
						a['distil']=a['full_setup_name'].apply( lambda x: x.split( '_' )[2] if len( x.split( '_' ) ) > 2 else 'non' ) # if SMMP will hold distilation source. else will hold Non]
						a['distil'][(a['distil'] == 'NRD')]='$N_{doc}$'
						#a['distil'][(a['distil'] == 'NRSeg')]='$Seg_{nr}$'
						a=a[(a['distil'] != 'NRSeg')]
					elif model == 'SMMPP':
						a=SMMPP
						add='$'
						modelname='4CSMM_a'
					elif model == 'SMM':
						a=SMM
						modelname='SF'
					mask=a.apply( lambda x: sampling in str( x ), axis=1 )
					a=a[mask]
					if a.shape[0] == 0:
						continue
					a['full_setup_name']=a['full_setup_name'].apply( lambda x: str( x ).split( '_' )[1] )
					# a=a[a['full_setup_name']==pos]
					a=a[(a['full_setup_name'] == 'RelSeg')]
					a['full_setup_name'][(a['full_setup_name'] == 'RelSeg')]='$R_{psg}$'
					if model != 'SMMP':
						a['full_setup_name']=a['full_setup_name']+''
						a=a[a['rerank_setup'] != 'non']
						a=a[a['rerank_setup'] != 'single_neg-NRD_fus_res']
						a=a[a['rerank_setup'] != 'single_neg-combined_fuse_res']
						a=a[a['rerank_setup'] != 'single_neg-NRSeg_fus_res']
						a=a[a['rerank_setup'] != 'multi_neg-combined_fuse_res']
						a=a[a['rerank_setup'] != 'multi_neg-NRSeg_fus_res']
						a=a[a['rerank_setup'] != 'multi_neg_avg-combined_fuse_res']
						a=a[a['rerank_setup'] != 'multi_neg_avg-NRSeg_fus_res']
						a=a[a['rerank_setup'] != 'multi_neg_avg-NRD_fus_res']
						a['unique_key']='blabla'
						a['unique_key'][a['rerank_setup'] == 'multi_neg-NRD_fus_res']='$($'+a['full_setup_name']+'$,N_{doc})$'
					else:
						a['full_setup_name']=a['full_setup_name']+","+a['distil']+'$)$'
						a=a[a['rerank_setup'] == 'non']
						a['unique_key']=a['full_setup_name']
					measures_to_check=[measure+'_residual', measure+'_normal']
					for m in measures_to_check:
						ai=pd.DataFrame( a[['unique_key', 'fb_docs', m]] )
						if 'normal' in m:
							ai['unique_key']=ai['unique_key'].apply( lambda x: '$normal$ '+modelname+str( x ) )
						else:
							ai['unique_key']=ai['unique_key'].apply( lambda x: '$residual$ '+modelname+str( x ) )
						ai=ai.drop_duplicates( )
						ai=ai.pivot( columns='fb_docs', index='unique_key', values=m )
						#print ai.index
						list_of_df_to_plot+=[ai]
				plotname='rerank-vs-distill'+'-'+measure+'-'+sampling+'-'+mname
				plot_headline='rerank-vs-distill'+'-'+measure+'-'+sampling+'-'+mname
				plt.paper_plot( plot_headline, this_plot_homedir, plotname, list_of_df_to_plot, ylable, 0, 'rVSd' )



def creat_plot_SMM_SMMP_norerank_all_QLpercentage(plotsdir,df_all,residual_with_ql=0,with_segmentsSMMPP=0,with_SMMPP=0,qlpercent=1):#for SMM,distillation effect over RelD,RelSeg
	this_plot_homedir=plotsdir+'/Distil-effect-all/'
	if not os.path.exists( this_plot_homedir ):
		os.makedirs( this_plot_homedir )
	for sampling in ['Fixed','Accumulative']:
		this_plot_homedir_samp=this_plot_homedir+'/'
		if not os.path.exists( this_plot_homedir_samp ):
			os.makedirs( this_plot_homedir_samp )
		for evalmethod in ['normal','residual']:
			for measure in ['map','p5']:
				if measure == 'map':
					ylable='MAP'
				else:
					ylable='P5'
				# #now the setup is set. for each model should creat the table and plot
				a=df_all
				mask=a.apply( lambda x:  sampling in str( x ), axis=1 )
				a=a[mask]
				a=a[(a['rerank_setup'] == 'non')]
				a['positive']='$D_{r}$'
				a['full_setup_name']=a['full_setup_name'].apply( lambda x: str( x ).split( '_' )[1] )
				a['positive'][(a['full_setup_name'] == 'RelSeg')]='$Seg_{r}$'
				a['distil'][(a['distil'] == 'NRD')]='$D_{nr}$'
				a['distil'][(a['distil'] == 'NRSeg')]='$Seg_{nr}$'
				a['distil'][(a['distil'] == 'NRD_NRSeg')]='$D_{nr},Seg_{nr}$'
				if not with_segmentsSMMPP:
					mask=a.apply( lambda x: not( 'SMMPP' in str( x ) and 'RelSeg' in str(x) ), axis=1 )
					a=a[mask]
				if not with_SMMPP:
					mask=a.apply( lambda x: not ( 'SMMPP' in str( x )  ), axis=1 )
					a=a[mask]
				a['model'][a['model'] == 'SMMPP']='$4CSMM_a$'+' '+'$q_{pos}$('+a['positive']+','+a['distil']+')'
				a['model'][a['model'] == 'SMMPP']='$4CSMM_a$'+' '+'$q_{pos}$('+a['positive']+','+a['distil']+')'
				a['model'][a['model'] == 'SMMP']='$3CSMM_a$'+' '+'$q_{pos}$('+a['positive']+','+a['distil']+')'
				a['model'][a['model'] == 'SMM']='$SMM_a$'+' '+'$q_{pos}$('+a['positive']+')'
				a['unique_key']=a['model']#+' '+a['positive']
				if evalmethod == 'residual' and residual_with_ql:
					measures_to_check=[measure+'_'+evalmethod, 'QL_'+measure+'_'+evalmethod]
				elif qlpercent:
					a['QL_improvement_percentage'+measure+'_'+evalmethod]=(a[measure+'_'+evalmethod]-a['QL_'+measure+'_'+evalmethod])/a[measure+'_'+evalmethod]
					#print a['QL_improvement_percentage'+measure+'_'+evalmethod]
					measures_to_check=['QL_improvement_percentage'+measure+'_'+evalmethod]
					if measure == 'map':
						ylable=" %  MAP improvement wrt QL"
					else:
						ylable="% P5 improvement wrt QL"
				else:
					measures_to_check=[measure+'_'+evalmethod]
				list_of_df_to_plot=[]
				for m in measures_to_check:
					# print a.columns
					#print m
					ai=pd.DataFrame( a[['unique_key', 'fb_docs', m]] )
					if 'QL' in m and residual_with_ql:
						ai['unique_key']=ai['unique_key'].apply( lambda x: '$D_{init}$' )
						ai=ai.drop_duplicates( )
					ai=ai.pivot( columns='fb_docs', index='unique_key', values=m )
					#print ai.index
					list_of_df_to_plot+=[ai]
				plotname='Distillation-effect-no-rerank'+'-'+measure+'-'+sampling+'-'+evalmethod
				plot_headline='3C-SMM'+'-'+measure+'-'+sampling+'-'+evalmethod+'-Distillation-effect-no-rerank'
				plt.paper_plot(plot_headline,this_plot_homedir_samp,plotname,list_of_df_to_plot,ylable,0,"distil")

#creat_plot_rerank_vs_distill_shortpaper(plotsdirout,SMM,SMMP,SMMPP)

#creat_plot_SMM_SMMP_norerank_all_QLpercentage(plotsdirout,SMM_all,0,0,0,1)
creat_plot_SMM_SMMP_norerank_all(plotsdirout,SMM_all,1,0)
creat_plot_SMM_SMMP_norerank_all(plotsdirout,SMM_all,0,0)
#creat_plot_SMM_SMMP_SMMPP(plotsdirout,SMM_all)

creat_plot_seperate_rerank_effect_basic(plotsdirout,SMM,RM, SMMP,SMMPP,1,1,0,1)
creat_plot_seperate_rerank_effect_basic(plotsdirout,SMM,RM, SMMP,SMMPP,1,0,0,1)
print "______________________________________________________________________"
creat_plot_seperate_rerank_effect_basic(plotsdirout,SMM,RM, SMMP,SMMPP,1,0,0,0)
creat_plot_seperate_rerank_effect_basic(plotsdirout,SMM,RM, SMMP,SMMPP,1,1,0,0)


#creat_plot_rerank_all(plotsdirout, RM, SMM)
