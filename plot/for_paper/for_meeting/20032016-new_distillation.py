__author__='Elinor'

import os
import pandas as pd
import sys
scriptpath="/lv_local/home/elinor/pycharm/nf/"
sys.path.append(os.path.dirname(scriptpath))
from plot import plotfromdata as plt

plotsdirout='/lv_local/home/elinor/negative_feedback/reports/comparable_plots_residual_all/'
datasdirout='/lv_local/home/elinor/negative_feedback/reports/data_comparable_plots_residual_all/'
#csv_source='/lv_local/home/elinor/negative_feedback/reports/191115extra_list_all_clean.csv'#this is from before caching the reranking cvloo to simple aggregation
csv_source='/lv_local/home/elinor/negative_feedback/reports/20032016_short_plus_new_distill/data_residualall_normal2212.csv'

SMM, SMM_all, SMMP, SMMPP, RM, all_table=plt.read_and_proccess( plotsdirout,datasdirout,csv_source,0 )
SMMPrerank=all_table[all_table['model'] == 'SMMP-rerank']
SMMPrerank=SMMPrerank[['full_setup_name', 'rerank_setup', 'fb_docs', 'map_normal', 'p5_normal', 'map_residual', 'p5_residual', 'QL_map_residual', 'QL_p5_residual', 'QL_map_normal', 'QL_p5_normal']]

SMM_allPlus=all_table[(all_table['model'] == 'SMM') | (all_table['model'] == 'SMMP-rerank')|(all_table['model'] == 'SMMP') | (all_table['model'] == 'SMMPP')]
SMM_allPlus=SMM_allPlus[['full_setup_name', 'model', 'rerank_setup', 'fb_docs', 'map_normal', 'p5_normal', 'map_residual', 'p5_residual', 'QL_map_residual', 'QL_p5_residual', 'QL_map_normal', 'QL_p5_normal']]
	#SMM_all['distil']=SMM_all['full_setup_name'].apply( lambda x: x.split( '_' )[2] if len( x.split( '_' ) ) ==3 else 'non' ) # if SMMP will hold distilation source. else will hold Non]
SMM_allPlus['distil']=SMM_allPlus['full_setup_name'].apply( lambda x: '_'.join( x.split( '_' )[2:] ) if len( x.split( '_' ) ) > 2 else 'non' ) # if SMMP will hold distilation source. else will hold Non]
	#print SMM_all

def creat_plot_SMM_SMMP_norerank_all(plotsdir,df_all,residual_with_ql=0,with_segmentsSMMPP=0,with_SMMPP=0):#for SMM,distillation effect over RelD,RelSeg
	this_plot_homedir=plotsdir+'/Distil-effect-new-all/'
	if not os.path.exists( this_plot_homedir ):
		os.makedirs( this_plot_homedir )
	for sampling in ['Fixed','Accumulative']:
		this_plot_homedir_samp=this_plot_homedir+'/'
		if not os.path.exists( this_plot_homedir_samp ):
			os.makedirs( this_plot_homedir_samp )
		for evalmethod in ['normal','residual']:
			for measure in ['map','p5']:
				if measure == 'map':
					ylable='MAP'
				else:
					ylable='P5'
				# #now the setup is set. for each model should creat the table and plot
				a=df_all
				mask=a.apply( lambda x:  sampling in str( x ), axis=1 )
				a=a[mask]
				a=a[(a['rerank_setup'] == 'non')]
				a['positive']='$R_{d}$'
				a['full_setup_name']=a['full_setup_name'].apply( lambda x: str( x ).split( '_' )[1] )
				a['positive'][(a['full_setup_name'] == 'RelSeg')]='$R_{p}$'
				a['distil'][(a['distil'] == 'NRD')]='$NR_{d}$'
				a['distil'][(a['distil'] == 'NRSeg')]='$NR_{p}$'
				a['distil'][(a['distil'] == 'NRD_NRSeg')]='$NR_{p},NR_{d}$'
				if not with_segmentsSMMPP:
					mask=a.apply( lambda x: not( 'SMMPP' in str( x ) and 'RelSeg' in str(x) ), axis=1 )
					a=a[mask]
				if not with_SMMPP:
					mask=a.apply( lambda x: not ( 'SMMPP' in str( x )  ), axis=1 )
					a=a[mask]
				print set(a['model'])
				a['model'][a['model'] == 'SMMPP']='$4CSMM_a$'+' '+'$q_{pos}$('+a['positive']+','+a['distil']+')'
				a['model'][a['model'] == 'SMMPP']='$4CSMM_a$'+' '+'$q_{pos}$('+a['positive']+','+a['distil']+')'
				a['model'][a['model'] == 'SMMP']='Distill'+'('+a['positive']+','+a['distil']+')'
				a['model'][a['model'] == 'SMMP-rerank']='RDistill'+'('+a['positive']+','+a['distil']+')'
				a['model'][a['model'] == 'SMM']='MM'+''+'('+a['positive']+')'
				a['unique_key']=a['model']#+' '+a['positive']
				if residual_with_ql:#evalmethod == 'residual' and
					measures_to_check=[measure+'_'+evalmethod, 'QL_'+measure+'_'+evalmethod]
				else:
					measures_to_check=[measure+'_'+evalmethod]
				list_of_df_to_plot=[]
				for m in measures_to_check:
					# print a.columns
					#print m
					ai=pd.DataFrame( a[['unique_key', 'fb_docs', m]] )
					if 'QL' in m:
						ai['unique_key']=ai['unique_key'].apply( lambda x: '$D_{init}$' )
						ai=ai.drop_duplicates( )
						#print ai
						ai=ai.pivot( columns='fb_docs', index='unique_key', values=m )
						#ai=ai.reindex(['Distill($R_{doc}$,$N_{doc}$)','Distill($R_{doc}$,$N_{psg}$)','Distill($R_{psg}$,$N_{doc}$)','Distill($R_{psg}$,$N_{psg}$)',"MM($R_{doc}$)","MM($R_{psg}$)",'$D_{init}$'])I once throw this away because it didn't work with (and worked without)
						#ai=ai.reindex(["MM($R_{doc}$)","MM($R_{por}$)",'$\\theta^q_{rel}$($R_{doc}$,$N_{doc}$)','$\\theta^q_{rel}$($R_{doc}$,$N_{por}$)','$\\theta^q_{rel}$($R_{por}$,$N_{doc}$)','$\\theta^q_{rel}$($R_{por}$,$N_{por}$)']) I once throw this away because it didn't work with (and worked without)
					else:
						ai=ai.pivot( columns='fb_docs', index='unique_key', values=m )
						#ai=ai.reindex(['Distill($R_{d}$,$NR_{d}$)','Distill($R_{d}$,$NR_{p}$)','Distill($R_{p}$,$NR_{d}$)','Distill($R_{p}$,$NR_{p}$)',"MM($R_{d}$)","MM($R_{p}$)"])
					#print ai.index
					#print ai
					list_of_df_to_plot+=[ai]
				plotname='Distillation-effect-no-rerank'+'-'+measure+'-'+sampling+'-'+evalmethod+["",'-w-QL'][residual_with_ql]
				plot_headline='3C-SMM'+'-'+measure+'-'+sampling+'-'+evalmethod+'-Distillation-effect-no-rerank'
				plt.paper_plot(plot_headline,this_plot_homedir_samp,plotname,list_of_df_to_plot,ylable,0,"distilPlus")


creat_plot_SMM_SMMP_norerank_all(plotsdirout,SMM_allPlus,0,0)
