__author__='Elinor'
import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt
import pandas as pd
import matplotlib.cbook as cbook
from pylab import plotfile, savefig, gca
from matplotlib.gridspec import GridSpec
#matplotlib.style.use('ggplot')
matplotlib.style.use('grayscale')


import os
plotsdir='/lv_local/home/elinor/negative_feedback/reports/comparable_plots/'
if not os.path.exists( '/lv_local/home/elinor/negative_feedback/reports/' ):
	os.makedirs( '/lv_local/home/elinor/negative_feedback/reports/' )
if not os.path.exists( plotsdir ):
		os.makedirs( plotsdir )


#seperate to rerank as well
def graph_fb_measure(type,measure,main_title,csv_source,flagRM):
	mt=["2","d","v",'s','>','h','+','*','6']
	if flagRM == 1:
		outputname='RM'
	else:
		outputname='SMM'
	outputname+='_'+measure+'_'+type
	from matplotlib.gridspec import GridSpec
	matplotlib.style.use( 'grayscale' )
	rm=pd.DataFrame.from_csv(csv_source)
	rm=rm.drop(['full_setup_name'],axis=0)
	if flagRM:
		rmP=pd.DataFrame(rm.loc[['Fixed_RelDP','Accumulative_RelDP']])
		rm=rm.drop(['Fixed_RelDP','Accumulative_RelDP'],axis=0)
	plt.suptitle(main_title)
	gs1 = GridSpec(4, 4)
	gs1.update( hspace=1.2)
	ax1 = plt.subplot(gs1[0, :])
	ax2 = plt.subplot(gs1[1, :])
	ax3 = plt.subplot(gs1[2, :])
	ax4 = plt.subplot(gs1[3, :])
	f=plt.figure( 1)
	for axi,Df_setup in zip([ax1,ax2,ax3,ax4],sorted(set(rm.index))):
		i=0;
		if flagRM == True:
			u=', uniform'
		else:
			u=''
		table=rm.loc[Df_setup]
		rerank_types=table['rerank_setup'].tolist()
		#drawing one time those lines how are the same for all
		temp=table.set_index( ['rerank_setup'] )
		table_normal=temp.loc['non'].set_index(['fb_docs'])
		table_normal=table_normal.astype( 'float64' )
		if type=='normal':
			table_normal=table_normal[['QL_'+measure+'_'+type]]
			table_normal.plot( ax=axi, marker=mt[i], markersize=3 )
			i+=1;
		for rerank_setup in set(rerank_types):
			temp=table.set_index(['rerank_setup'])
			minitable=temp.loc[rerank_setup]
			minitable=minitable.set_index(['fb_docs'])
			if type=='residual':
				table_normal=minitable[['QL_'+measure+'_'+type,measure+'_'+type]]
			else:
				table_normal=minitable[[measure+'_'+type]]
			if str(rerank_setup)=='NRSeg':
				lxrerank='_$Seg_{nr}$'+u
			elif str(rerank_setup)=='NRD':
				lxrerank='_'+'$D_{nr}$'+u
			elif str(rerank_setup)=='non':
				lxrerank=''+u
			table_normal=table_normal.rename( columns={measure+'_'+type: measure+'_'+type+lxrerank} ).astype( 'float64' )
			if type=='residual':
				table_normal=table_normal.rename( columns={'QL'+measure+'_'+type: 'QL'+measure+'_'+type+lxrerank} ).astype( 'float64' )
			table_normal.plot( ax=axi, marker=mt[i], markersize=3 )#plots the uniform part
			i+=1
			if str(Df_setup).split('_')[1]=='RelD' or str(Df_setup).split('_')[1]=='RelDP':
				lx='$D_r$'
			if str(Df_setup).split('_')[1]=='RelSeg':
				lx='$Seg_r$'
			if flagRM==True:
				if str(Df_setup).split('_')[1]=='RelD':
					p=', relevance percentage'
					ntable=rmP.loc[str(Df_setup)+'P']
					temp=ntable.set_index( ['rerank_setup'] )
					minitable=temp.loc[rerank_setup]
					minitable=minitable.set_index( ['fb_docs'] )
					add_table=minitable.astype( 'float64' )
					add_table=add_table[[measure+'_'+type]]
					if str( rerank_setup ) == 'NRSeg':
						lxrerank='_$Seg_{nr}$'+p
					elif str( rerank_setup ) == 'NRD':
						lxrerank='_'+'$D_{nr}$'+p
					elif str( rerank_setup ) == 'non':
						lxrerank=''+p
					add_table=add_table.rename( columns={measure+'_'+type: measure+'_'+type +lxrerank} ).astype( 'float64' )
					add_table.plot( ax=axi, marker="o", markersize=3 )#plots the % part
					i=i+1
			axi.legend( loc='center left', bbox_to_anchor=(1.0, 0.5), prop={'size': 5} )
			axtitle='$K$-   $'+str(Df_setup).split('_')[0]+'$ approach, information used for $q_{pos}$ is '+lx
			axi.xaxis.set_major_locator((plt.MaxNLocator(integer=True)))
			axi.set_xlabel(axtitle,fontsize=8)
			axi.tick_params(axis='y', which='major', labelsize=6)
			axi.tick_params(axis='x', which='major', labelsize=6)
	f.subplots_adjust(right=0.7)
	plt.savefig( plotsdir+outputname+'.png' ,format='png', dpi = 300)#change to 1000 after all
	f.clear()

def graph_fb_measure_SMM_P_compare(type,measure,csv_sourceSMM,csv_sourceSMMP):
	mt=["d","v",'s','>','h','+','*',"2",'6']
	outputname='SMM_SMMP_COMPARE'
	main_title=measure+' '+type+' '+'SMM SMMP comparison '
	outputname+='_'+measure+'_'+type
	from matplotlib.gridspec import GridSpec
	matplotlib.style.use( 'grayscale' )
	SMM=pd.DataFrame.from_csv(csv_sourceSMM)
	if 'full_setup_name' in SMM.index:
		SMM=SMM.drop(['full_setup_name'],axis=0)
	SMMP=pd.DataFrame.from_csv( csv_sourceSMMP )
	if 'full_setup_name' in SMMP.index:
		SMMP=SMMP.drop(['full_setup_name'],axis=0)
	for Df_setup in sorted(set(SMM.index)):
		if str( Df_setup ).split( '_' )[1] == 'RelD' or str( Df_setup ).split( '_' )[1] == 'RelDP':
			lx='$D_r$'
		if str( Df_setup ).split( '_' )[1] == 'RelSeg':
			lx='$Seg_r$'
		plt.suptitle( main_title + lx )
		gs1=GridSpec( 2, 4 )
		gs1.update( hspace=0.5 )
		ax1=plt.subplot( gs1[0, :] )
		ax2=plt.subplot( gs1[1, :] )
		f=plt.figure( 1 )
		table=SMMP[(Df_setup in SMMP.index)]
		rerank_types=table['rerank_setup'].tolist( )
		for rerank_setup, axi in zip (sorted( set( rerank_types )),[ax1,ax2] ):
			i=0;
			for rm,name in zip([SMM,SMMP],["SMM","SMMP"]):
				u=''
				table=rm.loc[Df_setup]
				temp=table.set_index(['rerank_setup'])
				minitable=temp.loc[rerank_setup]
				minitable=minitable.set_index(['fb_docs'])
				table_normal=minitable[[measure+'_'+type]]
				if str(rerank_setup)=='NRSeg':
					lxrerank='_$Seg_{nr}$'+u
				elif str(rerank_setup)=='NRD':
					lxrerank='_'+'$D_{nr}$'+u
				table_normal=table_normal.rename( columns={measure+'_'+type: name+'_'+lx+lxrerank} ).astype( 'float64' )
				table_normal.plot( ax=axi, marker=mt[i], markersize=4 )#plots the uniform part
				i+=1
			axi.legend( loc='center left', bbox_to_anchor=(1.0, 0.5), prop={'size': 5} )
			axtitle='$K$-   $'+str(Df_setup).split('_')[0]+'$ approach, information used for $q_{pos}$ is '+lx+', information used for $q_{neg}$ is '+ lxrerank.strip('_')
			axi.xaxis.set_major_locator((plt.MaxNLocator(integer=True)))
			axi.set_xlabel(axtitle,fontsize=8)
			axi.tick_params(axis='y', which='major', labelsize=6)
			axi.tick_params(axis='x', which='major', labelsize=6)
		f.subplots_adjust(right=0.7)
		plt.savefig( plotsdir+outputname+'_'+Df_setup+'.png' ,format='png', dpi = 300)#change to 1000 after all
		f.clear()


"""
now this version works for normal map, SMM, rm
"""


#csv_path='/lv_local/home/elinor/negative_feedback/RM/RM.csv'

#for measure in ['p5','map']:
#	for type in ['residual','normal']:
		#main_title=measure+' '+type+' over different $K$'
		#graph_fb_measure( type, measure, main_title, csv_path, flagRM=1 )
		#graph_fb_measure( type, measure, main_title, '/lv_local/home/elinor/negative_feedback/baselines/SMM.csv', 0 )

# for measure in ['p5','map']:
# 	for type in ['residual','normal']:
# 		graph_fb_measure_SMM_P_compare3(type,measure,'/lv_local/home/elinor/negative_feedback/baselines/SMM.csv','/lv_local/home/elinor/negative_feedback/SMMP/fbSMMPfixed.csv')

