__author__='Elinor'
"""
this code creat graph that compares smm and smmp's under different rankings
"""
import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt
import pandas as pd
import matplotlib.cbook as cbook
from pylab import plotfile, savefig, gca
matplotlib.style.use('grayscale')



csv_sourceSMM="/lv_local/home/elinor/negative_feedback/baselines/SMM.csv"



def from_table_to_graph_smmpcomapre(type,measure,main_title,table,sampling,positive,out_dir,qlflag): ## this function will receive  a table and will plot it for each rerank: non, NRD, NRSeg
	mt=["o","d","v",'s','>','h','+','*','6']
	outputname=sampling+'_SMM_compare'
	outputname+='_'+measure+'_'+type+positive
	from matplotlib.gridspec import GridSpec
	matplotlib.style.use( 'grayscale' )
	plt.suptitle(main_title)
	gs1 = GridSpec(3, 3)
	gs1.update( hspace=1.2)
	ax1 = plt.subplot(gs1[0, :])
	ax2 = plt.subplot(gs1[1, :])
	ax3 = plt.subplot(gs1[2, :])
	f=plt.figure( 1)
	rerank_types=table['rerank_setup'].tolist( )
	for axi, rerank_setup in zip( [ax1, ax2, ax3], sorted(  set( rerank_types ) ) ):
		i=0;
		w= pd.DataFrame(table.reset_index()['rerank_setup'])
		#print table.reset_index()
		tempmask=w.apply( lambda x: rerank_setup in str( x ), axis=1 )
		table_rerank=table.reset_index()[tempmask].set_index(['full_setup_name'])
		Df_list=table_rerank.index.tolist()
		for Df_setup in sorted(set(Df_list)):
			#drawing one time those lines how are the same for all
			table_plot=table_rerank.loc[Df_setup]
			a=table_plot.astype('object').set_index(['fb_docs'])
			if type == 'residual' and qlflag:
				a=a[['QL'+measure+'_'+type, measure+'_'+type]]
			else:
				a=a[[measure+'_'+type]]
			if len( str( Df_setup ).split( '_' ) ) == 2:
				lxA='SMM '
				if str(Df_setup).split('_')[1]=='RelD':
					lxB='$D_r$'
				if str(Df_setup).split('_')[1]=='RelSeg':
					lxB='$Seg_r$'
			if len(str( Df_setup ).split( '_' ))==3:
				lxA='SMMP '
				if str(Df_setup).split('_')[1]=='RelD':
					lxB='$D_r$_'
				if str(Df_setup).split('_')[1]=='RelSeg':
					lxB='$Seg_r$_'
				if str( Df_setup ).split( '_' )[2] == 'RelD':
					lxB+='$D_r$'
				if str( Df_setup ).split( '_' )[2] == 'RelSeg':
					lxB+='$Seg_r$'
				if str( Df_setup ).split( '_' )[2] == 'NRD':
					lxB+='$D_{nr}$'
				if str( Df_setup ).split( '_' )[2] == 'NRSeg':
					lxB+='$Seg_{nr}$'
			lx=lxA+lxB
			if str( rerank_setup ) == 'NRSeg':
				lxrerank='rerank with '+'_$Seg_{nr}$'
			elif str( rerank_setup ) == 'NRD':
				lxrerank='rerank with '+'_'+'$D_{nr}$'
			elif str( rerank_setup ) == 'non':
				lxrerank='no rerank'
			a=a.rename( columns={measure+'_'+type: measure+'_'+type +'_'+ lx} ).astype('float64' )
			if type == 'residual' and qlflag:
				a=a.rename( columns={'QL'+measure+'_'+type: 'QL'+measure+'_'+type+lx} ).astype('float64' )
			a=a.astype('object' )
			a.plot( ax=axi,sharex =1,use_index=1, marker=mt[i], markersize=4 )# plots the uniform part
			i+=1
		axi.legend( loc='center left', bbox_to_anchor=(1.0, 0.5), prop={'size': 5} )
		#axi.xaxis.set_major_locator((plt.MaxNLocator(integer=True)))
		axi.set_xlabel(lxrerank,fontsize=8)
		axi.tick_params(axis='y', which='major', labelsize=6)
		axi.tick_params(axis='x', which='major', labelsize=6)
	f.subplots_adjust(right=0.7)
	plt.savefig(out_dir+ '/'+outputname+'.png' ,format='png', dpi = 300)#change to 1000 after all
	f.clear()




smmtable=pd.DataFrame.from_csv( csv_sourceSMM )
# a= pd.DataFrame(smmptable.full_setup_name.str.split('_').tolist())
for sampling in ['Fixed','Accumulative']:
	if sampling=='Fixed':
		csv_sourceSMMP="/lv_local/home/elinor/negative_feedback/SMMP/fbSMMPfixed.csv"
	else:
		csv_sourceSMMP="/lv_local/home/elinor/negative_feedback/SMMP/fbSMMPaccu.csv"
	smmptable=pd.DataFrame.from_csv( csv_sourceSMMP )
	for positive in ['RelD', 'RelSeg']:
		if positive=='RelD':
			pos='$D_r$'
		else:
			pos='$Seg_{r}$'
		mask=smmptable.apply( lambda x: positive in str( x ) and sampling in str( x ), axis=1 )
		#print smmptable.drop(['sampling'], axis=1)[mask]
		mask1=smmtable.apply( lambda x: positive in str( x ) and sampling in str( x ), axis=1 )
		#print smmtable[mask1]
		#a=pd.concat( [smmptable[mask].drop(['sampling'], axis=1), smmtable[mask1]], axis=0 )
		a=pd.concat( [smmptable[mask], smmtable[mask1]], axis=0 )
		#print a
		for measure in ['p5', 'map']:
			for type in ['residual', 'normal']:
				main_title=measure+' '+type+' over different $K$, $q_{pos}$ with '+pos
				from_table_to_graph_smmpcomapre( type, measure, main_title, a,sampling ,positive,'/lv_local/home/elinor/negative_feedback/smmcompare_plot/',0)
				from_table_to_graph_smmpcomapre( type, measure, main_title, a,sampling ,positive,'/lv_local/home/elinor/negative_feedback/smmcomparetobl/',1)
