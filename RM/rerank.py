__author__='Elinor'
"""

when fixed setup:
this params should not include the feedbackDocno we want to use, the Indri know what to take from the top fbdocs specified.
DropQueryWord should be 1
and the number of fbdocs should be changing according to how high we want it to be.
when accumulative:
this params should include the fbDoc we want to use for the accumulative (k relevant documents from the QL top down),
DropQueryWord should be 1
and the number of fbdocs should be constant and bigger then the number of used fb docs(bigger than 0 is good enough).
checked and works.

"""

import os
import os.path
import sys
import subprocess
import itertools
scriptpath="/lv_local/home/elinor/pycharm/nf/evaluation/"
sys.path.append(os.path.dirname(scriptpath))
from qpos_eval import eval_settings as es
feedback_setup_name=['RelSeg','NRSeg','NRD','RelD','PRF']
RM_setup_name=['NRseg','uni','percent']

#define paths

#initial_feedback_setup=1
#baseline_home_dir="/lv_local/home/elinor/negative_feedback/baselines/Accumulative_RelSeg"+'/'
baseline_home_dir=sys.argv[1]+'/'
NRD_concat=1#sys.argv[2]
rerank_setups=[2,3] #both would be 2,3

initial_feedback_setup_name=baseline_home_dir.rstrip('/').rstrip('/').split('_')[-1]
print initial_feedback_setup_name

setup=baseline_home_dir.rstrip('/').rstrip('/').split('/')[-1].split('_')[0]
print setup


original_RM_setup=2
if initial_feedback_setup_name=='RelSeg':
	initial_feedback_setup=1
elif initial_feedback_setup_name == 'RelD' or initial_feedback_setup_name == 'RelDP':
	initial_feedback_setup=4
	if initial_feedback_setup_name == 'RelDP':
		Pflag=1
		original_RM_setup=3
else:
	print 'error'


rerank_setups=[3,2]
if setup=="Fixed":
	flag_fixed=True
	max_K=10
else:
	flag_fixed=False
	max_K=5

start_param_file_path="/lv_local/home/elinor/negative_feedback/Inex_qrelsAndParams/param_part1_dropping_origQW_RM.xml"
orig_queries="/lv_local/home/elinor/negative_feedback/Inex_qrelsAndParams/INEX_params_b_segQ.xml"

init_ret_path='/lv_local/home/elinor/negative_feedback/QL/ql_ret_INEX.txt'
qrels_path='/lv_local/home/elinor/negative_feedback/Inex_qrelsAndParams/inex2009-2010-article.qrels_binary'
baseline_res=baseline_home_dir+'res'+'/'
##any number bigger than the amount of docs we are using for feedback
fbdoc_num=max_K+1

fbDocs=range(1,max_K+1)
fbOrigWeight=[0.2,0.5,0.8] #for file name only. within we pass
fbTerms=[10,25,50]


def define_paths(rerank_setup):
	global home_dir
	global output_param_files_dir
	home_dir=baseline_home_dir+'rerank_with_'+feedback_setup_name[rerank_setup-1]+'/'
	output_param_files_dir=home_dir+"parameters/"

def create_dir():
	if not os.path.exists(home_dir):
		os.makedirs(home_dir)
	l=["parameters/","res/","chunks/"]
	for dir in l:
		if not os.path.exists(home_dir+dir):
			os.makedirs(home_dir+dir)

#define arg



def read_query_info(orig_queries):
	#step one- for each query create a dictionary of the query words.
	global q_info
	q_info={}
	q_file=open(orig_queries,'r')
	query_str_a='\t<query>\n'
	query_str_b=str()
	for line in q_file:
		if 'number' in line:
			q_id=line.strip('\t\t<number>')
			q_id=q_id.rstrip('</number>\n\t\t')
			query_str_a+=line;
		if '<text>' in line:
			query_str_a+=line;
		if '<relseg>' in line:
			query_str_b+=line;
		if '</query>' in line:
			query_str_b+=line;
			q_info[q_id]=[query_str_a,query_str_b]
			query_str_a='\t<query>\n'
			query_str_b=str()
	q_file.close()
	return


def create_parameters_accumulative():
	read_query_info(orig_queries)
	es.getting_rel_docs_from_res_file( max_K, flag_fixed, init_ret_path, qrels_path )
	for rerank_setup in rerank_setups:
		define_paths( rerank_setup )
		create_dir( )
		rerankname=feedback_setup_name[rerank_setup-1]
		if rerankname == 'NRSeg' or NRD_concat:
			print rerankname
			RM_setup=1
		else:
			print rerankname
			RM_setup=2
		#start code:
		#i=0;
		start_param_file=open(start_param_file_path,'r')
		start_param=start_param_file.read()
		start_param_file.close()
		for fbDocs_parm in fbDocs:
			for fbOrigWeight_parm in fbOrigWeight:
				for term, setup in list( itertools.product( fbTerms, [rerank_setup] ) ):
					#writing parameter file part 1
					this_output=str()
					this_input_name=str(fbDocs_parm)+'_'+str(fbOrigWeight_parm)+'_'+str(term)+'_'+initial_feedback_setup_name+'_'+RM_setup_name[original_RM_setup-1]
					this_output_name=this_input_name+'_'+feedback_setup_name[setup-1]
					this_output_path=output_param_files_dir+this_output_name
					this_output+=start_param
					this_output+='\t<RM_Setup>'+str( RM_setup )+'</RM_Setup>\n'
					this_output+='\t<fbTerms>'+str(term)+'</fbTerms>\n'
					this_output+='\t<fbOrigWeight>'+str(0)+'</fbOrigWeight>\n'
					this_output+='\t<feedback_setup>'+str(setup)+'</feedback_setup>\n'
					this_output+='\t<fbDocs>'+str(10)+'</fbDocs>\n'
					for query in sorted(q_info.keys()):
						this_output+=q_info[query][0]
						if rerank_setup == 2:
							for docNo in es.Rel_used[fbDocs_parm][query]:
								this_output+='\t\t<feedbackDocno>'+str(docNo)+'</feedbackDocno>\n'
						elif rerank_setup == 3:
							for docNo in es.All_used[fbDocs_parm][query]:
								this_output+='\t\t<feedbackDocno>'+str( docNo )+'</feedbackDocno>\n'
						batch_cut_top='grep "'+query+' Q0 " '+baseline_res+this_input_name+" | awk '{ if ($4<1200) print $3;}'"
						output2 = subprocess.check_output(batch_cut_top, shell=True)
						for Docno in output2.rstrip('\n').split('\n'):
							this_output+='\t\t<workingSetDocno>'+Docno+'</workingSetDocno>\n'
						this_output+=q_info[query][1]
					this_output+="</parameters>\n"
					out_f=open(this_output_path,'w+')
					out_f.write(this_output)
					out_f.close()
		print "done"
	return 1

def create_parameters_fixed():
	read_query_info(orig_queries)
	es.getting_rel_docs_from_res_file( max_K, flag_fixed, init_ret_path, qrels_path )
	for rerank_setup in rerank_setups:
		define_paths( rerank_setup )
		create_dir( )
		rerankname=feedback_setup_name[rerank_setup-1]
		if rerankname == 'NRSeg' or NRD_concat:
			print rerankname
			RM_setup=1
		else:
			print rerankname
			RM_setup=2
		#start code:
		#i=0;
		start_param_file=open(start_param_file_path,'r')
		start_param=start_param_file.read()
		start_param_file.close()
		for fbDocs_parm in fbDocs:
			for fbOrigWeight_parm in fbOrigWeight:
				for term,setup in list(itertools.product(fbTerms,[rerank_setup])):
					#writing parameter file part 1
					this_output=str()
					this_input_name=str(fbDocs_parm)+'_'+str(fbOrigWeight_parm)+'_'+str(term)+'_'+initial_feedback_setup_name+'_'+RM_setup_name[original_RM_setup-1]
					this_output_name=this_input_name+'_'+feedback_setup_name[setup-1]
					this_output_path=output_param_files_dir+this_output_name
					this_output+=start_param
					this_output+='\t<RM_Setup>'+str( RM_setup )+'</RM_Setup>\n'
					this_output+='\t<fbTerms>'+str(term)+'</fbTerms>\n'
					this_output+='\t<fbOrigWeight>'+str(0)+'</fbOrigWeight>\n'
					this_output+='\t<feedback_setup>'+str(setup)+'</feedback_setup>\n'
					this_output+='\t<fbDocs>'+str(fbDocs_parm)+'</fbDocs>\n'
					for query in sorted( q_info.keys( ) ):
						if rerank_setup == 2:
							D_f=es.Rel_used[fbDocs_parm][query]
						elif rerank_setup == 3:
							D_f=es.Neg_used[fbDocs_parm][query]
						if len( D_f ) >= 1:
							this_output+=q_info[query][0]
							for docNo in D_f:
								this_output+='\t\t<feedbackDocno>'+str( docNo )+'</feedbackDocno>\n'
							batch_cut_top='grep "'+query+' Q0 " '+baseline_res+this_input_name+" | awk '{ if ($4<1200) print $3;}'"
							output2=subprocess.check_output( batch_cut_top, shell=True )
							for Docno in output2.rstrip( '\n' ).split( '\n' ):
								this_output+='\t\t<workingSetDocno>'+Docno+'</workingSetDocno>\n'
							this_output+=q_info[query][1]
					this_output+="</parameters>\n"
					out_f=open(this_output_path,'w+')
					out_f.write(this_output)
					out_f.close()
		print "done"
	return 1

if flag_fixed:
	create_parameters_fixed( );
else:
	create_parameters_accumulative();
